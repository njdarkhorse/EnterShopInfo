package com.nihome.entershopinfo.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.nihome.entershopinfo.R;

/**
 * Created by Administrator on 2017/7/17.
 */
public class PolygonView extends View {
    private Paint paint;
    private Path path;
    private int width;
    private int height;

    private String text;
    private int color;

    public PolygonView(Context context){
        super(context, null);
    }

    public PolygonView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PolygonView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.PolygonView, defStyleAttr, 0);
        int count = array.getIndexCount();
        for (int i = 0; i < count; i++) {
            int attr = array.getIndex(i);
            switch (attr) {
                case R.styleable.PolygonView_text:
                    text = array.getString(attr);
                    break;
                case R.styleable.PolygonView_color:
                    color = array.getColor(attr, Color.RED);
                    break;
            }
        }
        array.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        width = MeasureSpec.getSize(widthMeasureSpec);
        height = MeasureSpec.getSize(heightMeasureSpec);
        Log.e("PolygonView", "width========" + width);
        Log.e("PolygonView", "height========" + height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Log.e("PolygonView", "onDraw========");
        super.onDraw(canvas);
        paint = new Paint();
        path = new Path();
        paint.setAntiAlias(true);                         //设置画笔为无锯齿
        paint.setColor(color);                            //设置画笔颜色
        //canvas.drawColor(Color.WHITE);                  //白色背景
        paint.setStrokeWidth((float) 2.0);                //线宽
        paint.setStyle(Paint.Style.STROKE);
        path.moveTo(0, 0);
        path.lineTo(width - 20, 0);
        path.lineTo(width, height / 2);
        path.lineTo(width - 20, height);
        path.lineTo(0, height);
        path.lineTo(20, height / 2);
        path.lineTo(0, 0);
        canvas.drawPath(path, paint);
        //绘制文字
        paint.setStrokeWidth((float) 1.0);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(16);
        canvas.drawText(text, width/2, height/2 + 5, paint);
    }
}
