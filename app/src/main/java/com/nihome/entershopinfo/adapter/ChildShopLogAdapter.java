package com.nihome.entershopinfo.adapter;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nihome.entershopinfo.R;
import com.nihome.entershopinfo.entity.response.ExpandsLogRes;

import java.util.List;

/**
 * Created by Carson on 2017/7/24.
 */
public class ChildShopLogAdapter extends BaseAdapter {
    public static final int LOOK = 1;
    public static final int VOUCHER = 2;
    private Context mContext;
    private LayoutInflater inflater;
    private List<ExpandsLogRes> data;
    private Handler mHandler;
    private int selectPosition = -1;

    public ChildShopLogAdapter(Context context, List<ExpandsLogRes> data, Handler mHandler){
        this.mContext = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.mHandler = mHandler;
    }


    public void setData(List<ExpandsLogRes> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        TextView childShopType;
        TextView childShopName;
        TextView bussShopType;
        TextView whetherCheckView;
        TextView lookBtn;
        TextView memoView;
        ViewHold viewHold = null;
        if (convertView == null) {
            viewHold = new ViewHold();
            convertView = inflater.inflate(R.layout.item_child_shop, parent, false);
            childShopType = (TextView) convertView.findViewById(R.id.child_shop_type);
            childShopName = (TextView) convertView.findViewById(R.id.child_shop_name);
            bussShopType = (TextView) convertView.findViewById(R.id.buss_shop_type);
            whetherCheckView = (TextView) convertView.findViewById(R.id.whether_check_view);
            lookBtn = (TextView) convertView.findViewById(R.id.look_btn);
            memoView = (TextView) convertView.findViewById(R.id.memo_view);
            viewHold.childShopType = childShopType;
            viewHold.childShopName = childShopName;
            viewHold.bussShopType = bussShopType;
            viewHold.whetherCheckView = whetherCheckView;
            viewHold.lookBtn = lookBtn;
            viewHold.memoView = memoView;
            convertView.setTag(viewHold);
        } else {
            viewHold = (ViewHold) convertView.getTag();
            childShopType = viewHold.childShopType;
            childShopName = viewHold.childShopName;
            bussShopType = viewHold.bussShopType;
            whetherCheckView = viewHold.whetherCheckView;
            lookBtn = viewHold.lookBtn;
            memoView = viewHold.memoView;
        }
        if (data.get(position).getBasicInfoResponseDTO().getPartnerType() == 2) childShopType.setText("总店");
        else childShopType.setText("分店");

        childShopName.setText(data.get(position).getBasicInfoResponseDTO().getStoreName());
        if (data.get(position).getBasicInfoResponseDTO().getPartnerType() == 3) bussShopType.setText("直营店");
        else if (data.get(position).getBasicInfoResponseDTO().getPartnerType() == 4) bussShopType.setText("加盟店");


        final short status = data.get(position).getOtherInfoResponseDTO().getAuditStatus();
        if (status == 101) whetherCheckView.setText("未审核");
        else if (status == 201) whetherCheckView.setText("审核中");
        else if (status == 202) whetherCheckView.setText("审核不通过");
        else whetherCheckView.setText("审核通过");
        memoView.setText(data.get(position).getOtherInfoResponseDTO().getAuditBDMemo());


        lookBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Message message = new Message();
                message.what = LOOK;
                message.arg1 = position;
                mHandler.sendMessage(message);

            }
        });
        return convertView;
    }

    public void setSelectItem(int selectItem) {
        this.selectPosition = selectItem;
    }

    class ViewHold{
        TextView childShopType;
        TextView childShopName;
        TextView bussShopType;
        TextView whetherCheckView;
        TextView lookBtn;
        TextView memoView;
    }
}
