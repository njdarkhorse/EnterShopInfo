package com.nihome.entershopinfo.adapter;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nihome.entershopinfo.R;
import com.nihome.entershopinfo.entity.response.ExpandsLogRes;

import java.util.IllegalFormatCodePointException;
import java.util.List;

/**
 * Created by Carson on 2017/7/24.
 */
public class ExpandsLogAdapter extends BaseAdapter {
    public static final int LOOK = 1;
    public static final int VOUCHER = 2;
    private Context mContext;
    private LayoutInflater inflater;
    private List<ExpandsLogRes> data;
    private Handler mHandler;
    private int selectPosition = -1;

    public ExpandsLogAdapter(Context context, List<ExpandsLogRes> data, Handler mHandler){
        this.mContext = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.mHandler = mHandler;
    }


    public void setData(List<ExpandsLogRes> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ImageView radioFlag;
        TextView subTimeView;
        TextView shopNameView;
        TextView typeView;
        TextView whetherCheckView;
        TextView lookBtn;
        TextView voucherBtn;
        TextView memoView;
        ViewHold viewHold = null;
        if (convertView == null) {
            viewHold = new ViewHold();
            convertView = inflater.inflate(R.layout.item_expands_log, parent, false);
            radioFlag = (ImageView) convertView.findViewById(R.id.radio_flag);
            subTimeView = (TextView) convertView.findViewById(R.id.sub_time_view);
            shopNameView = (TextView) convertView.findViewById(R.id.shop_name_view);
            typeView = (TextView) convertView.findViewById(R.id.type_view);
            whetherCheckView = (TextView) convertView.findViewById(R.id.whether_check_view);
            lookBtn = (TextView) convertView.findViewById(R.id.look_btn);
            voucherBtn = (TextView) convertView.findViewById(R.id.voucher_btn);
            memoView = (TextView) convertView.findViewById(R.id.memo_view);
            viewHold.radioFlag = radioFlag;
            viewHold.subTimeView = subTimeView;
            viewHold.shopNameView = shopNameView;
            viewHold.typeView = typeView;
            viewHold.whetherCheckView = whetherCheckView;
            viewHold.lookBtn = lookBtn;
            viewHold.voucherBtn = voucherBtn;
            viewHold.memoView = memoView;
            convertView.setTag(viewHold);
        } else {
            viewHold = (ViewHold) convertView.getTag();
            radioFlag = viewHold.radioFlag;
            subTimeView = viewHold.subTimeView;
            shopNameView = viewHold.shopNameView;
            typeView = viewHold.typeView;
            whetherCheckView = viewHold.whetherCheckView;
            lookBtn = viewHold.lookBtn;
            voucherBtn = viewHold.voucherBtn;
            memoView = viewHold.memoView;
        }
        subTimeView.setText(data.get(position).getOtherInfoResponseDTO().getDateCreated().substring(5, 10));
        shopNameView.setText(data.get(position).getBasicInfoResponseDTO().getStoreName());
        final short status = data.get(position).getOtherInfoResponseDTO().getAuditStatus();
        if (data.get(position).getBasicInfoResponseDTO().getPartnerType() == null) {
            typeView.setText("单店");
        } else if (data.get(position).getBasicInfoResponseDTO().getPartnerType() == 0){
            typeView.setText("单店");
        } else {
            typeView.setText("连锁店");
        }
        if (status == 101) whetherCheckView.setText("未审核");
        else if (status == 201) whetherCheckView.setText("审核中");
        else if (status == 202) whetherCheckView.setText("审核不通过");
        else whetherCheckView.setText("审核通过");
        memoView.setText(data.get(position).getOtherInfoResponseDTO().getAuditBDMemo());
        if (selectPosition == position) {
            radioFlag.setImageResource(R.drawable.radio_true);
        } else {
            radioFlag.setImageResource(R.drawable.radio_false);
        }


        lookBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Message message = new Message();
                message.what = LOOK;
                message.arg1 = position;
                mHandler.sendMessage(message);
            }
        });
        voucherBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status == 101 || status == 202) {
                    Toast.makeText(mContext, "请先提交全部信息", Toast.LENGTH_SHORT).show();
                    return;
                }
                Message message = new Message();
                message.what = VOUCHER;
                message.arg1 = position;
                mHandler.sendMessage(message);
            }
        });
        return convertView;
    }

    public void setSelectItem(int selectItem) {
        this.selectPosition = selectItem;
    }

    class ViewHold{
        ImageView radioFlag;
        TextView subTimeView;
        TextView shopNameView;
        TextView typeView;
        TextView whetherCheckView;
        TextView lookBtn;
        TextView voucherBtn;
        TextView memoView;
    }
}
