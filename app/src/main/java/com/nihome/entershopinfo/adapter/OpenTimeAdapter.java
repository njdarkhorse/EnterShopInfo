package com.nihome.entershopinfo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.nihome.entershopinfo.R;

/**
 * Created by Carson on 2017/7/20.
 */
public class OpenTimeAdapter extends BaseAdapter {
    private LayoutInflater inflater;

    public OpenTimeAdapter(Context context){
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.open_time_item, parent, false);
        }
        return convertView;
    }
}
