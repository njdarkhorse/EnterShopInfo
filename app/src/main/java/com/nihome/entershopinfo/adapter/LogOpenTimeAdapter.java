package com.nihome.entershopinfo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nihome.entershopinfo.R;

/**
 * Created by Carson on 2017/7/24.
 */
public class LogOpenTimeAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private String[] str;
    public LogOpenTimeAdapter(Context context, String[] str){
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.str = str;
    }

    @Override
    public int getCount() {
        return str.length;
    }

    @Override
    public Object getItem(int position) {
        return str[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView openTime;
        TextView closeTime;
        ViewHold viewHold = null;
        if (convertView == null) {
            viewHold = new ViewHold();
            convertView = inflater.inflate(R.layout.log_open_time, parent, false);
            openTime = (TextView) convertView.findViewById(R.id.log_open_time_item);
            closeTime = (TextView) convertView.findViewById(R.id.log_close_time_item);
            viewHold.openTime = openTime;
            viewHold.closeTime = closeTime;
            convertView.setTag(viewHold);
        } else {
            viewHold = (ViewHold) convertView.getTag();
            openTime = viewHold.openTime;
            closeTime = viewHold.closeTime;
        }
        String[] time = str[position].split("-");
        openTime.setText(time[0]);
        closeTime.setText(time[1]);
        return convertView;
    }

    class ViewHold{
        TextView openTime;
        TextView closeTime;
    }
}
