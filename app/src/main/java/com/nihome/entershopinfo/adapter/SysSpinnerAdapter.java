package com.nihome.entershopinfo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nihome.entershopinfo.R;

/**
 * Created by Carson on 2017/7/20.
 */
public class SysSpinnerAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private String[] data;

    public SysSpinnerAdapter(Context context, String[] data) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.data = data;
    }

    public void setData(String[] data){
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public Object getItem(int position) {
        return data[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView;
        ViewHold viewHold = null;
        if (convertView == null) {
            viewHold = new ViewHold();
            convertView = inflater.inflate(R.layout.spinner_item, parent, false);
            textView = (TextView) convertView.findViewById(R.id.spinner_text);
            viewHold.textView = textView;
            convertView.setTag(viewHold);
        } else {
            viewHold = (ViewHold) convertView.getTag();
            textView = viewHold.textView;
        }
        textView.setText(data[position]);
        return convertView;
    }

    static class ViewHold{
        TextView textView;
    }
}
