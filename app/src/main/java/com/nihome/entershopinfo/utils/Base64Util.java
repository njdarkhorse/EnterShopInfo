package com.nihome.entershopinfo.utils;

import org.apache.commons.codec.binary.Base64;

import java.io.UnsupportedEncodingException;

public class Base64Util {

	public static String encode(byte[] binaryData) {
		try {
			return new String(Base64.encodeBase64(binaryData), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}

	public static String encode(String normalString) {
		try {
			return new String(Base64.encodeBase64(normalString.getBytes()),
					"UTF-8");
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}

	public static byte[] decode(String base64String) {
		try {
			return Base64.decodeBase64(base64String.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}

	public static String base64ToString(String b64){
		try {
            return new String(android.util.Base64.decode(b64.getBytes(), android.util.Base64.NO_WRAP));
		} catch (Exception e){
			return null;
		}
	}

	public static String StringToBase64String(String text){

		try {
			return android.util.Base64.encodeToString(text.getBytes("UTF-8"), android.util.Base64.NO_WRAP);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}
}