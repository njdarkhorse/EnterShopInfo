package com.nihome.entershopinfo.utils;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Build;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.nio.ByteBuffer;

/**图片转换工具类*/
public class BitmapUtils {
	public static Bitmap downSizeBitmap(Bitmap bitmap,int reqSize)  {

		int width = bitmap.getWidth();
		int height = bitmap.getHeight();

		float scaleWidth = ((float) reqSize) / width;
		float scaleHeight = ((float) reqSize) / height;

		Matrix matrix = new Matrix();
		matrix.postScale(scaleWidth, scaleHeight);

		Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, false);
		return resizedBitmap;

		/*if(bitmap.getWidth() < reqSize) {
			return bitmap;
		} else {
			return Bitmap.createScaledBitmap(bitmap, reqSize, reqSize, false);
		} */
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	public static byte[] convertBitmapToBytes(Bitmap bitmap) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			ByteBuffer buffer = ByteBuffer.allocate(bitmap.getByteCount());
			bitmap.copyPixelsToBuffer(buffer);
			return buffer.array();
		} else {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			bitmap.compress(CompressFormat.JPEG, 100, baos);
			byte[] data = baos.toByteArray();
			return data;
		}
	}

	/** 输入流 转为输出流*/
	static public void copy(InputStream in, OutputStream out)
			throws IOException {
		byte[] b = new byte[1024*1024];
		int read;
		while ((read = in.read(b)) != -1) {
			out.write(b, 0, read);
		}
		out.flush();
		out.close();
	}

	/** file to bitmap */
	static public Bitmap FileToBitmap(File file){
		Bitmap bitmap = null;

		InputStream is = null;
		BufferedOutputStream bf = null;

		try {
			is = new BufferedInputStream(new FileInputStream(file));
			final ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
			bf = new BufferedOutputStream(dataStream);
			copy(is, bf);
			bf.flush();
			bf.close();
			byte[] bytes = dataStream.toByteArray();
			bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
			bytes = null;

			return bitmap;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	/** file to byte[]  */
	static public byte[] FileTobyte(File file){
		Bitmap bitmap = null;

		InputStream is = null;
		BufferedOutputStream bf = null;

		try {
			is = new BufferedInputStream(new FileInputStream(file));
			final ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
			bf = new BufferedOutputStream(dataStream);
			copy(is, bf);
			bf.flush();
			bf.close();
			byte[] bytes = dataStream.toByteArray();
			return bytes;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**将图片转为base64字符串*/
	static public String BitmapToBase64(Bitmap bitmap, int bitmapQuality){

		ByteArrayOutputStream bStream = new ByteArrayOutputStream();
		bitmap.compress(CompressFormat.JPEG, bitmapQuality, bStream);
		byte[] bytes = bStream.toByteArray();

		return Base64.encodeToString(bytes, Base64.NO_WRAP);
	}

	/**将base64 转成 byte[]*/
	static public byte[] String64ToBytes(String base64){
		try{
			return Base64.decode(base64, Base64.DEFAULT);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**将byte[] 转成bitmap*/
	static public Bitmap byteToBitmap(byte[] bytes){
		Bitmap bitmap = null;
		try{
			bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
			return bitmap;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**base64 转bitmap*/
	static public Bitmap StringToBitmap(String base64String){

		Bitmap bitmap = null;

		try{
			byte[] byteArray = Base64.decode(base64String, Base64.DEFAULT);
			bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
			return bitmap;
		} catch (Exception e) {
		}
		return null;
	}


	static public File savebitmap(Bitmap b,String filename) {
		String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
		OutputStream outStream = null;

		File file = new File(filename + ".jpg");
		if (file.exists()) {
			file.delete();
			file = new File(extStorageDirectory, filename + ".jpg");
			Log.e("file exist", "" + file + ",Bitmap= " + filename);
		}
		try {

			outStream = new FileOutputStream(file);
			b.compress(CompressFormat.PNG, 100, outStream);
			outStream.flush();
			outStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Log.e("file", "" + file);
		return file;

	}

}
