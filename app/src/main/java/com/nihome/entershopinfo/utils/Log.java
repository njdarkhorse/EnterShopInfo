package com.nihome.entershopinfo.utils;

/**
 * Created by carson on 2016/3/29.
 */
public class Log {
    private static final String debugTag = "entershopinfo";
    private static DebugLevel debugLevel = DebugLevel.VERBOSE;

    private Log(){
    }

    public static void d(final String message) {
        if (isLogEnable()) android.util.Log.d(debugTag, message, null);
    }

    public static void d(final String message, final Throwable throwable) {
        if (isLogEnable()) android.util.Log.d(debugTag, message, throwable);
    }

    public static void d(final String tag, final String message) {
        if (isLogEnable()) android.util.Log.d(tag, message, null);
    }

    public static void d(final String tag, final String message, final Throwable throwable) {
        if (isDebuggable(DebugLevel.DEBUG) == false) {
            return;
        }
        if (throwable == null) {
            if (isLogEnable()) android.util.Log.d(tag, message);
        } else {
            if (isLogEnable()) android.util.Log.d(tag, message, throwable);
        }
    }

    public static void i(final String message) {
        if (isLogEnable()) android.util.Log.i(debugTag, message, null);
    }

    public static void i(final String message, final Throwable throwable) {
        if (isLogEnable()) android.util.Log.i(debugTag, message, throwable);
    }

    public static void i(final String tag, final String message) {
        if (isLogEnable()) android.util.Log.i(tag, message, null);
    }

    public static void i(final String tag, final String message, final Throwable throwable) {
        if (isDebuggable(DebugLevel.INFO) == false) {
            return;
        }
        if (throwable == null) {
            if (isLogEnable()) android.util.Log.i(tag, message);
        } else {
            if (isLogEnable()) android.util.Log.i(tag, message, throwable);
        }
    }

    public static void w(final String message) {
        if (isLogEnable()) android.util.Log.w(debugTag, message, null);
    }

    public static void w(final Throwable throwable) {
        if (isLogEnable()) android.util.Log.w(debugTag, "", throwable);
    }

    public static void w(final String tag, final String message) {
        if (isLogEnable()) android.util.Log.w(tag, message, null);
    }

    public static void w(final String tag, final String message, final Throwable throwable) {
        if (isDebuggable(DebugLevel.WARNING) == false) {
            return;
        }
        if (throwable == null) {
            if (isLogEnable()) android.util.Log.w(tag, message, new Exception());
        } else {
            if (isLogEnable()) android.util.Log.w(tag, message, throwable);
        }
    }

    public static void e(final String message) {
        if (isLogEnable()) android.util.Log.e(debugTag, message, null);
    }

    public static void e(final Throwable throwable) {
        if (isLogEnable()) android.util.Log.e(debugTag, "", throwable);
    }

    public static void e(final String tag, final String message) {
        if (message == null || message.isEmpty()) {
            if (isLogEnable()) android.util.Log.e(tag, "null", null);
        } else {
            if (isLogEnable()) android.util.Log.e(tag, message, null);
        }
    }

    public static void e(final String tag, final String message, final Throwable throwable) {
        if (isDebuggable(DebugLevel.ERROR) == false) {
            return;
        }
        if (throwable == null) {
            if (isLogEnable()) android.util.Log.e(tag, message);
            return;
        }
        if (isLogEnable()) android.util.Log.e(tag, message, throwable);
    }

    public static boolean isLogEnable() {
        return true;
    }

    public static boolean isDebuggable(DebugLevel level) {
        return debugLevel.isDebuggable(level);
    }

    public enum DebugLevel implements Comparable<DebugLevel> {
        NONE, ERROR, WARNING, INFO, DEBUG, VERBOSE;

        public static DebugLevel ALL = DebugLevel.VERBOSE;

        private boolean isDebuggable(final DebugLevel debugLevel) {
            return this.compareTo(debugLevel) >= 0;
        }
    }
}
