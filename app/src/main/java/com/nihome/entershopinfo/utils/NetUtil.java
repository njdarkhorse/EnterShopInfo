package com.nihome.entershopinfo.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

/**
 * Created by carson on 2016/4/12.
 */
public class NetUtil {
    public static final String DEBUG_TAG = "NetUtil";

    Context             mContext;
    TelephonyManager    mTelephonyManager;
    NetworkInfo         mNetworkInfo;
    ConnectivityManager mConnectivityManager;

    public NetUtil(Context context) {
        mContext = context;
        mTelephonyManager = (TelephonyManager)
                mContext.getSystemService(Context.TELEPHONY_SERVICE);
        mConnectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
    }

    public boolean isNetConnected() {
        mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
        return !(mNetworkInfo == null || !mNetworkInfo.isAvailable());
    }

    public String getDeviceId(){
        String szImei = mTelephonyManager.getDeviceId();
        return szImei;
    }
}
