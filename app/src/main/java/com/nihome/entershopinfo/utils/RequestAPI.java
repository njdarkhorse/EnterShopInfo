package com.nihome.entershopinfo.utils;

/**
 * Created by Carson on 2017/7/21.
 */
public class RequestAPI {
    //static public final String SERVER_URL = "https://dev.nihome.cn/";
    static public final String SERVER_URL = "https://api.nihome.cn/";

    static public final String LOGIN_URL                 = SERVER_URL + "marketer/marketerLogin";
    static public final String INIT_SHOP_DATA_URL        = SERVER_URL + "audit/getAuditId/";
    static public final String BASIC_INFO_URL            = SERVER_URL + "/audit/disposeAuditBasicInfo/";
    static public final String SHOP_INFO_URL             = SERVER_URL + "/audit/judgeAuditStoreInfo/";
    static public final String SEND_INFO_URL             = SERVER_URL + "/audit/judgeAuditTakeoutInfo/";
    static public final String SETTLEMENT_INFO_URL       = SERVER_URL + "/audit/judgeAuditSettlementInfo/";
    static public final String XCX_RELEVANT_URL          = SERVER_URL + "/audit/judgeAuditProgramsInfo/";
    static public final String QUALIFICATIONS_URL        = SERVER_URL + "/audit/judgeAuditQualificationInfo/";
    static public final String FINISH_URL                = SERVER_URL + "/audit/judgeAuditGroupPhotoInfo/";
    static public final String EXPANDS_LOG_URL           = SERVER_URL + "/audit/getAuditList/";
    static public final String MONEY_VOUCHER_URL         = SERVER_URL + "/audit/uploadRemitVoucher/";
    static public final String FIND_CHILD_SHOP           = SERVER_URL + "/audit/subAuditStores/";

    static public final String UPLOAD_IMG_URL            = SERVER_URL + "/uploadFile/fileUploadStr2";
}
