package com.nihome.entershopinfo.utils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;

import java.io.File;

/**
 * Created by Carson on 2017/7/19.
 */
public class ImagePicker {
    public static final int REQUEST_CAPTURE = 1000;          //拍照
    public static final int REQUEST_PICK_IMAGE = 1001;       //相册选取
    public static final int REQUEST_PICTURE_CUT = 1003;      //剪裁图片
    private Activity activity;
    private Fragment fragment;

    private Uri imageUri;//原图保存地址

    public ImagePicker(Activity activity) {
        this.activity = activity;
    }

    public ImagePicker(Activity activity, Fragment fragment){
        this.activity = activity;
        this.fragment = fragment;
    }

    /** 打开系统相机 */
    public Uri openCamera() {
        File file = new FileStorage().createIconFile();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            imageUri = FileProvider.getUriForFile(activity, "com.nihome.entershopinfo.fileprovider", file);//通过FileProvider创建一个content类型的Uri
        } else {
            imageUri = Uri.fromFile(file);
        }
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); //添加这一句表示对目标应用临时授权该Uri所代表的文件
        }
        intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);//设置Action为拍照
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);//将拍取的照片保存到指定URI
        fragment.startActivityForResult(intent, REQUEST_CAPTURE);
        return imageUri;
    }

    /**
     * 相册选择
     */
    public void pickOnphone(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        fragment.startActivityForResult(intent, REQUEST_PICK_IMAGE);
    }


    /**
     * 裁剪
     */
    public Uri cropPhoto(Uri needCutUri) {
        File file = new FileStorage().createCropFile();
        Uri outputUri = Uri.fromFile(file);//缩略图保存地址
        Log.e("needCutUri====" , needCutUri.toString());
        Log.e("outputUri====" , outputUri.toString());
        Intent intent = new Intent("com.android.camera.action.CROP");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        intent.setDataAndType(needCutUri, "image/*");
        intent.putExtra("crop", "true");
        if (Build.MODEL.contains("KOB-W09")) {
            intent.putExtra("aspectX", 2);
            intent.putExtra("aspectY", 1.8);
        } else {
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
        }
        intent.putExtra("scale", true);
        intent.putExtra("return-data", false);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true);
        fragment.startActivityForResult(intent, REQUEST_PICTURE_CUT);
        return outputUri;
    }

    /*public Uri cropPhoto() {
        File file = new FileStorage().createCropFile();
        Uri outputUri = Uri.fromFile(file);//缩略图保存地址
        Log.e("裁剪====" , outputUri.toString());
        Intent intent = new Intent("com.android.camera.action.CROP");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        intent.setDataAndType(imageUri, "image*//*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("scale", true);
        intent.putExtra("return-data", false);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true);
        fragment.startActivityForResult(intent, REQUEST_PICTURE_CUT);
        return outputUri;
    }*/
}
