package com.nihome.entershopinfo.dialog;

import android.app.Dialog;
import android.content.Context;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nihome.entershopinfo.R;


/**
 * Created by carson on 2016/7/18.
 */
public class LoadingDialog extends Dialog {
    private Context mContext;
    private ProgressBar mBar;
    private TextView mMessage;

    public LoadingDialog(Context context) {
        super(context, R.style.LoadDialog);
        setContentView(R.layout.dialog_loading);
        //点击imageview外侧区域，动画不会消失
        setCanceledOnTouchOutside(false);
        mMessage = (TextView) findViewById(R.id.message);
        mBar = (ProgressBar) findViewById(R.id.bar);
        mContext = context;
    }


    public void show(String msg) {
        if (mMessage != null) {
            mMessage.setText(msg);
        }
        super.show();
    }

    //设置进度图片
    public void setIndeterminateDrawable(int drawable) {
        mBar.setIndeterminateDrawable(mContext.getResources().getDrawable(drawable));
    }

    //设置字体颜色
    public void setTextColor(int color) {
        mMessage.setTextColor(color);
    }

    public void setText(String msg){
        mMessage.setText(msg);
    }
}
