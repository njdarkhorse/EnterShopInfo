package com.nihome.entershopinfo.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;

import com.nihome.entershopinfo.R;


/**
 * Created by carson on 2016/6/7.
 */
public class ImagePickDialog extends Dialog {
    public interface DialogSelectedListener{
        void onCapture();
        void onPhone();
        void onCancel();
    }

    private DialogSelectedListener listener;

    public ImagePickDialog(Context context){
        super(context, R.style.MyTransparent);
        setContentView(R.layout.dialog_imagepick);
        findViewById(R.id.selectOnCapture).setOnClickListener(onClickListener);
        findViewById(R.id.selectOnPhone).setOnClickListener(onClickListener);
        findViewById(R.id.cancelButton).setOnClickListener(onClickListener);
    }


    public void setDialogSelectedListener(DialogSelectedListener l){
        this.listener = l;
    }

    private View.OnClickListener onClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.selectOnCapture:
                    if (listener != null) listener.onCapture();
                    dismiss();
                    break;
                case R.id.selectOnPhone:
                    if (listener != null) listener.onPhone();
                    dismiss();
                    break;
                case R.id.cancelButton:
                    if (listener != null) listener.onCancel();
                    dismiss();
                    break;
            }
        }
    };
}
