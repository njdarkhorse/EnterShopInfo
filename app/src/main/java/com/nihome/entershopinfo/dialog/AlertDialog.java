package com.nihome.entershopinfo.dialog;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.nihome.entershopinfo.R;
import com.nihome.entershopinfo.utils.StringUtils;


public class AlertDialog extends BaseDialog implements View.OnClickListener {
    private TextView btn_cancel, btn_ok;
    private TextView mTitleText;
    private TextView mMessageText;
    private View mDivider;
    private String mTitle;
    private String mOkText;
    private String mNgText;
    private String mContent;
    private OnClickListener mOnClickListener;
    private static BaseDialog      mBaseDialog;// 当前的对话框
    private static boolean         mSingleButton;

    public AlertDialog(Context context, String text) {
        super(context);
        mContent = text;
        mSingleButton = false;
        mBaseDialog = new BaseDialog(context);
        initContent();
    }

    public AlertDialog(Context context, String text, String okText, String ngText) {
        super(context);
        mContent = text;
        mSingleButton = false;
        mBaseDialog = new BaseDialog(context);
        mOkText = okText;
        mNgText = ngText;
        initContent();
    }

    public AlertDialog(Context context, String text, Boolean singleBotton) {
        super(context);
        mContent = text;
        mSingleButton = singleBotton;
        mBaseDialog = new BaseDialog(context);
        initContent();
    }

    private void initContent() {
        setContentView(R.layout.alert_dialog);
        mTitleText = (TextView) findViewById(R.id.title);
        mTitleText.setVisibility(View.GONE);
        mMessageText = (TextView) findViewById(R.id.alert_message);
        mMessageText.setText(Html.fromHtml(mContent));
        btn_cancel = (TextView) findViewById(R.id.btn_cancel);
        btn_ok = (TextView) findViewById(R.id.btn_ok);
        mDivider = findViewById(R.id.divider);
        btn_cancel.setOnClickListener(this);
        btn_ok.setOnClickListener(this);
        if (mSingleButton) {
            btn_cancel.setVisibility(View.GONE);
            mDivider.setVisibility(View.GONE);
        } else {
            btn_cancel.setVisibility(View.VISIBLE);
            mDivider.setVisibility(View.VISIBLE);
        }
        if (!StringUtils.isBlank(mOkText)) btn_ok.setText(mOkText);
        if (!StringUtils.isBlank(mNgText)) btn_cancel.setText(mNgText);

    }

    private void initTitle() {
        mTitleText.setVisibility(View.VISIBLE);
        mTitleText.setText(mTitle);
    }

    public void setText(String text) {
        if (text == null) {
            mMessageText.setVisibility(View.GONE);
        } else {
            mMessageText.setVisibility(View.VISIBLE);
            mContent = text;
            mMessageText.setText(mContent);
        }
    }

    public void setTitle(String title) {
        if (title == null) {
            mTitleText.setVisibility(View.GONE);
        } else {
            mTitleText.setVisibility(View.VISIBLE);
            mTitle = title;
            mTitleText.setText(mTitle);
        }
    }

    public void setBtnOkText(String text) {
        btn_ok.setText(text);
    }

    public void setBtnNgText(String text) {
        btn_cancel.setText(text);
    }

    public void setHiddenLeft(){
        btn_cancel.setVisibility(View.GONE);
    }

    public void setBtnOkLinstener(OnClickListener listener) {
        mOnClickListener = listener;
    }


    @Override
    public void dismiss() {
        if (isShowing()) {
            super.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                if (isShowing()) {
                    super.dismiss();
                }
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(mBaseDialog, 0);
                }
                break;

            case R.id.btn_ok:
                if (isShowing()) {
                    super.dismiss();
                }
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(mBaseDialog, 1);
                }
                break;
        }
    }
}
