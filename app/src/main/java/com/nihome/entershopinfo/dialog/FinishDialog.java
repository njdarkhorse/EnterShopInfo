package com.nihome.entershopinfo.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;

import com.nihome.entershopinfo.R;

/**
 * Created by Carson on 2017/7/24.
 */
public class FinishDialog extends Dialog {
    private Context mContext;
    private FinishDialogListener listener;


    public interface FinishDialogListener{
        void onFinish();
    }

    public FinishDialog(Context context){
        super(context, R.style.MyTransparent);
        this.mContext = context;
        initViews();
    }

    private void initViews(){
        setContentView(R.layout.dialog_finish);
        findViewById(R.id.dialog_finish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onFinish();
                dismiss();
            }
        });
    }

    public void setListener(FinishDialogListener listener) {
        this.listener = listener;
    }
}
