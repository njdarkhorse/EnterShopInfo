package com.nihome.entershopinfo.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;

import com.nihome.entershopinfo.R;

/**
 * Created by Carson on 2017/10/24.
 */
public class ShopTypeDialog extends Dialog implements View.OnClickListener{
    private ShopTypeListener listener;

    public void setListener(ShopTypeListener listener) {
        this.listener = listener;
    }

    public interface ShopTypeListener{
        void onSingleShop();
        void onChainShop();
    }

    public ShopTypeDialog(Context context){
        super(context, R.style.TransparentStyle);
        setCanceledOnTouchOutside(true);
        initView();
    }

    private void initView(){
        setContentView(R.layout.dialog_shop_type);
        findViewById(R.id.single_shop_layout).setOnClickListener(this);
        findViewById(R.id.chain_shop_layout).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.single_shop_layout:
                listener.onSingleShop();
                break;
            case R.id.chain_shop_layout:
                listener.onChainShop();
                break;
        }
    }
}
