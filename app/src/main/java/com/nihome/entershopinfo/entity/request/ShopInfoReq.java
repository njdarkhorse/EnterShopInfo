package com.nihome.entershopinfo.entity.request;


import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigDecimal;
import java.util.List;

/**
 * Description:
 * author: 格式化油条
 * date:  2017/7/19.
 */
public class ShopInfoReq implements Parcelable {

    /**
     * 资料id
     */
    public String auditId;

    /**
     * 市场人员id
     */
    public String marketerId;

    /**
     * 操作方式 21为添加 22为修改
     */
    public Short operation;


    public String province;

    public String city;

    public String district;

    /**
     * H5堂食的支付方式，0-线上公众号，1-线下小票扫码 ,
     */
    private Integer hallEatPayType;

    /**
     * 店铺地址
     */
    public String storeAddress;

    /**
     * 店铺营业时间 多个以 ; 分隔
     */
    public String storeOpeningHour;

    /**
     * 小票机数量
     */
    public Integer posNumber;

    /**
     * 小票打印份数
     */
    public Integer receiptNumber;

    /**
     * 茶位费
     */
    public BigDecimal teeFee;

    /**
     * 茶位费是否参与优惠 1为参与 0为不参与
     */
    public Short teeFeeDiscount;

    /**
     * 桌号数
     */
    public Integer numberOfSite;

    /**
     * 桌号备注
     */
    public String seatMemo;


    /**
     * 店铺就餐方式 0为堂食 1为外卖 2为堂食+外卖
     */
    public Short diningStyle;

    /**
     * 是否要取餐通知 1为需要 0为不需要
     */
    public Short takeMealReminder;

    /**
     * 是否要外卖 1为需要 0为不需要
     */
    public Short takeOut;


    /**
     * 店铺菜单URL
     */
    public List<String> menuUrl;

    /**
     * 店铺菜单ID
     */
    public String storeMenuIds;

    /**
     * 菜单备注
     */
    public String storeMenuMemo;

    /**
     * 手机型号
     */
    public String mobileModel;

    /**
     * 获取 资料id
     */
    public String getAuditId() {
        return this.auditId;
    }

    /**
     * 设置 资料id
     */
    public void setAuditId(String auditId) {
        this.auditId = auditId;
    }

    /**
     * 获取 市场人员id
     */
    public String getMarketerId() {
        return this.marketerId;
    }

    /**
     * 设置 市场人员id
     */
    public void setMarketerId(String marketerId) {
        this.marketerId = marketerId;
    }

    /**
     * 获取 店铺地址
     */
    public String getStoreAddress() {
        return this.storeAddress;
    }

    /**
     * 设置 店铺地址
     */
    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    /**
     * 获取 店铺营业时间 多个以 ; 分隔
     */
    public String getStoreOpeningHour() {
        return this.storeOpeningHour;
    }

    /**
     * 设置 店铺营业时间 多个以 ; 分隔
     */
    public void setStoreOpeningHour(String storeOpeningHour) {
        this.storeOpeningHour = storeOpeningHour;
    }

    /**
     * 获取 小票机数量
     */
    public Integer getPosNumber() {
        return this.posNumber;
    }

    /**
     * 设置 小票机数量
     */
    public void setPosNumber(Integer posNumber) {
        this.posNumber = posNumber;
    }

    /**
     * 获取 小票打印份数
     */
    public Integer getReceiptNumber() {
        return this.receiptNumber;
    }

    /**
     * 设置 小票打印份数
     */
    public void setReceiptNumber(Integer receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    /**
     * 获取 茶位费
     */
    public BigDecimal getTeeFee() {
        return this.teeFee;
    }

    /**
     * 设置 茶位费
     */
    public void setTeeFee(BigDecimal teeFee) {
        this.teeFee = teeFee;
    }

    /**
     * 获取 茶位费是否参与优惠 1为参与 0为不参与
     */
    public Short getTeeFeeDiscount() {
        return this.teeFeeDiscount;
    }

    /**
     * 设置 茶位费是否参与优惠 1为参与 0为不参与
     */
    public void setTeeFeeDiscount(Short teeFeeDiscount) {
        this.teeFeeDiscount = teeFeeDiscount;
    }

    /**
     * 获取 桌号数
     */
    public Integer getNumberOfSite() {
        return this.numberOfSite;
    }

    /**
     * 设置 桌号数
     */
    public void setNumberOfSite(Integer numberOfSite) {
        this.numberOfSite = numberOfSite;
    }

    public String getSeatMemo() {
        return seatMemo;
    }

    public void setSeatMemo(String seatMemo) {
        this.seatMemo = seatMemo;
    }

    /**
     * 获取 是否要取餐通知 1为需要 0为不需要
     */
    public Short getTakeMealReminder() {
        return this.takeMealReminder;
    }

    /**
     * 设置 是否要取餐通知 1为需要 0为不需要
     */
    public void setTakeMealReminder(Short takeMealReminder) {
        this.takeMealReminder = takeMealReminder;
    }

    public Short getDiningStyle() {
        return diningStyle;
    }

    public void setDiningStyle(Short diningStyle) {
        this.diningStyle = diningStyle;
    }

    /**
     * 获取 是否要外卖 1为需要 0为不需要
     */
    public Short getTakeOut() {
        return this.takeOut;
    }

    /**
     * 设置 是否要外卖 1为需要 0为不需要
     */
    public void setTakeOut(Short takeOut) {
        this.takeOut = takeOut;
    }

    public List<String> getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(List<String> menuUrl) {
        this.menuUrl = menuUrl;
    }

    /**
     * 获取 店铺菜单
     */
    public String getStoreMenuIds() {
        return this.storeMenuIds;
    }

    /**
     * 设置 店铺菜单
     */
    public void setStoreMenuIds(String storeMenuIds) {
        this.storeMenuIds = storeMenuIds;
    }

    public String getStoreMenuMemo() {
        return storeMenuMemo;
    }

    public void setStoreMenuMemo(String storeMenuMemo) {
        this.storeMenuMemo = storeMenuMemo;
    }

    /**
     * 获取 手机型号
     */
    public String getMobileModel() {
        return this.mobileModel;
    }

    /**
     * 设置 手机型号
     */
    public void setMobileModel(String mobileModel) {
        this.mobileModel = mobileModel;
    }

    public Integer getHallEatPayType() {
        return hallEatPayType;
    }

    public void setHallEatPayType(Integer hallEatPayType) {
        this.hallEatPayType = hallEatPayType;
    }

    /**
     * 获取 操作方式 21为添加 22为修改
     */
    public Short getOperation() {
        return this.operation;
    }

    /**
     * 设置 操作方式 21为添加 22为修改
     */
    public void setOperation(Short operation) {
        this.operation = operation;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public ShopInfoReq() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.auditId);
        dest.writeString(this.marketerId);
        dest.writeValue(this.operation);
        dest.writeString(this.province);
        dest.writeString(this.city);
        dest.writeString(this.district);
        dest.writeValue(this.hallEatPayType);
        dest.writeString(this.storeAddress);
        dest.writeString(this.storeOpeningHour);
        dest.writeValue(this.posNumber);
        dest.writeValue(this.receiptNumber);
        dest.writeSerializable(this.teeFee);
        dest.writeValue(this.teeFeeDiscount);
        dest.writeValue(this.numberOfSite);
        dest.writeString(this.seatMemo);
        dest.writeValue(this.diningStyle);
        dest.writeValue(this.takeMealReminder);
        dest.writeValue(this.takeOut);
        dest.writeStringList(this.menuUrl);
        dest.writeString(this.storeMenuIds);
        dest.writeString(this.storeMenuMemo);
        dest.writeString(this.mobileModel);
    }

    protected ShopInfoReq(Parcel in) {
        this.auditId = in.readString();
        this.marketerId = in.readString();
        this.operation = (Short) in.readValue(Short.class.getClassLoader());
        this.province = in.readString();
        this.city = in.readString();
        this.district = in.readString();
        this.hallEatPayType = (Integer) in.readValue(Integer.class.getClassLoader());
        this.storeAddress = in.readString();
        this.storeOpeningHour = in.readString();
        this.posNumber = (Integer) in.readValue(Integer.class.getClassLoader());
        this.receiptNumber = (Integer) in.readValue(Integer.class.getClassLoader());
        this.teeFee = (BigDecimal) in.readSerializable();
        this.teeFeeDiscount = (Short) in.readValue(Short.class.getClassLoader());
        this.numberOfSite = (Integer) in.readValue(Integer.class.getClassLoader());
        this.seatMemo = in.readString();
        this.diningStyle = (Short) in.readValue(Short.class.getClassLoader());
        this.takeMealReminder = (Short) in.readValue(Short.class.getClassLoader());
        this.takeOut = (Short) in.readValue(Short.class.getClassLoader());
        this.menuUrl = in.createStringArrayList();
        this.storeMenuIds = in.readString();
        this.storeMenuMemo = in.readString();
        this.mobileModel = in.readString();
    }

    public static final Creator<ShopInfoReq> CREATOR = new Creator<ShopInfoReq>() {
        @Override
        public ShopInfoReq createFromParcel(Parcel source) {
            return new ShopInfoReq(source);
        }

        @Override
        public ShopInfoReq[] newArray(int size) {
            return new ShopInfoReq[size];
        }
    };
}
