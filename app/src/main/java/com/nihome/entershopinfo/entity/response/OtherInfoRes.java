package com.nihome.entershopinfo.entity.response;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Description:
 * author: 格式化油条
 * date:  2017/7/20.
 */
public class OtherInfoRes implements Parcelable {

    /**
     * 资料id
     */
    public String auditId;

    /**
     * 市场人员id
     */
    public String marketerId;

    /**
     * 市场人员姓名
     */
    public String marketerName;

    /**
     * 提交时间
     */
    public String dateCreated;

    /**
     * 店铺来源   1 小程序  2  H5
     */
    public Short storeSource;

    /**
     * 店铺合照
     */
    public String photoId;

    /**
     * 店铺合照
     */
    public String photo;


    /**
     * 第一次验证金额
     */
    public String amountFirstVerifyPhotoId;

    /**
     * 第一次验证金额
     */
    public String amountFirstVerifyPhoto;


    /**
     * 第二次验证金额
     */
    public String amountSecondVerifyPhotoId;

    /**
     * 第二次验证金额
     */
    public String amountSecondVerifyPhoto;


    /**
     * 市场人员提交资料审核状态
     */
    public Short auditStatus;

    /**
     * 市场管理员审核BD提交资料备注
     */
    public String auditBDMemo;

    /**
     * 审核员审核市场管理员提交资料备注
     */
    public String auditBDAdminMemo;

    /**
     * 资料填写进度
     */
    public Short schedule;

    /**
     * 小程序账号
     */
    public String miniProgramsAccount;

    /**
     * 小程序密码
     */
    public String miniProgramsPassword;

    /**
     * 商户账号
     */
    public String wechatStoreAccount;

    /**
     * 商户账号密码
     */
    public String wechatStorePassword;

    /**
     * 达达账号
     */
    public String dadaAccount;

    /**
     * 达达密码
     */
    public String dadaPassword;



    /**
     * 获取 资料id
     */
    public String getAuditId() {
        return this.auditId;
    }

    /**
     * 设置 资料id
     */
    public void setAuditId(String auditId) {
        this.auditId = auditId;
    }

    /**
     * 获取 市场人员id
     */
    public String getMarketerId() {
        return this.marketerId;
    }

    /**
     * 设置 市场人员id
     */
    public void setMarketerId(String marketerId) {
        this.marketerId = marketerId;
    }

    /**
     * 获取 市场人员姓名
     */
    public String getMarketerName() {
        return this.marketerName;
    }

    /**
     * 设置 市场人员姓名
     */
    public void setMarketerName(String marketerName) {
        this.marketerName = marketerName;
    }

    /**
     * 获取 提交时间
     */
    public String getDateCreated() {
        return this.dateCreated;
    }

    /**
     * 设置 提交时间
     */
    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * 获取 店铺来源
     */
    public Short getStoreSource() {
        return this.storeSource;
    }

    /**
     * 设置 店铺来源
     */
    public void setStoreSource(Short storeSource) {
        this.storeSource = storeSource;
    }

    /**
     * 获取 店铺合照
     */
    public String getPhoto() {
        return this.photo;
    }

    /**
     * 设置 店铺合照
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     * 获取 第一次验证金额
     */
    public String getAmountFirstVerifyPhoto() {
        return this.amountFirstVerifyPhoto;
    }

    /**
     * 设置 第一次验证金额
     */
    public void setAmountFirstVerifyPhoto(String amountFirstVerifyPhoto) {
        this.amountFirstVerifyPhoto = amountFirstVerifyPhoto;
    }

    /**
     * 获取 第二次验证金额
     */
    public String getAmountSecondVerifyPhoto() {
        return this.amountSecondVerifyPhoto;
    }

    /**
     * 设置 第二次验证金额
     */
    public void setAmountSecondVerifyPhoto(String amountSecondVerifyPhoto) {
        this.amountSecondVerifyPhoto = amountSecondVerifyPhoto;
    }

    /**
     * 获取 市场人员提交资料审核状态
     */
    public Short getAuditStatus() {
        return this.auditStatus;
    }

    /**
     * 设置 市场人员提交资料审核状态
     */
    public void setAuditStatus(Short auditStatus) {
        this.auditStatus = auditStatus;
    }

    /**
     * 获取 市场管理员审核BD提交资料备注
     */
    public String getAuditBDMemo() {
        return this.auditBDMemo;
    }

    /**
     * 设置 市场管理员审核BD提交资料备注
     */
    public void setAuditBDMemo(String auditBDMemo) {
        this.auditBDMemo = auditBDMemo;
    }

    /**
     * 获取 审核员审核市场管理员提交资料备注
     */
    public String getAuditBDAdminMemo() {
        return this.auditBDAdminMemo;
    }

    /**
     * 设置 审核员审核市场管理员提交资料备注
     */
    public void setAuditBDAdminMemo(String auditBDAdminMemo) {
        this.auditBDAdminMemo = auditBDAdminMemo;
    }

    /**
     * 获取 资料填写进度
     */
    public Short getSchedule() {
        return this.schedule;
    }

    /**
     * 设置 资料填写进度
     */
    public void setSchedule(Short schedule) {
        this.schedule = schedule;
    }

    /**
     * 获取 小程序账号
     */
    public String getMiniProgramsAccount() {
        return this.miniProgramsAccount;
    }

    /**
     * 设置 小程序账号
     */
    public void setMiniProgramsAccount(String miniProgramsAccount) {
        this.miniProgramsAccount = miniProgramsAccount;
    }

    /**
     * 获取 小程序密码
     */
    public String getMiniProgramsPassword() {
        return this.miniProgramsPassword;
    }

    /**
     * 设置 小程序密码
     */
    public void setMiniProgramsPassword(String miniProgramsPassword) {
        this.miniProgramsPassword = miniProgramsPassword;
    }

    /**
     * 获取 商户账号
     */
    public String getWechatStoreAccount() {
        return this.wechatStoreAccount;
    }

    /**
     * 设置 商户账号
     */
    public void setWechatStoreAccount(String wechatStoreAccount) {
        this.wechatStoreAccount = wechatStoreAccount;
    }

    /**
     * 获取 商户账号密码
     */
    public String getWechatStorePassword() {
        return this.wechatStorePassword;
    }

    /**
     * 设置 商户账号密码
     */
    public void setWechatStorePassword(String wechatStorePassword) {
        this.wechatStorePassword = wechatStorePassword;
    }

    /**
     * 获取 达达账号
     */
    public String getDadaAccount() {
        return this.dadaAccount;
    }

    /**
     * 设置 达达账号
     */
    public void setDadaAccount(String dadaAccount) {
        this.dadaAccount = dadaAccount;
    }

    /**
     * 获取 达达密码
     */
    public String getDadaPassword() {
        return this.dadaPassword;
    }

    /**
     * 设置 达达密码
     */
    public void setDadaPassword(String dadaPassword) {
        this.dadaPassword = dadaPassword;
    }


    /**
     * 获取 店铺合照
     */
    public String getPhotoId() {
        return this.photoId;
    }

    /**
     * 设置 店铺合照
     */
    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    /**
     * 获取 第一次验证金额
     */
    public String getAmountFirstVerifyPhotoId() {
        return this.amountFirstVerifyPhotoId;
    }

    /**
     * 设置 第一次验证金额
     */
    public void setAmountFirstVerifyPhotoId(String amountFirstVerifyPhotoId) {
        this.amountFirstVerifyPhotoId = amountFirstVerifyPhotoId;
    }

    /**
     * 获取 第二次验证金额
     */
    public String getAmountSecondVerifyPhotoId() {
        return this.amountSecondVerifyPhotoId;
    }

    /**
     * 设置 第二次验证金额
     */
    public void setAmountSecondVerifyPhotoId(String amountSecondVerifyPhotoId) {
        this.amountSecondVerifyPhotoId = amountSecondVerifyPhotoId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.auditId);
        dest.writeString(this.marketerId);
        dest.writeString(this.marketerName);
        dest.writeString(this.dateCreated);
        dest.writeValue(this.storeSource);
        dest.writeString(this.photoId);
        dest.writeString(this.photo);
        dest.writeString(this.amountFirstVerifyPhotoId);
        dest.writeString(this.amountFirstVerifyPhoto);
        dest.writeString(this.amountSecondVerifyPhotoId);
        dest.writeString(this.amountSecondVerifyPhoto);
        dest.writeValue(this.auditStatus);
        dest.writeString(this.auditBDMemo);
        dest.writeString(this.auditBDAdminMemo);
        dest.writeValue(this.schedule);
        dest.writeString(this.miniProgramsAccount);
        dest.writeString(this.miniProgramsPassword);
        dest.writeString(this.wechatStoreAccount);
        dest.writeString(this.wechatStorePassword);
        dest.writeString(this.dadaAccount);
        dest.writeString(this.dadaPassword);
    }

    public OtherInfoRes() {
    }

    protected OtherInfoRes(Parcel in) {
        this.auditId = in.readString();
        this.marketerId = in.readString();
        this.marketerName = in.readString();
        this.dateCreated = in.readString();
        this.storeSource = (Short) in.readValue(Short.class.getClassLoader());
        this.photoId = in.readString();
        this.photo = in.readString();
        this.amountFirstVerifyPhotoId = in.readString();
        this.amountFirstVerifyPhoto = in.readString();
        this.amountSecondVerifyPhotoId = in.readString();
        this.amountSecondVerifyPhoto = in.readString();
        this.auditStatus = (Short) in.readValue(Short.class.getClassLoader());
        this.auditBDMemo = in.readString();
        this.auditBDAdminMemo = in.readString();
        this.schedule = (Short) in.readValue(Short.class.getClassLoader());
        this.miniProgramsAccount = in.readString();
        this.miniProgramsPassword = in.readString();
        this.wechatStoreAccount = in.readString();
        this.wechatStorePassword = in.readString();
        this.dadaAccount = in.readString();
        this.dadaPassword = in.readString();
    }

    public static final Parcelable.Creator<OtherInfoRes> CREATOR = new Parcelable.Creator<OtherInfoRes>() {
        @Override
        public OtherInfoRes createFromParcel(Parcel source) {
            return new OtherInfoRes(source);
        }

        @Override
        public OtherInfoRes[] newArray(int size) {
            return new OtherInfoRes[size];
        }
    };
}
