package com.nihome.entershopinfo.entity.request;

import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigDecimal;

/**
 * Description:
 * author: 格式化油条
 * date:  2017/7/19.
 */
public class SendInfoReq implements Parcelable {

    /**
     * 资料id
     */
    public String auditId;

    /**
     * 市场人员id
     */
    public String marketerId;

    /**
     * 外卖方式 10为自送 11为达达
     */
    public Short deliverType;


    /**
     * 订单是否允许切换配送方式
     */
    private Boolean enableSwitchDelivery;

    /**
     * 允许切换配送方式时优先使用计算配送费的方式 9允许切换设置的固定费用，10自送，11达达
     */
    private Short switchDeliverFeeType;

    /**
     * 允许切换配送方式时固定的配送费
     */
    private BigDecimal switchableDeliverFeeFixed;



    /**
     * 操作方式 21为添加 22为修改
     */
    public Short operation;

    /**
     * 外卖备注
     */
    public String takeOutMemo;

    /**
     * 店铺定位位置
     */
    public String location;

    /**
     * 店铺的经度
     */
    public String longitude;

    /**
     * 店铺的纬度
     */
    public String latitude;

    /**
     * 外卖起送费
     */
    public BigDecimal takeOutDeliverFee;

    /**
     * 外卖打包费
     */
    public BigDecimal packingFee;

    /**
     * 自送外卖配送费
     */
    public BigDecimal deliverFeeFixed;

    /**
     * 外卖配送最大公里数
     */
    public Double deliveryKM;

    /**
     * 外送骑手取餐起始时间间隔
     */
    public Integer startingInterval;

    /**
     * 达达手机号
     */
    public String dadaMobile;

    /**
     * 达达联系人姓名
     */
    public String dadaContactsName;

    /**
     * 达达邮箱
     */
    public String dadaEmail;

    /**
     * 达达联系人身份证
     */
    public String contactsIdentifyCard;

    /**
     * 获取 资料id
     */
    public String getAuditId() {
        return this.auditId;
    }

    /**
     * 设置 资料id
     */
    public void setAuditId(String auditId) {
        this.auditId = auditId;
    }

    /**
     * 获取 市场人员id
     */
    public String getMarketerId() {
        return this.marketerId;
    }

    /**
     * 设置 市场人员id
     */
    public void setMarketerId(String marketerId) {
        this.marketerId = marketerId;
    }

    /**
     * 获取 外卖方式 10为自送 11为达达
     */
    public Short getDeliverType() {
        return this.deliverType;
    }

    /**
     * 设置 外卖方式 10为自送 11为达达
     */
    public void setDeliverType(Short deliverType) {
        this.deliverType = deliverType;
    }

    /**
     * 获取 外卖起送费
     */
    public BigDecimal getTakeOutDeliverFee() {
        return this.takeOutDeliverFee;
    }

    /**
     * 设置 外卖起送费
     */
    public void setTakeOutDeliverFee(BigDecimal takeOutDeliverFee) {
        this.takeOutDeliverFee = takeOutDeliverFee;
    }

    public String getTakeOutMemo() {
        return takeOutMemo;
    }

    public void setTakeOutMemo(String takeOutMemo) {
        this.takeOutMemo = takeOutMemo;
    }

    /**
     * 获取 达达手机号
     */
    public String getDadaMobile() {
        return this.dadaMobile;
    }

    /**
     * 设置 达达手机号
     */
    public void setDadaMobile(String dadaMobile) {
        this.dadaMobile = dadaMobile;
    }

    /**
     * 获取 达达联系人姓名
     */
    public String getDadaContactsName() {
        return this.dadaContactsName;
    }

    /**
     * 设置 达达联系人姓名
     */
    public void setDadaContactsName(String dadaContactsName) {
        this.dadaContactsName = dadaContactsName;
    }

    /**
     * 获取 达达邮箱
     */
    public String getDadaEmail() {
        return this.dadaEmail;
    }

    /**
     * 设置 达达邮箱
     */
    public void setDadaEmail(String dadaEmail) {
        this.dadaEmail = dadaEmail;
    }

    /**
     * 获取 达达联系人身份证
     */
    public String getContactsIdentifyCard() {
        return this.contactsIdentifyCard;
    }

    /**
     * 设置 达达联系人身份证
     */
    public void setContactsIdentifyCard(String contactsIdentifyCard) {
        this.contactsIdentifyCard = contactsIdentifyCard;
    }

    /**
     * 获取 店铺定位位置
     */
    public String getLocation() {
        return this.location;
    }

    /**
     * 设置 店铺定位位置
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * 获取 店铺的经度
     */
    public String getLongitude() {
        return this.longitude;
    }

    /**
     * 设置 店铺的经度
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * 获取 店铺的纬度
     */
    public String getLatitude() {
        return this.latitude;
    }

    /**
     * 设置 店铺的纬度
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }


    /**
     * 获取 操作方式 21为添加 22为修改
     */
    public Short getOperation() {
        return this.operation;
    }

    /**
     * 设置 操作方式 21为添加 22为修改
     */
    public void setOperation(Short operation) {
        this.operation = operation;
    }

    /**
     * 获取 自送外卖配送费
     */
    public BigDecimal getDeliverFeeFixed() {
        return this.deliverFeeFixed;
    }

    /**
     * 设置 自送外卖配送费
     */
    public void setDeliverFeeFixed(BigDecimal deliverFeeFixed) {
        this.deliverFeeFixed = deliverFeeFixed;
    }

    /**
     * 获取 外卖配送最大公里数
     */
    public Double getDeliveryKM() {
        return this.deliveryKM;
    }

    /**
     * 设置 外卖配送最大公里数
     */
    public void setDeliveryKM(Double deliveryKM) {
        this.deliveryKM = deliveryKM;
    }

    /**
     * 获取 外送骑手取餐起始时间间隔
     */
    public Integer getStartingInterval() {
        return this.startingInterval;
    }

    /**
     * 设置 外送骑手取餐起始时间间隔
     */
    public void setStartingInterval(Integer startingInterval) {
        this.startingInterval = startingInterval;
    }

    /**
     * 获取 外卖打包费
     */
    public BigDecimal getPackingFee() {
        return this.packingFee;
    }

    /**
     * 设置 外卖打包费
     */
    public void setPackingFee(BigDecimal packingFee) {
        this.packingFee = packingFee;
    }


    public Boolean getEnableSwitchDelivery() {
        return enableSwitchDelivery;
    }

    public void setEnableSwitchDelivery(Boolean enableSwitchDelivery) {
        this.enableSwitchDelivery = enableSwitchDelivery;
    }

    public Short getSwitchDeliverFeeType() {
        return switchDeliverFeeType;
    }

    public void setSwitchDeliverFeeType(Short switchDeliverFeeType) {
        this.switchDeliverFeeType = switchDeliverFeeType;
    }

    public BigDecimal getSwitchableDeliverFeeFixed() {
        return switchableDeliverFeeFixed;
    }

    public void setSwitchableDeliverFeeFixed(BigDecimal switchableDeliverFeeFixed) {
        this.switchableDeliverFeeFixed = switchableDeliverFeeFixed;
    }

    public SendInfoReq() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.auditId);
        dest.writeString(this.marketerId);
        dest.writeValue(this.deliverType);
        dest.writeValue(this.enableSwitchDelivery);
        dest.writeValue(this.switchDeliverFeeType);
        dest.writeSerializable(this.switchableDeliverFeeFixed);
        dest.writeValue(this.operation);
        dest.writeString(this.takeOutMemo);
        dest.writeString(this.location);
        dest.writeString(this.longitude);
        dest.writeString(this.latitude);
        dest.writeSerializable(this.takeOutDeliverFee);
        dest.writeSerializable(this.packingFee);
        dest.writeSerializable(this.deliverFeeFixed);
        dest.writeValue(this.deliveryKM);
        dest.writeValue(this.startingInterval);
        dest.writeString(this.dadaMobile);
        dest.writeString(this.dadaContactsName);
        dest.writeString(this.dadaEmail);
        dest.writeString(this.contactsIdentifyCard);
    }

    protected SendInfoReq(Parcel in) {
        this.auditId = in.readString();
        this.marketerId = in.readString();
        this.deliverType = (Short) in.readValue(Short.class.getClassLoader());
        this.enableSwitchDelivery = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.switchDeliverFeeType = (Short) in.readValue(Short.class.getClassLoader());
        this.switchableDeliverFeeFixed = (BigDecimal) in.readSerializable();
        this.operation = (Short) in.readValue(Short.class.getClassLoader());
        this.takeOutMemo = in.readString();
        this.location = in.readString();
        this.longitude = in.readString();
        this.latitude = in.readString();
        this.takeOutDeliverFee = (BigDecimal) in.readSerializable();
        this.packingFee = (BigDecimal) in.readSerializable();
        this.deliverFeeFixed = (BigDecimal) in.readSerializable();
        this.deliveryKM = (Double) in.readValue(Double.class.getClassLoader());
        this.startingInterval = (Integer) in.readValue(Integer.class.getClassLoader());
        this.dadaMobile = in.readString();
        this.dadaContactsName = in.readString();
        this.dadaEmail = in.readString();
        this.contactsIdentifyCard = in.readString();
    }

    public static final Creator<SendInfoReq> CREATOR = new Creator<SendInfoReq>() {
        @Override
        public SendInfoReq createFromParcel(Parcel source) {
            return new SendInfoReq(source);
        }

        @Override
        public SendInfoReq[] newArray(int size) {
            return new SendInfoReq[size];
        }
    };
}
