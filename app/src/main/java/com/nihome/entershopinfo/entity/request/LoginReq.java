package com.nihome.entershopinfo.entity.request;

/**
 * Created by Carson on 2017/7/21.
 */
public class LoginReq {
    private String mobile;
    private String password;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
