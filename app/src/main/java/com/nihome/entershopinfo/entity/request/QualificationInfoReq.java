package com.nihome.entershopinfo.entity.request;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Description: 收集系统-资质信息请求参数
 * author: 格式化油条
 * date:  2017/7/19.
 */
public class QualificationInfoReq implements Parcelable {

    /**
     * 资料id
     */
    public String auditId;

    /**
     * 市场人员id
     */
    public String marketerId;

    /**
     * 操作方式 21为添加 22为修改
     */
    public Short operation;

    /**
     * 营业执照
     */
    public String businessLicenseFileId;

    /**
     * 组织代码代码证
     */
    public String organizationCodeFileId;
    /**
     * 税务登记证
     */
    public String taxRegistrationFileId;

    /**
     * 餐饮许可证
     */
    public String foodPermitFileId;

    /**
     * 营业执照
     */
    public String businessLicenseUrl;

    /**
     * 组织代码代码证
     */
    public String organizationCodeUrl;

    /**
     * 税务登记证
     */
    public String taxRegistrationUrl;

    /**
     * 餐饮许可证
     */
    public String foodPermitUrl;



    /**
     * 获取 资料id
     */
    public String getAuditId() {
        return this.auditId;
    }

    /**
     * 设置 资料id
     */
    public void setAuditId(String auditId) {
        this.auditId = auditId;
    }

    /**
     * 获取 市场人员id
     */
    public String getMarketerId() {
        return this.marketerId;
    }

    /**
     * 设置 市场人员id
     */
    public void setMarketerId(String marketerId) {
        this.marketerId = marketerId;
    }

    /**
     * 获取 操作方式 21为添加 22为修改
     */
    public Short getOperation() {
        return this.operation;
    }

    /**
     * 设置 操作方式 21为添加 22为修改
     */
    public void setOperation(Short operation) {
        this.operation = operation;
    }

    /**
     * 获取 营业执照
     */
    public String getBusinessLicenseFileId() {
        return this.businessLicenseFileId;
    }

    /**
     * 设置 营业执照
     */
    public void setBusinessLicenseFileId(String businessLicenseFileId) {
        this.businessLicenseFileId = businessLicenseFileId;
    }

    /**
     * 获取 组织代码代码证
     */
    public String getOrganizationCodeFileId() {
        return this.organizationCodeFileId;
    }

    /**
     * 设置 组织代码代码证
     */
    public void setOrganizationCodeFileId(String organizationCodeFileId) {
        this.organizationCodeFileId = organizationCodeFileId;
    }

    /**
     * 获取 税务登记证
     */
    public String getTaxRegistrationFileId() {
        return this.taxRegistrationFileId;
    }

    /**
     * 设置 税务登记证
     */
    public void setTaxRegistrationFileId(String taxRegistrationFileId) {
        this.taxRegistrationFileId = taxRegistrationFileId;
    }

    /**
     * 获取 餐饮许可证
     */
    public String getFoodPermitFileId() {
        return this.foodPermitFileId;
    }

    /**
     * 设置 餐饮许可证
     */
    public void setFoodPermitFileId(String foodPermitFileId) {
        this.foodPermitFileId = foodPermitFileId;
    }

    public String getBusinessLicenseUrl() {
        return businessLicenseUrl;
    }

    public void setBusinessLicenseUrl(String businessLicenseUrl) {
        this.businessLicenseUrl = businessLicenseUrl;
    }

    public String getOrganizationCodeUrl() {
        return organizationCodeUrl;
    }

    public void setOrganizationCodeUrl(String organizationCodeUrl) {
        this.organizationCodeUrl = organizationCodeUrl;
    }

    public String getTaxRegistrationUrl() {
        return taxRegistrationUrl;
    }

    public void setTaxRegistrationUrl(String taxRegistrationUrl) {
        this.taxRegistrationUrl = taxRegistrationUrl;
    }

    public String getFoodPermitUrl() {
        return foodPermitUrl;
    }

    public void setFoodPermitUrl(String foodPermitUrl) {
        this.foodPermitUrl = foodPermitUrl;
    }

    public QualificationInfoReq() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.auditId);
        dest.writeString(this.marketerId);
        dest.writeValue(this.operation);
        dest.writeString(this.businessLicenseFileId);
        dest.writeString(this.organizationCodeFileId);
        dest.writeString(this.taxRegistrationFileId);
        dest.writeString(this.foodPermitFileId);
        dest.writeString(this.businessLicenseUrl);
        dest.writeString(this.organizationCodeUrl);
        dest.writeString(this.taxRegistrationUrl);
        dest.writeString(this.foodPermitUrl);
    }

    protected QualificationInfoReq(Parcel in) {
        this.auditId = in.readString();
        this.marketerId = in.readString();
        this.operation = (Short) in.readValue(Short.class.getClassLoader());
        this.businessLicenseFileId = in.readString();
        this.organizationCodeFileId = in.readString();
        this.taxRegistrationFileId = in.readString();
        this.foodPermitFileId = in.readString();
        this.businessLicenseUrl = in.readString();
        this.organizationCodeUrl = in.readString();
        this.taxRegistrationUrl = in.readString();
        this.foodPermitUrl = in.readString();
    }

    public static final Creator<QualificationInfoReq> CREATOR = new Creator<QualificationInfoReq>() {
        @Override
        public QualificationInfoReq createFromParcel(Parcel source) {
            return new QualificationInfoReq(source);
        }

        @Override
        public QualificationInfoReq[] newArray(int size) {
            return new QualificationInfoReq[size];
        }
    };
}
