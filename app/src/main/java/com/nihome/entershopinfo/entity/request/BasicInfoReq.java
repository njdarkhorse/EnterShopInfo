package com.nihome.entershopinfo.entity.request;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Description: 收集系统 - 基本信息请求参数
 * author: 格式化油条
 * date:  2017/7/19.
 */
public class BasicInfoReq implements Parcelable {

    /**
     * 资料id
     */
    public String auditId;

    /**
     * 市场人员id
     */
    public String marketerId;

    /**
     * 操作方式 21为添加 22为修改
     */
    public Short operation;

    /**
     * 店铺名称
     */
    public String storeName;

    /**
     * 店铺联系人名称
     */
    public String contactsName;

    /**
     * 店铺联系人电话
     */
    public String contactsMobile;

    /**
     * 店铺服务行业 1为餐饮行业 2为其他
     */
    public Short storeIndustry;

    /**
     * 营业执照类型 1为个人 2为公司
     */
    public Short businessLicenseType;

    private String businessLicensePhoto;


    private String parentAuditId;

    private Integer partnerType;

    private Integer subStoreCashType;



    public String getBusinessLicensePhoto() {
        return businessLicensePhoto;
    }

    public void setBusinessLicensePhoto(String businessLicensePhoto) {
        this.businessLicensePhoto = businessLicensePhoto;
    }

    /**
     * 获取 资料id
     */
    public String getAuditId() {
        return this.auditId;
    }

    /**
     * 设置 资料id
     */
    public void setAuditId(String auditId) {
        this.auditId = auditId;
    }

    /**
     * 获取 市场人员id
     */
    public String getMarketerId() {
        return this.marketerId;
    }

    /**
     * 设置 市场人员id
     */
    public void setMarketerId(String marketerId) {
        this.marketerId = marketerId;
    }

    /**
     * 获取 店铺名称
     */
    public String getStoreName() {
        return this.storeName;
    }

    /**
     * 设置 店铺名称
     */
    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    /**
     * 获取 店铺联系人名称
     */
    public String getContactsName() {
        return this.contactsName;
    }

    /**
     * 设置 店铺联系人名称
     */
    public void setContactsName(String contactsName) {
        this.contactsName = contactsName;
    }

    /**
     * 获取 店铺联系人电话
     */
    public String getContactsMobile() {
        return this.contactsMobile;
    }

    /**
     * 设置 店铺联系人电话
     */
    public void setContactsMobile(String contactsMobile) {
        this.contactsMobile = contactsMobile;
    }

    /**
     * 获取 店铺服务行业 1为餐饮行业 2为其他
     */
    public Short getStoreIndustry() {
        return this.storeIndustry;
    }

    /**
     * 设置 店铺服务行业 1为餐饮行业 2为其他
     */
    public void setStoreIndustry(Short storeIndustry) {
        this.storeIndustry = storeIndustry;
    }

    /**
     * 获取 营业执照类型 1为个人 2为公司
     */
    public Short getBusinessLicenseType() {
        return this.businessLicenseType;
    }

    /**
     * 设置 营业执照类型 1为个人 2为公司
     */
    public void setBusinessLicenseType(Short businessLicenseType) {
        this.businessLicenseType = businessLicenseType;
    }

    /**
     * 获取 操作方式 21为添加 22为修改
     */
    public Short getOperation() {
        return this.operation;
    }

    /**
     * 设置 操作方式 21为添加 22为修改
     */
    public void setOperation(Short operation) {
        this.operation = operation;
    }

    public String getParentAuditId() {
        return parentAuditId;
    }

    public void setParentAuditId(String parentAuditId) {
        this.parentAuditId = parentAuditId;
    }

    public Integer getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(Integer partnerType) {
        this.partnerType = partnerType;
    }

    public Integer getSubStoreCashType() {
        return subStoreCashType;
    }

    public void setSubStoreCashType(Integer subStoreCashType) {
        this.subStoreCashType = subStoreCashType;
    }

    public BasicInfoReq() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.auditId);
        dest.writeString(this.marketerId);
        dest.writeValue(this.operation);
        dest.writeString(this.storeName);
        dest.writeString(this.contactsName);
        dest.writeString(this.contactsMobile);
        dest.writeValue(this.storeIndustry);
        dest.writeValue(this.businessLicenseType);
        dest.writeString(this.businessLicensePhoto);
        dest.writeString(this.parentAuditId);
        dest.writeValue(this.partnerType);
        dest.writeValue(this.subStoreCashType);
    }

    protected BasicInfoReq(Parcel in) {
        this.auditId = in.readString();
        this.marketerId = in.readString();
        this.operation = (Short) in.readValue(Short.class.getClassLoader());
        this.storeName = in.readString();
        this.contactsName = in.readString();
        this.contactsMobile = in.readString();
        this.storeIndustry = (Short) in.readValue(Short.class.getClassLoader());
        this.businessLicenseType = (Short) in.readValue(Short.class.getClassLoader());
        this.businessLicensePhoto = in.readString();
        this.parentAuditId = in.readString();
        this.partnerType = (Integer) in.readValue(Integer.class.getClassLoader());
        this.subStoreCashType = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<BasicInfoReq> CREATOR = new Creator<BasicInfoReq>() {
        @Override
        public BasicInfoReq createFromParcel(Parcel source) {
            return new BasicInfoReq(source);
        }

        @Override
        public BasicInfoReq[] newArray(int size) {
            return new BasicInfoReq[size];
        }
    };
}
