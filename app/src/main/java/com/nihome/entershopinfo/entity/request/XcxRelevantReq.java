package com.nihome.entershopinfo.entity.request;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Description:
 * author: 格式化油条
 * date:  2017/7/19.
 */
public class XcxRelevantReq implements Parcelable {

    /**
     * 资料id
     */
    public String auditId;

    /**
     * 市场人员id
     */
    public String marketerId;

    /**
     * 操作方式 21为添加 22为修改
     */
    public Short operation;

    /**
     * 小程序名称，以 , 分隔
     */
    public String miniProgramsName;

    /**
     * 小程序介绍
     */
    public String miniProgramsIntroduce;

    /**
     * 小程序标签，以 , 分隔
     */
    public String miniProgramsLabel;

    /**
     * 店铺理念
     */
    public String storeIdea;

    /**
     * 外卖说明
     */
    public String takeOutMemo;

    /**
     * 店铺logo
     */
    public String storeLogoId;

    /**
     * 店铺logo URL
     */
    public String storeLogo;


    /**
     * 获取 资料id
     */
    public String getAuditId() {
        return this.auditId;
    }

    /**
     * 设置 资料id
     */
    public void setAuditId(String auditId) {
        this.auditId = auditId;
    }

    /**
     * 获取 市场人员id
     */
    public String getMarketerId() {
        return this.marketerId;
    }

    /**
     * 设置 市场人员id
     */
    public void setMarketerId(String marketerId) {
        this.marketerId = marketerId;
    }

    /**
     * 获取 小程序名称，以 , 分隔
     */
    public String getMiniProgramsName() {
        return this.miniProgramsName;
    }

    /**
     * 设置 小程序名称，以 , 分隔
     */
    public void setMiniProgramsName(String miniProgramsName) {
        this.miniProgramsName = miniProgramsName;
    }

    /**
     * 获取 操作方式 21为添加 22为修改
     */
    public Short getOperation() {
        return this.operation;
    }

    /**
     * 设置 操作方式 21为添加 22为修改
     */
    public void setOperation(Short operation) {
        this.operation = operation;
    }

    /**
     * 获取 小程序介绍
     */
    public String getMiniProgramsIntroduce() {
        return this.miniProgramsIntroduce;
    }

    /**
     * 设置 小程序介绍
     */
    public void setMiniProgramsIntroduce(String miniProgramsIntroduce) {
        this.miniProgramsIntroduce = miniProgramsIntroduce;
    }

    /**
     * 获取 小程序标签，以 , 分隔
     */
    public String getMiniProgramsLabel() {
        return this.miniProgramsLabel;
    }

    /**
     * 设置 小程序标签，以 , 分隔
     */
    public void setMiniProgramsLabel(String miniProgramsLabel) {
        this.miniProgramsLabel = miniProgramsLabel;
    }

    /**
     * 获取 店铺理念
     */
    public String getStoreIdea() {
        return this.storeIdea;
    }

    /**
     * 设置 店铺理念
     */
    public void setStoreIdea(String storeIdea) {
        this.storeIdea = storeIdea;
    }

    /**
     * 获取 外卖说明
     */
    public String getTakeOutMemo() {
        return this.takeOutMemo;
    }

    /**
     * 设置 外卖说明
     */
    public void setTakeOutMemo(String takeOutMemo) {
        this.takeOutMemo = takeOutMemo;
    }

    /**
     * 获取 店铺logo
     */
    public String getStoreLogoId() {
        return this.storeLogoId;
    }

    /**
     * 设置 店铺logo
     */
    public void setStoreLogoId(String storeLogoId) {
        this.storeLogoId = storeLogoId;
    }

    public String getStoreLogo() {
        return storeLogo;
    }

    public void setStoreLogo(String storeLogo) {
        this.storeLogo = storeLogo;
    }

    public XcxRelevantReq() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.auditId);
        dest.writeString(this.marketerId);
        dest.writeValue(this.operation);
        dest.writeString(this.miniProgramsName);
        dest.writeString(this.miniProgramsIntroduce);
        dest.writeString(this.miniProgramsLabel);
        dest.writeString(this.storeIdea);
        dest.writeString(this.takeOutMemo);
        dest.writeString(this.storeLogoId);
        dest.writeString(this.storeLogo);
    }

    protected XcxRelevantReq(Parcel in) {
        this.auditId = in.readString();
        this.marketerId = in.readString();
        this.operation = (Short) in.readValue(Short.class.getClassLoader());
        this.miniProgramsName = in.readString();
        this.miniProgramsIntroduce = in.readString();
        this.miniProgramsLabel = in.readString();
        this.storeIdea = in.readString();
        this.takeOutMemo = in.readString();
        this.storeLogoId = in.readString();
        this.storeLogo = in.readString();
    }

    public static final Creator<XcxRelevantReq> CREATOR = new Creator<XcxRelevantReq>() {
        @Override
        public XcxRelevantReq createFromParcel(Parcel source) {
            return new XcxRelevantReq(source);
        }

        @Override
        public XcxRelevantReq[] newArray(int size) {
            return new XcxRelevantReq[size];
        }
    };
}
