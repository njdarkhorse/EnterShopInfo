package com.nihome.entershopinfo.entity.response;

/**
 * Created by Carson on 2017/5/24.
 */
public class ResponseBase<T>  {
    private String resultCode;

    private String msg;

    private T result;

    private long size;

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }
}
