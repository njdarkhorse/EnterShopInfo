package com.nihome.entershopinfo.entity.request;

/**
 * Created by Carson on 2017/7/26.
 */
public class MoneyVoucherInfoReq {
    private String auditId;
    private String firstPhotoId;
    private String secondPhotoId;

    public String getAuditId() {
        return auditId;
    }

    public void setAuditId(String auditId) {
        this.auditId = auditId;
    }

    public String getFirstPhotoId() {
        return firstPhotoId;
    }

    public void setFirstPhotoId(String firstPhotoId) {
        this.firstPhotoId = firstPhotoId;
    }

    public String getSecondPhotoId() {
        return secondPhotoId;
    }

    public void setSecondPhotoId(String secondPhotoId) {
        this.secondPhotoId = secondPhotoId;
    }
}
