package com.nihome.entershopinfo.entity;

import com.google.gson.Gson;
import com.nihome.entershopinfo.application.SysApplication;
import com.nihome.entershopinfo.utils.FileUtils;
import com.nihome.entershopinfo.utils.Log;

/**
 * Created by Carson on 2017/7/21.
 */
public class User {
    private String userId;
    private String userName;
    private String mobile;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }


    public boolean cache(){
        Log.e("User", "==> cache............... ");
        boolean result   = false;
        String  dirPath  = SysApplication.getInstance().getNiHomeCacheDir();
        String  filePath = SysApplication.getInstance().getLoginUserCacheFilePath();
        if (!FileUtils.isFolderExist(dirPath)) {
            FileUtils.makeDirs(dirPath);
        }
        if (FileUtils.isFileExist(filePath)) {
            FileUtils.deleteFile(filePath);
        }
        FileUtils.writeFile(filePath, this.toString());
        Log.e("User", "==> mUser= " + this.toString());
        result = true;
        return result;
    }

    public static User fromJson(String userJsonString) {
        Gson gson = new Gson();
        return gson.fromJson(userJsonString, User.class);
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this, User.class);
    }
}
