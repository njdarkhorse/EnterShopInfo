package com.nihome.entershopinfo.entity.response;

/**
 * Created by Carson on 2017/7/21.
 */
public class LoginRes {
    private String marketerId;
    private String mobile;
    private String name;

    public String getMarketerId() {
        return marketerId;
    }

    public void setMarketerId(String marketerId) {
        this.marketerId = marketerId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
