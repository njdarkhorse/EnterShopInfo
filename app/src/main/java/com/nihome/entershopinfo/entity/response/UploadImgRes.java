package com.nihome.entershopinfo.entity.response;

/**
 * Created by Carson on 2017/7/21.
 */
public class UploadImgRes {
    private String fileId;
    private String fileUrl;

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }
}
