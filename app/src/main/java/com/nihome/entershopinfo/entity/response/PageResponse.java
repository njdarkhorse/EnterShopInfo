package com.nihome.entershopinfo.entity.response;

import java.util.List;

/**
 * Created by Carson on 2017/7/24.
 */
public class PageResponse {
    private Integer currentPage;
    private Integer totalPage;
    private Integer sumNumber;
    private List<ExpandsLogRes> auditOtherInfoResponseDTOList;

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public Integer getSumNumber() {
        return sumNumber;
    }

    public void setSumNumber(Integer sumNumber) {
        this.sumNumber = sumNumber;
    }

    public List<ExpandsLogRes> getAuditOtherInfoResponseDTOList() {
        return auditOtherInfoResponseDTOList;
    }

    public void setAuditOtherInfoResponseDTOList(List<ExpandsLogRes> auditOtherInfoResponseDTOList) {
        this.auditOtherInfoResponseDTOList = auditOtherInfoResponseDTOList;
    }
}
