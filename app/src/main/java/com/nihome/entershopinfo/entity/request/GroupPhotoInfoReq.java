package com.nihome.entershopinfo.entity.request;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Description:
 * author: 格式化油条
 * date:  2017/7/19.
 */
public class GroupPhotoInfoReq implements Parcelable {

    /**
     * 资料id
     */
    public String auditId;

    /**
     * 市场人员id
     */
    public String marketerId;

    /**
     * 操作方式 21为添加 22为修改
     */
    public Short operation;

    /**
     * 合照url
     */
    public String groupPhotoId;

    /**
     * 获取 资料id
     */
    public String getAuditId() {
        return this.auditId;
    }

    /**
     * 设置 资料id
     */
    public void setAuditId(String auditId) {
        this.auditId = auditId;
    }

    /**
     * 获取 市场人员id
     */
    public String getMarketerId() {
        return this.marketerId;
    }

    /**
     * 设置 市场人员id
     */
    public void setMarketerId(String marketerId) {
        this.marketerId = marketerId;
    }


    /**
     * 获取 操作方式 21为添加 22为修改
     */
    public Short getOperation() {
        return this.operation;
    }

    /**
     * 设置 操作方式 21为添加 22为修改
     */
    public void setOperation(Short operation) {
        this.operation = operation;
    }

    /**
     * 获取 合照url
     */
    public String getGroupPhotoId() {
        return this.groupPhotoId;
    }

    /**
     * 设置 合照url
     */
    public void setGroupPhotoId(String groupPhotoId) {
        this.groupPhotoId = groupPhotoId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.auditId);
        dest.writeString(this.marketerId);
        dest.writeValue(this.operation);
        dest.writeString(this.groupPhotoId);
    }

    public GroupPhotoInfoReq() {
    }

    protected GroupPhotoInfoReq(Parcel in) {
        this.auditId = in.readString();
        this.marketerId = in.readString();
        this.operation = (Short) in.readValue(Short.class.getClassLoader());
        this.groupPhotoId = in.readString();
    }

    public static final Parcelable.Creator<GroupPhotoInfoReq> CREATOR = new Parcelable.Creator<GroupPhotoInfoReq>() {
        @Override
        public GroupPhotoInfoReq createFromParcel(Parcel source) {
            return new GroupPhotoInfoReq(source);
        }

        @Override
        public GroupPhotoInfoReq[] newArray(int size) {
            return new GroupPhotoInfoReq[size];
        }
    };
}
