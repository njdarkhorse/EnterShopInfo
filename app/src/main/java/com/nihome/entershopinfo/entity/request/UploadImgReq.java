package com.nihome.entershopinfo.entity.request;

import java.io.Serializable;

/**
 * Created by Carson on 2017/7/21.
 */
public class UploadImgReq implements Serializable{
    private String fileBase64Data;
    private String userId;

    public String getFileBase64Data() {
        return fileBase64Data;
    }

    public void setFileBase64Data(String fileBase64Data) {
        this.fileBase64Data = fileBase64Data;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
