package com.nihome.entershopinfo.entity.request;



import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigDecimal;

/**
 * Description:
 * author: 格式化油条
 * date:  2017/7/19.
 */
public class SettlementInfoReq implements Parcelable {

    /**
     * 资料id
     */
    public String auditId;

    /**
     * 市场人员id
     */
    public String marketerId;

    /**
     * 操作方式 21为添加 22为修改
     */
    public Short operation;

    /**
     * 结算费率
     */
    public BigDecimal settlementRate;

    /**
     * 结算周期
     */
    public Short settlementCycle;

    /**
     * 对公账户-开户许可证文件id
     */
    public String publicOpenVoucherId;

    /**
     * 对公账户-开户许可证url
     */
    public String publicOpenVoucherUrl;

    /**
     * 对公账户-开户许可证编码
     */
    public String publicOpenVoucherCode;


    /**
     * 对公账户-打款人姓名
     */
    public String publicTransferName;

    /**
     * 对公账户-打款人身份证
     */
    public String publicTransferIdentifyCard;

    /**
     * 对公账户-打款人银行卡号
     */
    public String publicTransferBankCard;

    /**
     * 对公账户-打款人银行卡开户行
     */
    public String publicTransferBankAccount;

    /**
     * 对私账户-打款人姓名
     */
    public String privateTransferName;

    /**
     * 对私账户-打款人身份证
     */
    public String privateTransferIdentifyCard;

    /**
     * 对私账户-打款人银行卡号
     */
    public String privateTransferBankCard;

    /**
     * 对私账户-打款人银行卡开户行
     */
    public String privateTransferBankAccount;


    private String privateTransferBankCardPhoto;
    private String privateTransferIdCardBack;
    private String privateTransferIdCardFront;
    private String publicTransferBankCardPhoto;
    private String publicTransferIdCardBack;
    private String publicTransferIdCardFront;


    public String getPrivateTransferBankCardPhoto() {
        return privateTransferBankCardPhoto;
    }

    public void setPrivateTransferBankCardPhoto(String privateTransferBankCardPhoto) {
        this.privateTransferBankCardPhoto = privateTransferBankCardPhoto;
    }

    public String getPrivateTransferIdCardBack() {
        return privateTransferIdCardBack;
    }

    public void setPrivateTransferIdCardBack(String privateTransferIdCardBack) {
        this.privateTransferIdCardBack = privateTransferIdCardBack;
    }

    public String getPrivateTransferIdCardFront() {
        return privateTransferIdCardFront;
    }

    public void setPrivateTransferIdCardFront(String privateTransferIdCardFront) {
        this.privateTransferIdCardFront = privateTransferIdCardFront;
    }

    public String getPublicTransferBankCardPhoto() {
        return publicTransferBankCardPhoto;
    }

    public void setPublicTransferBankCardPhoto(String publicTransferBankCardPhoto) {
        this.publicTransferBankCardPhoto = publicTransferBankCardPhoto;
    }

    public String getPublicTransferIdCardBack() {
        return publicTransferIdCardBack;
    }

    public void setPublicTransferIdCardBack(String publicTransferIdCardBack) {
        this.publicTransferIdCardBack = publicTransferIdCardBack;
    }

    public String getPublicTransferIdCardFront() {
        return publicTransferIdCardFront;
    }

    public void setPublicTransferIdCardFront(String publicTransferIdCardFront) {
        this.publicTransferIdCardFront = publicTransferIdCardFront;
    }

    /**
     * 获取 资料id
     */
    public String getAuditId() {
        return this.auditId;
    }

    /**
     * 设置 资料id
     */
    public void setAuditId(String auditId) {
        this.auditId = auditId;
    }

    /**
     * 获取 市场人员id
     */
    public String getMarketerId() {
        return this.marketerId;
    }

    /**
     * 设置 市场人员id
     */
    public void setMarketerId(String marketerId) {
        this.marketerId = marketerId;
    }

    /**
     * 获取 操作方式 21为添加 22为修改
     */
    public Short getOperation() {
        return this.operation;
    }

    /**
     * 设置 操作方式 21为添加 22为修改
     */
    public void setOperation(Short operation) {
        this.operation = operation;
    }

    /**
     * 获取 结算费率
     */
    public BigDecimal getSettlementRate() {
        return this.settlementRate;
    }

    /**
     * 设置 结算费率
     */
    public void setSettlementRate(BigDecimal settlementRate) {
        this.settlementRate = settlementRate;
    }

    /**
     * 获取 结算周期
     */
    public Short getSettlementCycle() {
        return this.settlementCycle;
    }

    /**
     * 设置 结算周期
     */
    public void setSettlementCycle(Short settlementCycle) {
        this.settlementCycle = settlementCycle;
    }

    /**
     * 获取 对公账户-开户许可证文件id
     */
    public String getPublicOpenVoucherId() {
        return this.publicOpenVoucherId;
    }

    /**
     * 设置 对公账户-开户许可证文件id
     */
    public void setPublicOpenVoucherId(String publicOpenVoucherId) {
        this.publicOpenVoucherId = publicOpenVoucherId;
    }

    public String getPublicOpenVoucherUrl() {
        return publicOpenVoucherUrl;
    }

    public void setPublicOpenVoucherUrl(String publicOpenVoucherUrl) {
        this.publicOpenVoucherUrl = publicOpenVoucherUrl;
    }

    /**
     * 获取 对公账户-开户许可证编码
     */
    public String getPublicOpenVoucherCode() {
        return this.publicOpenVoucherCode;
    }

    /**
     * 设置 对公账户-开户许可证编码
     */
    public void setPublicOpenVoucherCode(String publicOpenVoucherCode) {
        this.publicOpenVoucherCode = publicOpenVoucherCode;
    }

    /**
     * 获取 对公账户-打款人姓名
     */
    public String getPublicTransferName() {
        return this.publicTransferName;
    }

    /**
     * 设置 对公账户-打款人姓名
     */
    public void setPublicTransferName(String publicTransferName) {
        this.publicTransferName = publicTransferName;
    }

    /**
     * 获取 对公账户-打款人身份证
     */
    public String getPublicTransferIdentifyCard() {
        return this.publicTransferIdentifyCard;
    }

    /**
     * 设置 对公账户-打款人身份证
     */
    public void setPublicTransferIdentifyCard(String publicTransferIdentifyCard) {
        this.publicTransferIdentifyCard = publicTransferIdentifyCard;
    }

    /**
     * 获取 对公账户-打款人银行卡号
     */
    public String getPublicTransferBankCard() {
        return this.publicTransferBankCard;
    }

    /**
     * 设置 对公账户-打款人银行卡号
     */
    public void setPublicTransferBankCard(String publicTransferBankCard) {
        this.publicTransferBankCard = publicTransferBankCard;
    }

    /**
     * 获取 对公账户-打款人银行卡开户行
     */
    public String getPublicTransferBankAccount() {
        return this.publicTransferBankAccount;
    }

    /**
     * 设置 对公账户-打款人银行卡开户行
     */
    public void setPublicTransferBankAccount(String publicTransferBankAccount) {
        this.publicTransferBankAccount = publicTransferBankAccount;
    }

    /**
     * 获取 对私账户-打款人姓名
     */
    public String getPrivateTransferName() {
        return this.privateTransferName;
    }

    /**
     * 设置 对私账户-打款人姓名
     */
    public void setPrivateTransferName(String privateTransferName) {
        this.privateTransferName = privateTransferName;
    }

    /**
     * 获取 对私账户-打款人身份证
     */
    public String getPrivateTransferIdentifyCard() {
        return this.privateTransferIdentifyCard;
    }

    /**
     * 设置 对私账户-打款人身份证
     */
    public void setPrivateTransferIdentifyCard(String privateTransferIdentifyCard) {
        this.privateTransferIdentifyCard = privateTransferIdentifyCard;
    }

    /**
     * 获取 对私账户-打款人银行卡号
     */
    public String getPrivateTransferBankCard() {
        return this.privateTransferBankCard;
    }

    /**
     * 设置 对私账户-打款人银行卡号
     */
    public void setPrivateTransferBankCard(String privateTransferBankCard) {
        this.privateTransferBankCard = privateTransferBankCard;
    }

    /**
     * 获取 对私账户-打款人银行卡开户行
     */
    public String getPrivateTransferBankAccount() {
        return this.privateTransferBankAccount;
    }

    /**
     * 设置 对私账户-打款人银行卡开户行
     */
    public void setPrivateTransferBankAccount(String privateTransferBankAccount) {
        this.privateTransferBankAccount = privateTransferBankAccount;
    }

    public SettlementInfoReq() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.auditId);
        dest.writeString(this.marketerId);
        dest.writeValue(this.operation);
        dest.writeSerializable(this.settlementRate);
        dest.writeValue(this.settlementCycle);
        dest.writeString(this.publicOpenVoucherId);
        dest.writeString(this.publicOpenVoucherUrl);
        dest.writeString(this.publicOpenVoucherCode);
        dest.writeString(this.publicTransferName);
        dest.writeString(this.publicTransferIdentifyCard);
        dest.writeString(this.publicTransferBankCard);
        dest.writeString(this.publicTransferBankAccount);
        dest.writeString(this.privateTransferName);
        dest.writeString(this.privateTransferIdentifyCard);
        dest.writeString(this.privateTransferBankCard);
        dest.writeString(this.privateTransferBankAccount);
        dest.writeString(this.privateTransferBankCardPhoto);
        dest.writeString(this.privateTransferIdCardBack);
        dest.writeString(this.privateTransferIdCardFront);
        dest.writeString(this.publicTransferBankCardPhoto);
        dest.writeString(this.publicTransferIdCardBack);
        dest.writeString(this.publicTransferIdCardFront);
    }

    protected SettlementInfoReq(Parcel in) {
        this.auditId = in.readString();
        this.marketerId = in.readString();
        this.operation = (Short) in.readValue(Short.class.getClassLoader());
        this.settlementRate = (BigDecimal) in.readSerializable();
        this.settlementCycle = (Short) in.readValue(Short.class.getClassLoader());
        this.publicOpenVoucherId = in.readString();
        this.publicOpenVoucherUrl = in.readString();
        this.publicOpenVoucherCode = in.readString();
        this.publicTransferName = in.readString();
        this.publicTransferIdentifyCard = in.readString();
        this.publicTransferBankCard = in.readString();
        this.publicTransferBankAccount = in.readString();
        this.privateTransferName = in.readString();
        this.privateTransferIdentifyCard = in.readString();
        this.privateTransferBankCard = in.readString();
        this.privateTransferBankAccount = in.readString();
        this.privateTransferBankCardPhoto = in.readString();
        this.privateTransferIdCardBack = in.readString();
        this.privateTransferIdCardFront = in.readString();
        this.publicTransferBankCardPhoto = in.readString();
        this.publicTransferIdCardBack = in.readString();
        this.publicTransferIdCardFront = in.readString();
    }

    public static final Creator<SettlementInfoReq> CREATOR = new Creator<SettlementInfoReq>() {
        @Override
        public SettlementInfoReq createFromParcel(Parcel source) {
            return new SettlementInfoReq(source);
        }

        @Override
        public SettlementInfoReq[] newArray(int size) {
            return new SettlementInfoReq[size];
        }
    };
}