package com.nihome.entershopinfo.entity.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.nihome.entershopinfo.entity.request.BasicInfoReq;
import com.nihome.entershopinfo.entity.request.QualificationInfoReq;
import com.nihome.entershopinfo.entity.request.SendInfoReq;
import com.nihome.entershopinfo.entity.request.SettlementInfoReq;
import com.nihome.entershopinfo.entity.request.ShopInfoReq;
import com.nihome.entershopinfo.entity.request.XcxRelevantReq;

/**
 * Created by Carson on 2017/7/24.
 */
public class ExpandsLogRes implements Parcelable {
    private OtherInfoRes otherInfoResponseDTO;
    private BasicInfoReq basicInfoResponseDTO;
    private ShopInfoReq storeInfoResponseDTO;
    private SendInfoReq takeOutInfoResponseDTO;
    private SettlementInfoReq settlementInfoResponseDTO;
    private XcxRelevantReq programsInfoResponseDTO;
    private QualificationInfoReq  qualificationInfoResponseDTO;

    public OtherInfoRes getOtherInfoResponseDTO() {
        return otherInfoResponseDTO;
    }

    public void setOtherInfoResponseDTO(OtherInfoRes otherInfoResponseDTO) {
        this.otherInfoResponseDTO = otherInfoResponseDTO;
    }

    public BasicInfoReq getBasicInfoResponseDTO() {
        return basicInfoResponseDTO;
    }

    public void setBasicInfoResponseDTO(BasicInfoReq basicInfoResponseDTO) {
        this.basicInfoResponseDTO = basicInfoResponseDTO;
    }

    public ShopInfoReq getStoreInfoResponseDTO() {
        return storeInfoResponseDTO;
    }

    public void setStoreInfoResponseDTO(ShopInfoReq storeInfoResponseDTO) {
        this.storeInfoResponseDTO = storeInfoResponseDTO;
    }

    public SendInfoReq getTakeOutInfoResponseDTO() {
        return takeOutInfoResponseDTO;
    }

    public void setTakeOutInfoResponseDTO(SendInfoReq takeOutInfoResponseDTO) {
        this.takeOutInfoResponseDTO = takeOutInfoResponseDTO;
    }

    public SettlementInfoReq getSettlementInfoResponseDTO() {
        return settlementInfoResponseDTO;
    }

    public void setSettlementInfoResponseDTO(SettlementInfoReq settlementInfoResponseDTO) {
        this.settlementInfoResponseDTO = settlementInfoResponseDTO;
    }

    public XcxRelevantReq getProgramsInfoResponseDTO() {
        return programsInfoResponseDTO;
    }

    public void setProgramsInfoResponseDTO(XcxRelevantReq programsInfoResponseDTO) {
        this.programsInfoResponseDTO = programsInfoResponseDTO;
    }

    public QualificationInfoReq getQualificationInfoResponseDTO() {
        return qualificationInfoResponseDTO;
    }

    public void setQualificationInfoResponseDTO(QualificationInfoReq qualificationInfoResponseDTO) {
        this.qualificationInfoResponseDTO = qualificationInfoResponseDTO;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.otherInfoResponseDTO, flags);
        dest.writeParcelable(this.basicInfoResponseDTO, flags);
        dest.writeParcelable(this.storeInfoResponseDTO, flags);
        dest.writeParcelable(this.takeOutInfoResponseDTO, flags);
        dest.writeParcelable(this.settlementInfoResponseDTO, flags);
        dest.writeParcelable(this.programsInfoResponseDTO, flags);
        dest.writeParcelable(this.qualificationInfoResponseDTO, flags);
    }

    public ExpandsLogRes() {
    }

    protected ExpandsLogRes(Parcel in) {
        this.otherInfoResponseDTO = in.readParcelable(OtherInfoRes.class.getClassLoader());
        this.basicInfoResponseDTO = in.readParcelable(BasicInfoReq.class.getClassLoader());
        this.storeInfoResponseDTO = in.readParcelable(ShopInfoReq.class.getClassLoader());
        this.takeOutInfoResponseDTO = in.readParcelable(SendInfoReq.class.getClassLoader());
        this.settlementInfoResponseDTO = in.readParcelable(SettlementInfoReq.class.getClassLoader());
        this.programsInfoResponseDTO = in.readParcelable(XcxRelevantReq.class.getClassLoader());
        this.qualificationInfoResponseDTO = in.readParcelable(QualificationInfoReq.class.getClassLoader());
    }

    public static final Parcelable.Creator<ExpandsLogRes> CREATOR = new Parcelable.Creator<ExpandsLogRes>() {
        @Override
        public ExpandsLogRes createFromParcel(Parcel source) {
            return new ExpandsLogRes(source);
        }

        @Override
        public ExpandsLogRes[] newArray(int size) {
            return new ExpandsLogRes[size];
        }
    };
}
