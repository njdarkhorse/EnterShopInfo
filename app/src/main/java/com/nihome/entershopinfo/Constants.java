package com.nihome.entershopinfo;

import android.os.Environment;

/**
 * Created by Carson on 2017/7/19.
 */
public class Constants {
    /** 指定编码格式 */
    public static final String ENCODING_RULE                = "UTF-8";
    public static final int REQ_SUCCESS                     = 0000;
    public static final String GD_KEY                       = "28894e7fe9d05cb7938d2ff8b5c283a8";
    public static final String SELECT_TYPE                  = "selectType";
    public static final String DATA                         = "data";

    public static final int XCX_TYPE                        = 1000;
    public static final int H5_TYPE                         = 1001;
    public static final int CHAIN_TYPE                      = 1002;
    public static final int CHAIN_CHILD_TYPE                = 1003;


    public static final short NEW                           = 21;
    public static final short UPDATE                        = 22;


    /** SD卡目录 */
    public static final String SD_CARDFILE                  = Environment.getExternalStorageDirectory().toString();
    //public static final String CACHE_DIR                    = SD_CARDFILE + "/data/com.nihome.entershopinfo/cache/";
    public static final String CACHE_DIR                    = "/data/data/com.nihome.entershopinfo/cache/";
    /** 用户资料缓存文件名 */
    public static final String USER_CACHE_FILE_NAME         = "user.txt";
}
