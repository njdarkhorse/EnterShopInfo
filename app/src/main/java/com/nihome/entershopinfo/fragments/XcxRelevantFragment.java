package com.nihome.entershopinfo.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nihome.entershopinfo.Constants;
import com.nihome.entershopinfo.R;
import com.nihome.entershopinfo.application.SysApplication;
import com.nihome.entershopinfo.dialog.ImagePickDialog;
import com.nihome.entershopinfo.dialog.LoadingDialog;
import com.nihome.entershopinfo.entity.User;
import com.nihome.entershopinfo.entity.request.UploadImgReq;
import com.nihome.entershopinfo.entity.request.XcxRelevantReq;
import com.nihome.entershopinfo.entity.response.ExpandsLogRes;
import com.nihome.entershopinfo.entity.response.ResponseBase;
import com.nihome.entershopinfo.entity.response.UploadImgRes;
import com.nihome.entershopinfo.ui.HomePageActivity;
import com.nihome.entershopinfo.ui.PermissionsActivity;
import com.nihome.entershopinfo.utils.Base64Util;
import com.nihome.entershopinfo.utils.BitmapUtils;
import com.nihome.entershopinfo.utils.ImagePicker;
import com.nihome.entershopinfo.utils.Log;
import com.nihome.entershopinfo.utils.NetUtil;
import com.nihome.entershopinfo.utils.PermissionsChecker;
import com.nihome.entershopinfo.utils.RequestAPI;
import com.nihome.entershopinfo.utils.StringUtils;
import com.squareup.picasso.Picasso;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.Response;

/**
 * Created by Carson on 2017/7/20.
 */
public class XcxRelevantFragment extends Fragment implements View.OnClickListener, ImagePickDialog.DialogSelectedListener{
    private static final String TAG = "XcxRelevantFragment";
    private PermissionsChecker mPermissionsChecker; // 权限检测器
    static final String[] PERMISSIONS = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};
    private static final int REQUEST_PERMISSION = 100;  //权限请求
    private ImagePickDialog imagePickDialog;
    private ImagePicker mImagePicker;
    private Uri cameraUri;
    private Uri cutImgUri;

    private Context mContext;
    private NetUtil mNetUtil;
    private Gson mGson;
    private User mUser;
    private LoadingDialog dialog;
    private int selectType;
    private ExpandsLogRes expandsLog;
    private XcxRelevantReq relevantReq;
    private String auditId;
    private short operation;

    private LinearLayout xcxLayout;
    private EditText xcxName_1;
    private EditText xcxName_2;
    private EditText xcxName_3;
    private EditText xcxIntroduce;
    private EditText xcxLabel_1;
    private EditText xcxLabel_2;
    private EditText xcxLabel_3;
    private EditText xcxLabel_4;
    private EditText xcxLabel_5;
    private EditText shopIdea;
    private EditText sendExplain;
    private ImageView shopLogoImg;
    private TextView backBtn;
    private TextView goNextBtn;

    private StringBuffer xcxNameBuffer;
    private StringBuffer xcxLabelBuffer;
    private String shopLogoId;
    private String shopLogoUrl;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mGson = new Gson();
        mNetUtil = new NetUtil(mContext);
        mUser = SysApplication.getInstance().getmUser();
        mPermissionsChecker = new PermissionsChecker(mContext);
        dialog = new LoadingDialog(mContext);
        selectType = getArguments().getInt(Constants.SELECT_TYPE);
        expandsLog = getArguments().getParcelable(Constants.DATA);
        auditId = SysApplication.getInstance().getAuditId();
        if (expandsLog.getProgramsInfoResponseDTO() == null) operation = Constants.NEW;
        else operation = Constants.UPDATE;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_xcx_relevant, container, false);
        xcxLayout = (LinearLayout) view.findViewById(R.id.xcx_layout);
        xcxName_1 = (EditText) view.findViewById(R.id.xcx_name_1);
        xcxName_2 = (EditText) view.findViewById(R.id.xcx_name_2);
        xcxName_3 = (EditText) view.findViewById(R.id.xcx_name_3);
        xcxIntroduce = (EditText) view.findViewById(R.id.xcx_introduce);
        xcxLabel_1 = (EditText) view.findViewById(R.id.xcx_label_1);
        xcxLabel_2 = (EditText) view.findViewById(R.id.xcx_label_2);
        xcxLabel_3 = (EditText) view.findViewById(R.id.xcx_label_3);
        xcxLabel_4 = (EditText) view.findViewById(R.id.xcx_label_4);
        xcxLabel_5 = (EditText) view.findViewById(R.id.xcx_label_5);
        shopIdea = (EditText) view.findViewById(R.id.shop_idea);
        sendExplain = (EditText) view.findViewById(R.id.send_explain);
        shopLogoImg = (ImageView) view.findViewById(R.id.shop_logo);
        goNextBtn = (TextView) view.findViewById(R.id.relevant_go_next);
        backBtn = (TextView) view.findViewById(R.id.relevant_back);

        shopLogoImg.setOnClickListener(this);
        backBtn.setOnClickListener(this);
        goNextBtn.setOnClickListener(this);
        if (selectType == Constants.XCX_TYPE || selectType == Constants.CHAIN_TYPE ) {
            xcxLayout.setVisibility(View.VISIBLE);
        } else {
            xcxLayout.setVisibility(View.GONE);
        }
        if (operation == Constants.UPDATE) initData();
        return view;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            selectType = getArguments().getInt(Constants.SELECT_TYPE);
            expandsLog = getArguments().getParcelable(Constants.DATA);
            auditId = SysApplication.getInstance().getAuditId();
            if (expandsLog.getProgramsInfoResponseDTO() == null) operation = Constants.NEW;
            else operation = Constants.UPDATE;
        }
        if (selectType == Constants.XCX_TYPE || selectType == Constants.CHAIN_TYPE ) {
            xcxLayout.setVisibility(View.VISIBLE);
        } else {
            xcxLayout.setVisibility(View.GONE);
        }
        if (operation == Constants.NEW) viewInit();
        if (operation == Constants.UPDATE) initData();
    }


    void initData(){
        String[] xcxNames = expandsLog.getProgramsInfoResponseDTO().getMiniProgramsName().split(",");
        for (int index = 0; index < xcxNames.length; index ++) {
            if (index == 0) xcxName_1.setText(xcxNames[index]);
            if (index == 1) xcxName_2.setText(xcxNames[index]);
            if (index == 2) xcxName_3.setText(xcxNames[index]);
        }
        xcxIntroduce.setText(expandsLog.getProgramsInfoResponseDTO().getMiniProgramsIntroduce());
        String[] xcxLabels = expandsLog.getProgramsInfoResponseDTO().getMiniProgramsLabel().split(",");
        for (int index = 0; index < xcxLabels.length; index ++) {
            if (index == 0) xcxLabel_1.setText(xcxLabels[index]);
            if (index == 1) xcxLabel_2.setText(xcxLabels[index]);
            if (index == 2) xcxLabel_3.setText(xcxLabels[index]);
            if (index == 3) xcxLabel_4.setText(xcxLabels[index]);
            if (index == 4) xcxLabel_5.setText(xcxLabels[index]);
        }
        shopIdea.setText(expandsLog.getProgramsInfoResponseDTO().getStoreIdea());
        sendExplain.setText(expandsLog.getProgramsInfoResponseDTO().getTakeOutMemo());
        shopLogoId = expandsLog.getProgramsInfoResponseDTO().getStoreLogoId();
        shopLogoUrl = expandsLog.getProgramsInfoResponseDTO().getStoreLogo();
        Picasso.with(mContext).load(expandsLog.getProgramsInfoResponseDTO().getStoreLogo()).
                placeholder(R.drawable.loading).resize(200, 200).into(shopLogoImg);
    }

    void viewInit(){
        xcxName_1.setText("");
        xcxName_2.setText("");
        xcxName_3.setText("");
        xcxIntroduce.setText("");
        xcxLabel_1.setText("");
        xcxLabel_2.setText("");
        xcxLabel_3.setText("");
        xcxLabel_4.setText("");
        xcxLabel_5.setText("");
        shopIdea.setText("");
        sendExplain.setText("");
        shopLogoId = "";
        shopLogoUrl = "";
        shopLogoImg.setImageResource(R.drawable.upload_img);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.shop_logo:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (mPermissionsChecker.lacksPermissions(PERMISSIONS)) {
                        startPermissionsActivity();
                    } else {
                        showDialog();
                    }
                } else {
                    showDialog();
                }
                break;
            case R.id.relevant_back:
                ((HomePageActivity)getActivity()).backClick();
                break;
            case R.id.relevant_go_next:
                if (!mNetUtil.isNetConnected()) {
                    Toast.makeText(mContext, getString(R.string.cannot_connect_network), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (check()) subXcxRelevant();
                else Toast.makeText(mContext, getString(R.string.submit_prompt), Toast.LENGTH_SHORT).show();
                //((HomePageActivity)getActivity()).goNextOfRelevant();
                break;
        }
    }

    private Boolean check(){
        if (selectType == Constants.XCX_TYPE || selectType == Constants.CHAIN_TYPE) {
            if (StringUtils.isEmpty(xcxName_1.getText().toString()) && StringUtils.isEmpty(xcxName_2.getText().toString()) &&
                    StringUtils.isEmpty(xcxName_3.getText().toString())) {
                Toast.makeText(mContext, "xcxName_1, xcxName_2, xcxName_3为空，请截图提示信息提交到禅道", Toast.LENGTH_LONG).show();
                return false;
            }
            if (!StringUtils.isEmpty(xcxName_1.getText().toString()) && StringUtils.isEmpty(xcxName_2.getText().toString()) &&
                    StringUtils.isEmpty(xcxName_3.getText().toString())) {
                Toast.makeText(mContext, "xcxName_2, xcxName_3为空，请截图提示信息提交到禅道", Toast.LENGTH_LONG).show();
                return false;
            }
            if (StringUtils.isEmpty(xcxName_1.getText().toString()) && !StringUtils.isEmpty(xcxName_2.getText().toString()) &&
                    StringUtils.isEmpty(xcxName_3.getText().toString())) {
                Toast.makeText(mContext, "xcxName_1, xcxName_3为空，请截图提示信息提交到禅道", Toast.LENGTH_LONG).show();
                return false;
            }
            if (StringUtils.isEmpty(xcxName_1.getText().toString()) && StringUtils.isEmpty(xcxName_2.getText().toString()) &&
                    !StringUtils.isEmpty(xcxName_3.getText().toString())) {
                Toast.makeText(mContext, "xcxName_1, xcxName_2为空，请截图提示信息提交到禅道", Toast.LENGTH_LONG).show();
                return false;
            }
        }
        if (StringUtils.isEmpty(shopIdea.getText().toString())) {
            Toast.makeText(mContext, "shopIdea为空，请截图提示信息提交到禅道", Toast.LENGTH_LONG).show();
            return false;
        }
        if (StringUtils.isEmpty(sendExplain.getText().toString())) {
            Toast.makeText(mContext, "shopIdea为空，请截图提示信息提交到禅道", Toast.LENGTH_LONG).show();
            return false;
        }
        if (StringUtils.isEmpty(shopLogoId)) {
            Toast.makeText(mContext, "shopIdea为空，请截图提示信息提交到禅道", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    private void subXcxRelevant(){
        dialog.show(getString(R.string.load_text));
        xcxNameBuffer = new StringBuffer();
        xcxLabelBuffer = new StringBuffer();
        if (!StringUtils.isEmpty(xcxName_1.getText().toString())) xcxNameBuffer.append(xcxName_1.getText().toString()).append(",");
        if (!StringUtils.isEmpty(xcxName_2.getText().toString())) xcxNameBuffer.append(xcxName_2.getText().toString()).append(",");
        if (!StringUtils.isEmpty(xcxName_3.getText().toString())) xcxNameBuffer.append(xcxName_3.getText().toString());
        if (!StringUtils.isEmpty(xcxLabel_1.getText().toString())) xcxLabelBuffer.append(xcxLabel_1.getText().toString()).append(",");
        if (!StringUtils.isEmpty(xcxLabel_2.getText().toString())) xcxLabelBuffer.append(xcxLabel_2.getText().toString()).append(",");
        if (!StringUtils.isEmpty(xcxLabel_3.getText().toString())) xcxLabelBuffer.append(xcxLabel_3.getText().toString()).append(",");
        if (!StringUtils.isEmpty(xcxLabel_4.getText().toString())) xcxLabelBuffer.append(xcxLabel_4.getText().toString()).append(",");
        if (!StringUtils.isEmpty(xcxLabel_5.getText().toString())) xcxLabelBuffer.append(xcxLabel_5.getText().toString());

        relevantReq = new XcxRelevantReq();
        relevantReq.setAuditId(auditId);
        relevantReq.setMarketerId(mUser.getUserId());
        relevantReq.setOperation(operation);
        relevantReq.setMiniProgramsName(xcxNameBuffer.toString());
        if (!StringUtils.isEmpty(xcxIntroduce.getText().toString())) relevantReq.setMiniProgramsIntroduce(xcxIntroduce.getText().toString());
        relevantReq.setMiniProgramsLabel(xcxLabelBuffer.toString());
        relevantReq.setStoreIdea(shopIdea.getText().toString());
        relevantReq.setTakeOutMemo(sendExplain.getText().toString());
        relevantReq.setStoreLogoId(shopLogoId);
        relevantReq.setStoreLogo(shopLogoUrl);
        Log.e(TAG, "subXcxRelevant json = " + mGson.toJson(relevantReq));
        OkHttpUtils.postString().url(RequestAPI.XCX_RELEVANT_URL)
                .content(new Gson().toJson(relevantReq))
                .mediaType(MediaType.parse("application/json; charset=utf-8"))
                .build().execute(new XcxRelevantCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.e(TAG, e + "");
                dialog.dismiss();
                if (e instanceof UnknownHostException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (e instanceof SocketException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (e instanceof SocketTimeoutException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (e instanceof IOException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }

                Toast.makeText(mContext, getString(R.string.service_error), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(ResponseBase<String> response, int id) {
                dialog.dismiss();
                switch (Integer.valueOf(response.getResultCode())) {
                    case Constants.REQ_SUCCESS:
                        expandsLog.setProgramsInfoResponseDTO(relevantReq);
                        ((HomePageActivity)getActivity()).goNextOfRelevant();
                        break;
                    default:
                        Toast.makeText(mContext, response.getMsg(), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

    }

    abstract class XcxRelevantCallback extends Callback<ResponseBase<String>> {
        @Override
        public ResponseBase<String> parseNetworkResponse(Response response, int id) throws Exception {
            String string = response.body().string();
            Log.e(TAG, string);
            Type jsonType = new TypeToken<ResponseBase<String>>(){}.getType();
            ResponseBase<String> res = mGson.fromJson(string, jsonType);
            return res;
        }
    }


    /** 上传图片 */
    private void uploadImgService(){
        dialog.show(getString(R.string.load_text));
        Log.e(TAG, "image path = " + cutImgUri.getPath());
        String img = "data:image/png;base64,";
        img +=  Base64Util.encode(BitmapUtils.FileTobyte(new File(cutImgUri.getPath())));
        //String img =  Base64Util.encode(BitmapUtils.FileTobyte(new File(cutImgUri.getPath())));
        UploadImgReq uploadImgReq = new UploadImgReq();
        uploadImgReq.setFileBase64Data(img);
        uploadImgReq.setUserId(mUser.getUserId());
        OkHttpUtils.postString().url(RequestAPI.UPLOAD_IMG_URL)
                .content(new Gson().toJson(uploadImgReq))
                .mediaType(MediaType.parse("application/json; charset=utf-8"))
                .build().execute(new UploadImgCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.e(TAG, e + "");
                dialog.dismiss();
                if (e instanceof UnknownHostException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (e instanceof SocketException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (e instanceof SocketTimeoutException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (e instanceof IOException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }

                Toast.makeText(mContext, getString(R.string.service_error), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(ResponseBase<UploadImgRes> response, int id) {
                dialog.dismiss();
                switch (Integer.valueOf(response.getResultCode())) {
                    case Constants.REQ_SUCCESS:
                        try {
                            Toast.makeText(mContext, getString(R.string.upload_success), Toast.LENGTH_SHORT).show();
                            shopLogoId = response.getResult().getFileId();
                            shopLogoUrl = response.getResult().getFileUrl();
                            Picasso.with(mContext).load(response.getResult().getFileUrl()).
                                    placeholder(R.drawable.loading).resize(200, 200).into(shopLogoImg);
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                        break;
                    default:
                        Toast.makeText(mContext, response.getMsg(), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }

    abstract class UploadImgCallback extends Callback<ResponseBase<UploadImgRes>> {
        @Override
        public ResponseBase<UploadImgRes> parseNetworkResponse(Response response, int id) throws Exception {
            String string = response.body().string();
            Log.e(TAG, string);
            Type jsonType = new TypeToken<ResponseBase<UploadImgRes>>(){}.getType();
            ResponseBase<UploadImgRes> res = mGson.fromJson(string, jsonType);
            return res;
        }
    }

    @Override
    public void onCapture() {
        Log.e(TAG, "拍照........");
        if (mImagePicker == null) mImagePicker = new ImagePicker(getActivity(), this);
        cameraUri = mImagePicker.openCamera();
    }

    @Override
    public void onPhone() {
        Log.e(TAG, "相册........");
        if (mImagePicker == null) mImagePicker = new ImagePicker(getActivity(), this);
        mImagePicker.pickOnphone();
    }

    @Override
    public void onCancel() {

    }

    private void startPermissionsActivity() {
        PermissionsActivity.startActivityForResult(getActivity(), REQUEST_PERMISSION,
                PERMISSIONS);
    }

    private void showDialog(){
        if (imagePickDialog == null) imagePickDialog = new ImagePickDialog(mContext);
        imagePickDialog.setDialogSelectedListener(this);
        imagePickDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case ImagePicker.REQUEST_CAPTURE:
                    cutImgUri = mImagePicker.cropPhoto(cameraUri);
                    break;
                case ImagePicker.REQUEST_PICK_IMAGE:
                    cameraUri = data.getData(); //获取系统返回的照片的Uri
                    cutImgUri = mImagePicker.cropPhoto(cameraUri);
                    break;
                case ImagePicker.REQUEST_PICTURE_CUT:
                    if (!mNetUtil.isNetConnected()) {
                        Toast.makeText(mContext, getString(R.string.cannot_connect_network), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    uploadImgService();
                    break;
            }
        }
    }
}
