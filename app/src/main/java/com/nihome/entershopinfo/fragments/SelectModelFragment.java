package com.nihome.entershopinfo.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nihome.entershopinfo.Constants;
import com.nihome.entershopinfo.R;
import com.nihome.entershopinfo.application.SysApplication;
import com.nihome.entershopinfo.dialog.LoadingDialog;
import com.nihome.entershopinfo.dialog.ShopTypeDialog;
import com.nihome.entershopinfo.entity.User;
import com.nihome.entershopinfo.entity.response.ExpandsLogRes;
import com.nihome.entershopinfo.entity.response.ResponseBase;
import com.nihome.entershopinfo.ui.HomePageActivity;
import com.nihome.entershopinfo.utils.Log;
import com.nihome.entershopinfo.utils.NetUtil;
import com.nihome.entershopinfo.utils.RequestAPI;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Carson on 2017/7/18.
 */
public class SelectModelFragment extends Fragment implements View.OnClickListener, ShopTypeDialog.ShopTypeListener{
    private static final String TAG = "SelectModelFragment";
    private Context mContext;
    private User mUser;
    private Gson mGson;
    private NetUtil mNetUtil;
    private LoadingDialog dialog;
    private HomePageActivity activity;
    private LinearLayout xcxLayout;
    private LinearLayout h5Layout;

    private ShopTypeDialog shopTypeDialog;
    private int shopType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (HomePageActivity) getActivity();
        mUser = SysApplication.getInstance().getmUser();
        mContext = getActivity();
        mGson = new Gson();
        mNetUtil = new NetUtil(mContext);
        dialog = new LoadingDialog(mContext);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_select_mode, container, false);
        xcxLayout = (LinearLayout) view.findViewById(R.id.xcx_layout);
        h5Layout = (LinearLayout) view.findViewById(R.id.h5_layout);

        xcxLayout.setOnClickListener(this);
        h5Layout.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.xcx_layout:
                if (shopTypeDialog == null) {
                    shopTypeDialog = new ShopTypeDialog(getActivity());
                    shopTypeDialog.setListener(this);
                }
                shopTypeDialog.show();
                break;
            case R.id.h5_layout:
                shopType = Constants.H5_TYPE;
                if (!mNetUtil.isNetConnected()) {
                    Toast.makeText(mContext, getString(R.string.cannot_connect_network), Toast.LENGTH_SHORT).show();
                    return;
                }
                initShopData(2);
                break;
        }
    }

    @Override
    public void onSingleShop() {
        shopType = Constants.XCX_TYPE;
        if (!mNetUtil.isNetConnected()) {
            Toast.makeText(mContext, getString(R.string.cannot_connect_network), Toast.LENGTH_SHORT).show();
            return;
        }
        initShopData(1);
        shopTypeDialog.dismiss();
        //activity.xcxClick();
    }

    @Override
    public void onChainShop() {
        shopType = Constants.CHAIN_TYPE;
        if (!mNetUtil.isNetConnected()) {
            Toast.makeText(mContext, getString(R.string.cannot_connect_network), Toast.LENGTH_SHORT).show();
            return;
        }
        initShopData(1);
        shopTypeDialog.dismiss();
    }

    private void initShopData(int type){
        dialog.show(getString(R.string.load_text));
        String url = RequestAPI.INIT_SHOP_DATA_URL + "?source="+ type +"&marketerId="+mUser.getUserId();
        OkHttpUtils.get().url(url).build().execute(new InitShopDataCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.e(TAG, e + "");
                dialog.dismiss();
                if (e instanceof UnknownHostException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (e instanceof SocketException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (e instanceof SocketTimeoutException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (e instanceof IOException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }

                Toast.makeText(mContext, getString(R.string.service_error), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(ResponseBase<String> response, int id) {
                dialog.dismiss();
                switch (Integer.valueOf(response.getResultCode())) {
                    case Constants.REQ_SUCCESS:
                        SysApplication.getInstance().setAuditId(response.getResult());
                        if (shopType ==Constants.XCX_TYPE) activity.xcxClick(SelectModelFragment.this);
                        if (shopType ==Constants.H5_TYPE) activity.h5Click();
                        if (shopType ==Constants.CHAIN_TYPE) activity.xcxClick_chain(new ExpandsLogRes(), SelectModelFragment.this);
                        break;
                    default:
                        Toast.makeText(mContext, response.getMsg(), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }

    abstract class InitShopDataCallback extends Callback<ResponseBase<String>>{
        @Override
        public ResponseBase<String> parseNetworkResponse(Response response, int id) throws Exception {
            String string = response.body().string();
            Log.e(TAG, string);
            Type jsonType = new TypeToken<ResponseBase<String>>(){}.getType();
            ResponseBase<String> res = mGson.fromJson(string, jsonType);
            return res;
        }
    }
}
