package com.nihome.entershopinfo.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nihome.entershopinfo.Constants;
import com.nihome.entershopinfo.R;
import com.nihome.entershopinfo.adapter.LogOpenTimeAdapter;
import com.nihome.entershopinfo.entity.response.ExpandsLogRes;
import com.nihome.entershopinfo.ui.HomePageActivity;
import com.nihome.entershopinfo.utils.StringUtils;
import com.squareup.picasso.Picasso;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Carson on 2017/7/24.
 */
public class LogDetailFragment extends Fragment {
    private static final String TAG = "LogDetailFragment";
    private Context mContext;

    private TextView applyTypeView;
    private TextView shopNameView;
    private TextView contactsNameView;
    private TextView contactsPhoneView;
    private TextView joinTypeView;
    private ImageView radioTrueImg;
    private ImageView radioFalseImg;
    private ImageView businessPersonalImg;
    private ImageView businessCompanyImg;
    private LinearLayout businessLicenseLayout;
    private ImageView businessLicenseImg;

    private LinearLayout cashLayout;
    private ImageView yesCashImg;
    private ImageView noCashImg;

    private LinearLayout detailQualificationsLayout;
    private ImageView imgView1;
    private ImageView imgView2;
    private ImageView imgView3;
    private ImageView imgView4;

    private LinearLayout shopInfoLayout;
    private TextView provinceView;
    private TextView cityView;
    private TextView areaView;
    private TextView detailAddressView;
    private TextView latitudeLongitudeView;
    private LinearLayout openTimeLayoutView;
    private TextView posNumberView;
    private TextView posPrintView;
    private LinearLayout eatLayoutView;
    private TextView teaFeeView;
    private TextView teaFeeActivity;
    private TextView tableNumberView;
    private TextView memoView;
    private TextView getFoodNotifyView;
    private TextView phoneTypeView;
    private TextView businessWayView;
    private LinearLayout menuMemoLayout;
    private LinearLayout shopMenuImgLayout;
    private ImageView shopMenu_1;
    private ImageView shopMenu_2;
    private ImageView shopMenu_3;
    private ImageView shopMenu_4;
    private TextView menuMemoView;

    private LinearLayout sendInfoLayoutView;
    private TextView sendWayView;
    private TextView whetherSendTypeView;
    private LinearLayout chargingTypeLayout;
    private TextView chargingTypeView;
    private LinearLayout chargingFreeLayout;
    private TextView chargingFreeView;
    private TextView takeOutDeliverFeeView;
    private LinearLayout daDaInfoLayoutView;
    private TextView sendPhoneView;
    private TextView sendContactsView;
    private TextView sendEmailView;
    private TextView sendCardNoView;
    private LinearLayout sendInfoLayoutSelf;
    private TextView sendFreeEditView;
    private TextView sendMaxDistanceView;
    private TextView sendFirstTimeView;

    private LinearLayout payMoneyInfoLayout;
    private TextView hundredFlagView;
    private TextView serviceChargeView;
    private TextView settlementCycleView;
    private LinearLayout serviceChargeLayout;
    private LinearLayout settlementCycleLayout;

    private TextView privateUserNameEdit;
    private TextView privateIdNumberEdit;
    private TextView privateCardNumberEdit;
    private TextView privateBankNameEdit;
    private ImageView privateImg_1;
    private ImageView privateImg_2;
    private ImageView privateImg_3;


    private LinearLayout openingPermitLayout;
    private ImageView openingPermitImg;
    private TextView openingPermitEdit;
    private TextView publicUserNameEdit;
    private TextView publicIdNumberEdit;
    private TextView publicCardNumberEdit;
    private TextView publicBankNameEdit;
    private ImageView publicImg_1;
    private ImageView publicImg_2;
    private ImageView publicImg_3;

    private TextView title6View;
    private LinearLayout xcxInfoLayoutView;
    private TextView xcxName1View;
    private TextView xcxName2View;
    private TextView xcxName3View;
    private TextView xcxIntroduceView;
    private TextView xcxLabel1View;
    private TextView xcxLabel2View;
    private TextView xcxLabel3View;
    private TextView xcxLabel4View;
    private TextView xcxLabel5View;
    private TextView shopIdeaView;
    private TextView sendExplainView;


    private ImageView shopLogoView;
    private ImageView groupPhotoView;

    private ExpandsLogRes expandsLog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        expandsLog = getArguments().getParcelable(Constants.DATA);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_log_detail, container, false);
        applyTypeView = (TextView) view.findViewById(R.id.apply_type_view);
        shopNameView = (TextView) view.findViewById(R.id.shop_name_view);
        contactsNameView = (TextView) view.findViewById(R.id.contacts_name_view);
        contactsPhoneView = (TextView) view.findViewById(R.id.contacts_phone_view);
        joinTypeView = (TextView) view.findViewById(R.id.join_type_view);
        radioTrueImg = (ImageView) view.findViewById(R.id.radio_true_img);
        radioFalseImg = (ImageView) view.findViewById(R.id.radio_false_img);
        businessPersonalImg = (ImageView) view.findViewById(R.id.business_personal_img);
        businessCompanyImg = (ImageView) view.findViewById(R.id.business_company_img);
        businessLicenseLayout = (LinearLayout) view.findViewById(R.id.business_license_layout);
        businessLicenseImg = (ImageView) view.findViewById(R.id.business_license_img);
        cashLayout = (LinearLayout) view.findViewById(R.id.cash_layout);
        yesCashImg = (ImageView) view.findViewById(R.id.yes_cash_img);
        noCashImg = (ImageView) view.findViewById(R.id.no_cash_img);



        detailQualificationsLayout = (LinearLayout) view.findViewById(R.id.detail_qualifications_layout);
        imgView1 = (ImageView) view.findViewById(R.id.img_1);
        imgView2 = (ImageView) view.findViewById(R.id.img_2);
        imgView3 = (ImageView) view.findViewById(R.id.img_3);
        imgView4 = (ImageView) view.findViewById(R.id.img_4);

        shopInfoLayout = (LinearLayout) view.findViewById(R.id.shop_info_layout);
        provinceView = (TextView) view.findViewById(R.id.province_view);
        cityView = (TextView) view.findViewById(R.id.city_view);
        areaView = (TextView) view.findViewById(R.id.area_view);
        detailAddressView = (TextView) view.findViewById(R.id.detail_address_view);
        latitudeLongitudeView = (TextView) view.findViewById(R.id.latitude_longitude_view);
        openTimeLayoutView = (LinearLayout) view.findViewById(R.id.open_time_layout_view);
        posNumberView = (TextView) view.findViewById(R.id.pos_number_view);
        posPrintView = (TextView) view.findViewById(R.id.pos_print_view);
        eatLayoutView = (LinearLayout) view.findViewById(R.id.eat_layout_view);
        teaFeeView = (TextView) view.findViewById(R.id.tea_fee_view);
        teaFeeActivity = (TextView) view.findViewById(R.id.tea_fee_activity);
        tableNumberView = (TextView) view.findViewById(R.id.table_number_view);
        memoView = (TextView) view.findViewById(R.id.memo_view);
        getFoodNotifyView = (TextView) view.findViewById(R.id.get_food_notify_view);
        phoneTypeView = (TextView) view.findViewById(R.id.phone_type_view);
        businessWayView = (TextView) view.findViewById(R.id.business_way_view);
        shopMenu_1 = (ImageView) view.findViewById(R.id.shop_menu_1);
        shopMenu_2 = (ImageView) view.findViewById(R.id.shop_menu_2);
        shopMenu_3 = (ImageView) view.findViewById(R.id.shop_menu_3);
        shopMenu_4 = (ImageView) view.findViewById(R.id.shop_menu_4);
        menuMemoView = (TextView) view.findViewById(R.id.menu_memo_view);
        menuMemoLayout = (LinearLayout) view.findViewById(R.id.menu_memo_layout);
        shopMenuImgLayout = (LinearLayout) view.findViewById(R.id.shop_menu_img_layout);

        sendInfoLayoutView = (LinearLayout) view.findViewById(R.id.send_info_layout_view);
        sendWayView = (TextView) view.findViewById(R.id.send_way_view);
        whetherSendTypeView = (TextView) view.findViewById(R.id.whether_send_type_view);
        chargingTypeLayout = (LinearLayout) view.findViewById(R.id.charging_type_layout);
        chargingTypeView = (TextView) view.findViewById(R.id.charging_type_view);
        chargingFreeLayout = (LinearLayout) view.findViewById(R.id.charging_free_layout);
        chargingFreeView = (TextView) view.findViewById(R.id.charging_free_view);
        takeOutDeliverFeeView = (TextView) view.findViewById(R.id.takeOut_deliver_fee_view);
        daDaInfoLayoutView = (LinearLayout) view.findViewById(R.id.da_da_info_layout_view);
        sendPhoneView = (TextView) view.findViewById(R.id.send_phone_view);
        sendContactsView = (TextView) view.findViewById(R.id.send_contacts_view);
        sendEmailView = (TextView) view.findViewById(R.id.send_email_view);
        sendCardNoView = (TextView) view.findViewById(R.id.send_card_no_view);
        sendInfoLayoutSelf = (LinearLayout) view.findViewById(R.id.send_info_layout_self);
        sendFreeEditView = (TextView) view.findViewById(R.id.send_free_edit_view);
        sendMaxDistanceView = (TextView) view.findViewById(R.id.send_max_distance_view);
        sendFirstTimeView = (TextView) view.findViewById(R.id.send_first_time_view);

        payMoneyInfoLayout = (LinearLayout) view.findViewById(R.id.pay_money_info_layout);
        hundredFlagView = (TextView) view.findViewById(R.id.hundred_flag_view);
        serviceChargeView = (TextView) view.findViewById(R.id.service_charge_view);
        settlementCycleView = (TextView) view.findViewById(R.id.settlement_cycle_view);
        serviceChargeLayout = (LinearLayout) view.findViewById(R.id.service_charge_layout);
        settlementCycleLayout = (LinearLayout) view.findViewById(R.id.settlement_cycle_layout);

        privateUserNameEdit = (TextView) view.findViewById(R.id.private_user_name);
        privateIdNumberEdit = (TextView) view.findViewById(R.id.private_id_number);
        privateCardNumberEdit = (TextView) view.findViewById(R.id.private_card_number);
        privateBankNameEdit = (TextView) view.findViewById(R.id.private_bank_name);
        privateImg_1 = (ImageView) view.findViewById(R.id.private_img_1);
        privateImg_2 = (ImageView) view.findViewById(R.id.private_img_2);
        privateImg_3 = (ImageView) view.findViewById(R.id.private_img_3);

        publicUserNameEdit = (TextView) view.findViewById(R.id.public_user_name);
        publicIdNumberEdit = (TextView) view.findViewById(R.id.public_id_number);
        publicCardNumberEdit = (TextView) view.findViewById(R.id.public_card_number);
        publicBankNameEdit = (TextView) view.findViewById(R.id.public_bank_name);
        openingPermitLayout = (LinearLayout) view.findViewById(R.id.opening_permit_layout);
        openingPermitImg = (ImageView) view.findViewById(R.id.opening_permit_img);
        openingPermitEdit = (TextView) view.findViewById(R.id.opening_permit_edit);
        publicImg_1 = (ImageView) view.findViewById(R.id.public_img_1);
        publicImg_2 = (ImageView) view.findViewById(R.id.public_img_2);
        publicImg_3 = (ImageView) view.findViewById(R.id.public_img_3);


        title6View = (TextView) view.findViewById(R.id.title_6_view);
        xcxInfoLayoutView = (LinearLayout) view.findViewById(R.id.xcx_info_layout_view);
        xcxName1View = (TextView) view.findViewById(R.id.xcx_name_1_view);
        xcxName2View = (TextView) view.findViewById(R.id.xcx_name_2_view);
        xcxName3View = (TextView) view.findViewById(R.id.xcx_name_3_view);
        xcxIntroduceView = (TextView) view.findViewById(R.id.xcx_introduce_view);
        xcxLabel1View = (TextView) view.findViewById(R.id.xcx_label_1_view);
        xcxLabel2View = (TextView) view.findViewById(R.id.xcx_label_2_view);
        xcxLabel3View = (TextView) view.findViewById(R.id.xcx_label_3_view);
        xcxLabel4View = (TextView) view.findViewById(R.id.xcx_label_4_view);
        xcxLabel5View = (TextView) view.findViewById(R.id.xcx_label_5_view);
        shopIdeaView = (TextView) view.findViewById(R.id.shop_idea_view);
        sendExplainView = (TextView) view.findViewById(R.id.send_explain_view);
        shopLogoView = (ImageView) view.findViewById(R.id.shop_logo_view);
        groupPhotoView = (ImageView) view.findViewById(R.id.group_photo_view);
        view.findViewById(R.id.detail_log_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomePageActivity)getActivity()).logDetailBack();
            }
        });
        chainInfoInit();
        return view;
    }


    void chainInfoInit(){
        if (expandsLog.getBasicInfoResponseDTO().getPartnerType() != null) {
            if (expandsLog.getBasicInfoResponseDTO().getPartnerType() == 0) {
                joinTypeView.setText("非连锁店");
                cashLayout.setVisibility(View.GONE);
            }
            if (expandsLog.getBasicInfoResponseDTO().getPartnerType() == 1) {
                joinTypeView.setText("总账号");
                cashLayout.setVisibility(View.GONE);
            }
            if (expandsLog.getBasicInfoResponseDTO().getPartnerType() == 2) {
                joinTypeView.setText("总店实体店");
                cashLayout.setVisibility(View.VISIBLE);
                if (expandsLog.getBasicInfoResponseDTO().getSubStoreCashType() == 0) {
                    yesCashImg.setImageResource(R.drawable.radio_false);
                    noCashImg.setImageResource(R.drawable.radio_true);
                } else {
                    yesCashImg.setImageResource(R.drawable.radio_true);
                    noCashImg.setImageResource(R.drawable.radio_false);
                }
            }
            if (expandsLog.getBasicInfoResponseDTO().getPartnerType() == 3) {
                joinTypeView.setText("直营店");
                cashLayout.setVisibility(View.VISIBLE);
                if (expandsLog.getBasicInfoResponseDTO().getSubStoreCashType() == 0) {
                    yesCashImg.setImageResource(R.drawable.radio_false);
                    noCashImg.setImageResource(R.drawable.radio_true);
                } else {
                    yesCashImg.setImageResource(R.drawable.radio_true);
                    noCashImg.setImageResource(R.drawable.radio_false);
                }
            }
            if (expandsLog.getBasicInfoResponseDTO().getPartnerType() == 4) {
                joinTypeView.setText("加盟店");
                cashLayout.setVisibility(View.VISIBLE);
                if (expandsLog.getBasicInfoResponseDTO().getSubStoreCashType() == 0) {
                    yesCashImg.setImageResource(R.drawable.radio_false);
                    noCashImg.setImageResource(R.drawable.radio_true);
                } else {
                    yesCashImg.setImageResource(R.drawable.radio_true);
                    noCashImg.setImageResource(R.drawable.radio_false);
                }
            }





            switch (expandsLog.getBasicInfoResponseDTO().getPartnerType()) {
                case 1:

                    businessLicenseLayout.setVisibility(View.GONE);
                    detailQualificationsLayout.setVisibility(View.GONE);
                    shopInfoLayout.setVisibility(View.GONE);
                    sendInfoLayoutView.setVisibility(View.GONE);
                    payMoneyInfoLayout.setVisibility(View.GONE);

                    shopNameView.setText(expandsLog.getBasicInfoResponseDTO().getStoreName());
                    contactsNameView.setText(expandsLog.getBasicInfoResponseDTO().getContactsName());
                    contactsPhoneView.setText(expandsLog.getBasicInfoResponseDTO().getContactsMobile());
                    if (expandsLog.getBasicInfoResponseDTO().getStoreIndustry() == 1) {
                        radioTrueImg.setImageResource(R.drawable.radio_true);
                        radioFalseImg.setImageResource(R.drawable.radio_false);
                    }
                    if (expandsLog.getBasicInfoResponseDTO().getStoreIndustry() == 2) {
                        radioFalseImg.setImageResource(R.drawable.radio_true);
                        radioTrueImg.setImageResource(R.drawable.radio_false);
                    }
                    if (expandsLog.getBasicInfoResponseDTO().getBusinessLicenseType() == 1) {
                        businessPersonalImg.setImageResource(R.drawable.radio_true);
                        businessCompanyImg.setImageResource(R.drawable.radio_false);
                    }
                    if (expandsLog.getBasicInfoResponseDTO().getBusinessLicenseType() == 2) {
                        businessCompanyImg.setImageResource(R.drawable.radio_true);
                        businessPersonalImg.setImageResource(R.drawable.radio_false);
                    }

                    applyTypeView.setText(getString(R.string.xcx));
                    hundredFlagView.setText(getString(R.string.hundred_flag));
                    title6View.setText(getString(R.string.title_6));

                    serviceChargeLayout.setVisibility(View.GONE);
                    settlementCycleLayout.setVisibility(View.GONE);
                    xcxInfoInit(true);
                    /** 银行卡信息 */
                    bankCardInit();
                    break;
                case 2:
                case 3:
                case 4:
                    xcxInfoLayoutView.setVisibility(View.GONE);
                    businessLicenseLayout.setVisibility(View.VISIBLE);
                    detailQualificationsLayout.setVisibility(View.VISIBLE);
                    shopInfoLayout.setVisibility(View.VISIBLE);
                    sendInfoLayoutView.setVisibility(View.VISIBLE);
                    payMoneyInfoLayout.setVisibility(View.VISIBLE);
                    init(false);
                    break;
                default:
                    xcxInfoLayoutView.setVisibility(View.VISIBLE);
                    businessLicenseLayout.setVisibility(View.VISIBLE);
                    detailQualificationsLayout.setVisibility(View.VISIBLE);
                    shopInfoLayout.setVisibility(View.VISIBLE);
                    sendInfoLayoutView.setVisibility(View.VISIBLE);
                    payMoneyInfoLayout.setVisibility(View.VISIBLE);
                    init(true);
                    break;
            }





        }




    }

    void init(boolean flag){
        BigDecimal rate = new BigDecimal(0);
        if (expandsLog.getOtherInfoResponseDTO().getStoreSource() == 1) {
            applyTypeView.setText(getString(R.string.xcx));
            hundredFlagView.setText(getString(R.string.hundred_flag));
            title6View.setText(getString(R.string.title_6));
            xcxInfoInit(flag);

            rate = expandsLog.getSettlementInfoResponseDTO().getSettlementRate();
            rate = rate.multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP);
            /** 小程序所需的资质信息 */
            detailQualificationsLayout.setVisibility(View.VISIBLE);

            if(expandsLog.getQualificationInfoResponseDTO() != null) {
                if(!StringUtils.isEmpty(expandsLog.getQualificationInfoResponseDTO().getBusinessLicenseUrl()))
                    Picasso.with(mContext).load(expandsLog.getQualificationInfoResponseDTO().getBusinessLicenseUrl()).resize(200, 200).into(imgView1);

                if(!StringUtils.isEmpty(expandsLog.getQualificationInfoResponseDTO().getOrganizationCodeUrl()))
                Picasso.with(mContext).load(expandsLog.getQualificationInfoResponseDTO().getOrganizationCodeUrl()).resize(200, 200).into(imgView2);

                Picasso.with(mContext).load(expandsLog.getQualificationInfoResponseDTO().getTaxRegistrationUrl()).resize(200, 200).into(imgView3);
                Picasso.with(mContext).load(expandsLog.getQualificationInfoResponseDTO().getFoodPermitUrl()).resize(200, 200).into(imgView4);
            }
        } else if (expandsLog.getOtherInfoResponseDTO().getStoreSource() == 2) {
            applyTypeView.setText(getString(R.string.h5));
            hundredFlagView.setText(getString(R.string.thousand_flag));
            title6View.setText(getString(R.string.tile_h5));
            xcxInfoLayoutView.setVisibility(View.GONE);
            businessLicenseLayout.setVisibility(View.VISIBLE);
            detailQualificationsLayout.setVisibility(View.GONE);

            rate = expandsLog.getSettlementInfoResponseDTO().getSettlementRate();
            rate =  rate.multiply(new BigDecimal(1000)).setScale(0, BigDecimal.ROUND_HALF_UP);

            if (!StringUtils.isEmpty(expandsLog.getBasicInfoResponseDTO().getBusinessLicensePhoto())) {
                Picasso.with(mContext).load(expandsLog.getBasicInfoResponseDTO().getBusinessLicensePhoto()).resize(200, 200).into(businessLicenseImg);
            }
        }





        shopNameView.setText(expandsLog.getBasicInfoResponseDTO().getStoreName());
        contactsNameView.setText(expandsLog.getBasicInfoResponseDTO().getContactsName());
        contactsPhoneView.setText(expandsLog.getBasicInfoResponseDTO().getContactsMobile());
        if (expandsLog.getBasicInfoResponseDTO().getStoreIndustry() == 1) {
            radioTrueImg.setImageResource(R.drawable.radio_true);
            radioFalseImg.setImageResource(R.drawable.radio_false);
        }
        if (expandsLog.getBasicInfoResponseDTO().getStoreIndustry() == 2) {
            radioFalseImg.setImageResource(R.drawable.radio_true);
            radioTrueImg.setImageResource(R.drawable.radio_false);
        }
        if (expandsLog.getBasicInfoResponseDTO().getBusinessLicenseType() == 1) {
            businessPersonalImg.setImageResource(R.drawable.radio_true);
            businessCompanyImg.setImageResource(R.drawable.radio_false);
        }
        if (expandsLog.getBasicInfoResponseDTO().getBusinessLicenseType() == 2) {
            businessCompanyImg.setImageResource(R.drawable.radio_true);
            businessPersonalImg.setImageResource(R.drawable.radio_false);
        }


        if(expandsLog == null || expandsLog.getStoreInfoResponseDTO() == null)
            return;

        provinceView.setText(expandsLog.getStoreInfoResponseDTO().getProvince());
        cityView.setText(expandsLog.getStoreInfoResponseDTO().getCity());
        areaView.setText(expandsLog.getStoreInfoResponseDTO().getDistrict());
        detailAddressView.setText(expandsLog.getStoreInfoResponseDTO().getStoreAddress());
        if (expandsLog.getStoreInfoResponseDTO().getDiningStyle() != null
                && (expandsLog.getStoreInfoResponseDTO().getDiningStyle() == 1
                || expandsLog.getStoreInfoResponseDTO().getDiningStyle() == 2)){
            latitudeLongitudeView.setVisibility(View.VISIBLE);
            latitudeLongitudeView.setText(getString(R.string.latitude_and_longitude,
                    expandsLog.getTakeOutInfoResponseDTO().getLongitude(), expandsLog.getTakeOutInfoResponseDTO().getLatitude()));
        } else {
            latitudeLongitudeView.setVisibility(View.GONE);
        }
        String[] openTime = expandsLog.getStoreInfoResponseDTO().getStoreOpeningHour().split(";");
        LogOpenTimeAdapter adapter = new LogOpenTimeAdapter(mContext, openTime);
        for (int i = 0; i < openTime.length; i++) {
            openTimeLayoutView.addView(adapter.getView(i, null, openTimeLayoutView));
        }
        posNumberView.setText(expandsLog.getStoreInfoResponseDTO().getPosNumber().toString());
        posPrintView.setText(expandsLog.getStoreInfoResponseDTO().getReceiptNumber().toString());



        phoneTypeView.setText(expandsLog.getStoreInfoResponseDTO().getMobileModel());
        if (expandsLog.getStoreInfoResponseDTO().getDiningStyle() == 0) businessWayView.setText("堂食");
        else if (expandsLog.getStoreInfoResponseDTO().getDiningStyle() == 1) businessWayView.setText("外卖");
        else if (expandsLog.getStoreInfoResponseDTO().getDiningStyle() == 2) businessWayView.setText("堂食+外卖");
        List<String> menuUrl = expandsLog.getStoreInfoResponseDTO().getMenuUrl();
        if (menuUrl == null || menuUrl.size() <= 0) {
            shopMenuImgLayout.setVisibility(View.GONE);
        } else {
            shopMenuImgLayout.setVisibility(View.VISIBLE);
            for (int index = 0; index < menuUrl.size(); index++) {
                if (index == 0) {
                    shopMenu_1.setVisibility(View.VISIBLE);
                    Picasso.with(mContext).load(menuUrl.get(index)).resize(200, 200).into(shopMenu_1);
                }
                if (index == 1) {
                    shopMenu_2.setVisibility(View.VISIBLE);
                    Picasso.with(mContext).load(menuUrl.get(index)).resize(200, 200).into(shopMenu_2);
                }
                if (index == 2) {
                    shopMenu_3.setVisibility(View.VISIBLE);
                    Picasso.with(mContext).load(menuUrl.get(index)).resize(200, 200).into(shopMenu_3);
                }
                if (index == 3) {
                    shopMenu_4.setVisibility(View.VISIBLE);
                    Picasso.with(mContext).load(menuUrl.get(index)).resize(200, 200).into(shopMenu_4);
                }

            }
        }
        if (StringUtils.isEmpty(expandsLog.getStoreInfoResponseDTO().getStoreMenuMemo())) {
            menuMemoLayout.setVisibility(View.GONE);
        }else {
            menuMemoLayout.setVisibility(View.VISIBLE);
            menuMemoView.setText(expandsLog.getStoreInfoResponseDTO().getStoreMenuMemo());
        }

        if (expandsLog.getStoreInfoResponseDTO().getDiningStyle() == 0 ||
                expandsLog.getStoreInfoResponseDTO().getDiningStyle() == 2) {
            eatLayoutView.setVisibility(View.VISIBLE);
            teaFeeView.setText(expandsLog.getStoreInfoResponseDTO().getTeeFee().toString());

            if (expandsLog.getStoreInfoResponseDTO().getTeeFeeDiscount() != null
                    && expandsLog.getStoreInfoResponseDTO().getTeeFeeDiscount() == 1) {
                teaFeeActivity.setText("是");
            } else if (expandsLog.getStoreInfoResponseDTO().getTeeFeeDiscount() == null
                    || expandsLog.getStoreInfoResponseDTO().getTeeFeeDiscount() == 0) {
                teaFeeActivity.setText("否");
            }
            tableNumberView.setText(expandsLog.getStoreInfoResponseDTO().getNumberOfSite().toString());
            memoView.setText(expandsLog.getStoreInfoResponseDTO().getSeatMemo());
            if (expandsLog.getStoreInfoResponseDTO().getTakeMealReminder() != null) {
                if (expandsLog.getStoreInfoResponseDTO().getTakeMealReminder() == 1) getFoodNotifyView.setText("是");
                else if (expandsLog.getStoreInfoResponseDTO().getTakeMealReminder() == 0) getFoodNotifyView.setText("否");
            }

        } else {
            eatLayoutView.setVisibility(View.GONE);
        }


        if (expandsLog.getStoreInfoResponseDTO().getDiningStyle() == 1 ||
                expandsLog.getStoreInfoResponseDTO().getDiningStyle() == 2) {
            sendInfoLayoutView.setVisibility(View.VISIBLE);
            if (expandsLog.getTakeOutInfoResponseDTO().getEnableSwitchDelivery()) {
                whetherSendTypeView.setText("是");
                chargingTypeLayout.setVisibility(View.VISIBLE);
                if (expandsLog.getTakeOutInfoResponseDTO().getSwitchDeliverFeeType() == 9) {
                    chargingTypeView.setText("固定配送费");
                    chargingFreeLayout.setVisibility(View.VISIBLE);
                    chargingFreeView.setText(expandsLog.getTakeOutInfoResponseDTO().getSwitchableDeliverFeeFixed().toString());
                }
                if (expandsLog.getTakeOutInfoResponseDTO().getSwitchDeliverFeeType() == 10) {
                    chargingTypeView.setText("自送费用");
                    chargingFreeLayout.setVisibility(View.GONE);
                }
                if (expandsLog.getTakeOutInfoResponseDTO().getSwitchDeliverFeeType() == 9) {
                    chargingTypeView.setText("达达配送费用");
                    chargingFreeLayout.setVisibility(View.GONE);
                }


                if (expandsLog.getTakeOutInfoResponseDTO().getDeliverType() == 10) sendWayView.setText("自送");
                else sendWayView.setText("达达配送");
                daDaInfoLayoutView.setVisibility(View.VISIBLE);
                sendInfoLayoutSelf.setVisibility(View.VISIBLE);
                sendFreeEditView.setText(expandsLog.getTakeOutInfoResponseDTO().getDeliverFeeFixed().toString());
                sendMaxDistanceView.setText(expandsLog.getTakeOutInfoResponseDTO().getDeliveryKM().toString());
                sendFirstTimeView.setText(expandsLog.getTakeOutInfoResponseDTO().getStartingInterval().toString());
                sendPhoneView.setText(expandsLog.getTakeOutInfoResponseDTO().getDadaMobile());
                sendContactsView.setText(expandsLog.getTakeOutInfoResponseDTO().getDadaContactsName());
                sendEmailView.setText(expandsLog.getTakeOutInfoResponseDTO().getDadaEmail());
                sendCardNoView.setText(expandsLog.getTakeOutInfoResponseDTO().getContactsIdentifyCard());
            } else {
                whetherSendTypeView.setText("否");
                chargingTypeLayout.setVisibility(View.GONE);
                chargingFreeLayout.setVisibility(View.GONE);

                if (expandsLog.getTakeOutInfoResponseDTO().getDeliverType() == 10) {
                    sendWayView.setText("自送");
                    daDaInfoLayoutView.setVisibility(View.GONE);
                    sendInfoLayoutSelf.setVisibility(View.VISIBLE);
                    sendFreeEditView.setText(expandsLog.getTakeOutInfoResponseDTO().getDeliverFeeFixed().toString());
                    sendMaxDistanceView.setText(expandsLog.getTakeOutInfoResponseDTO().getDeliveryKM().toString());
                    sendFirstTimeView.setText(expandsLog.getTakeOutInfoResponseDTO().getStartingInterval().toString());
                } else if (expandsLog.getTakeOutInfoResponseDTO().getDeliverType() == 11) {
                    sendWayView.setText("达达配送");
                    sendInfoLayoutSelf.setVisibility(View.GONE);
                    daDaInfoLayoutView.setVisibility(View.VISIBLE);
                    sendPhoneView.setText(expandsLog.getTakeOutInfoResponseDTO().getDadaMobile());
                    sendContactsView.setText(expandsLog.getTakeOutInfoResponseDTO().getDadaContactsName());
                    sendEmailView.setText(expandsLog.getTakeOutInfoResponseDTO().getDadaEmail());
                    sendCardNoView.setText(expandsLog.getTakeOutInfoResponseDTO().getContactsIdentifyCard());
                }
            }

            takeOutDeliverFeeView.setText(expandsLog.getTakeOutInfoResponseDTO().getTakeOutDeliverFee().toString());



        }else {
            sendInfoLayoutView.setVisibility(View.GONE);
        }


        serviceChargeLayout.setVisibility(View.VISIBLE);
        settlementCycleLayout.setVisibility(View.VISIBLE);
        serviceChargeView.setText(rate.toString());
        settlementCycleView.setText(expandsLog.getSettlementInfoResponseDTO().getSettlementCycle().toString());

        /** 银行卡信息 */
        bankCardInit();
        /** 合照 */
        Picasso.with(mContext).load(expandsLog.getOtherInfoResponseDTO().getPhoto()).resize(200, 200).into(groupPhotoView);
    }


    void xcxInfoInit(boolean flag) {
        try {
            businessLicenseLayout.setVisibility(View.GONE);
            if (flag) {
                xcxInfoLayoutView.setVisibility(View.VISIBLE);
                xcxName1View.setVisibility(View.GONE);
                xcxName2View.setVisibility(View.GONE);
                xcxName3View.setVisibility(View.GONE);
                String[] xcxNames = expandsLog.getProgramsInfoResponseDTO().getMiniProgramsName().split(",");
                for (int index = 0; index < xcxNames.length; index ++) {
                    if (index == 0) {
                        xcxName1View.setVisibility(View.VISIBLE);
                        xcxName1View.setText(xcxNames[index]);
                    }
                    if (index == 1) {
                        xcxName2View.setVisibility(View.VISIBLE);
                        xcxName2View.setText(xcxNames[index]);
                    }
                    if (index == 2) {
                        xcxName3View.setVisibility(View.VISIBLE);
                        xcxName3View.setText(xcxNames[index]);
                    }
                }
                xcxIntroduceView.setText(expandsLog.getProgramsInfoResponseDTO().getMiniProgramsIntroduce());
                String[] xcxLabels = expandsLog.getProgramsInfoResponseDTO().getMiniProgramsLabel().split(",");
                xcxLabel1View.setVisibility(View.GONE);
                xcxLabel2View.setVisibility(View.GONE);
                xcxLabel3View.setVisibility(View.GONE);
                xcxLabel4View.setVisibility(View.GONE);
                xcxLabel5View.setVisibility(View.GONE);
                for (int index = 0; index < xcxLabels.length; index ++) {
                    if (index == 0) {
                        xcxLabel1View.setVisibility(View.VISIBLE);
                        xcxLabel1View.setText(xcxLabels[index]);
                    }
                    if (index == 1) {
                        xcxLabel2View.setVisibility(View.VISIBLE);
                        xcxLabel2View.setText(xcxLabels[index]);
                    }
                    if (index == 2) {
                        xcxLabel3View.setVisibility(View.VISIBLE);
                        xcxLabel3View.setText(xcxLabels[index]);
                    }
                    if (index == 3) {
                        xcxLabel4View.setVisibility(View.VISIBLE);
                        xcxLabel4View.setText(xcxLabels[index]);
                    }
                    if (index == 4) {
                        xcxLabel5View.setVisibility(View.VISIBLE);
                        xcxLabel5View.setText(xcxLabels[index]);
                    }
                }
            }

            /** 店铺理念和外卖说明 */
            shopIdeaView.setText(expandsLog.getProgramsInfoResponseDTO().getStoreIdea());
            sendExplainView.setText(expandsLog.getProgramsInfoResponseDTO().getTakeOutMemo());
            /** 店铺logo，合照 */
            Picasso.with(mContext).load(expandsLog.getProgramsInfoResponseDTO().getStoreLogo()).resize(200, 200).into(shopLogoView);
            Picasso.with(mContext).load(expandsLog.getOtherInfoResponseDTO().getPhoto()).resize(200, 200).into(groupPhotoView);
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    void bankCardInit(){
        privateUserNameEdit.setText(expandsLog.getSettlementInfoResponseDTO().getPrivateTransferName());
        privateIdNumberEdit.setText(expandsLog.getSettlementInfoResponseDTO().getPrivateTransferIdentifyCard());
        privateCardNumberEdit.setText(expandsLog.getSettlementInfoResponseDTO().getPrivateTransferBankCard());
        privateBankNameEdit.setText(expandsLog.getSettlementInfoResponseDTO().getPrivateTransferBankAccount());

        publicUserNameEdit.setText(expandsLog.getSettlementInfoResponseDTO().getPublicTransferName());
        publicIdNumberEdit.setText(expandsLog.getSettlementInfoResponseDTO().getPublicTransferIdentifyCard());
        publicCardNumberEdit.setText(expandsLog.getSettlementInfoResponseDTO().getPublicTransferBankCard());
        publicBankNameEdit.setText(expandsLog.getSettlementInfoResponseDTO().getPublicTransferBankAccount());
        if (StringUtils.isEmpty(expandsLog.getSettlementInfoResponseDTO().getPublicOpenVoucherId()) &&
                StringUtils.isEmpty(expandsLog.getSettlementInfoResponseDTO().getPublicOpenVoucherCode())) {
            openingPermitLayout.setVisibility(View.GONE);
        } else if (!StringUtils.isEmpty(expandsLog.getSettlementInfoResponseDTO().getPublicOpenVoucherId()) &&
                StringUtils.isEmpty(expandsLog.getSettlementInfoResponseDTO().getPublicOpenVoucherCode())){
            openingPermitLayout.setVisibility(View.VISIBLE);
            openingPermitEdit.setVisibility(View.GONE);
            Picasso.with(mContext).load(expandsLog.getSettlementInfoResponseDTO().getPublicOpenVoucherUrl()).
                    placeholder(R.drawable.loading).resize(200, 200).into(openingPermitImg);
        } else if (StringUtils.isEmpty(expandsLog.getSettlementInfoResponseDTO().getPublicOpenVoucherId()) &&
                !StringUtils.isEmpty(expandsLog.getSettlementInfoResponseDTO().getPublicOpenVoucherCode())){
            openingPermitLayout.setVisibility(View.VISIBLE);
            openingPermitImg.setVisibility(View.GONE);
            openingPermitEdit.setText(expandsLog.getSettlementInfoResponseDTO().getPublicOpenVoucherCode());
        } else {
            openingPermitLayout.setVisibility(View.VISIBLE);
            Picasso.with(mContext).load(expandsLog.getSettlementInfoResponseDTO().getPublicOpenVoucherUrl()).
                    placeholder(R.drawable.loading).resize(200, 200).into(openingPermitImg);
            openingPermitEdit.setText(expandsLog.getSettlementInfoResponseDTO().getPublicOpenVoucherCode());
        }


        if (!StringUtils.isEmpty(expandsLog.getSettlementInfoResponseDTO().getPrivateTransferBankCardPhoto())) {
            Picasso.with(mContext).load(expandsLog.getSettlementInfoResponseDTO().getPrivateTransferBankCardPhoto()).resize(200, 200).into(privateImg_1);
        }
        if (!StringUtils.isEmpty(expandsLog.getSettlementInfoResponseDTO().getPrivateTransferIdCardFront())) {
            Picasso.with(mContext).load(expandsLog.getSettlementInfoResponseDTO().getPrivateTransferIdCardFront()).resize(200, 200).into(privateImg_2);
        }
        if (!StringUtils.isEmpty(expandsLog.getSettlementInfoResponseDTO().getPrivateTransferIdCardBack())) {
            Picasso.with(mContext).load(expandsLog.getSettlementInfoResponseDTO().getPrivateTransferIdCardBack()).resize(200, 200).into(privateImg_3);
        }

        if (!StringUtils.isEmpty(expandsLog.getSettlementInfoResponseDTO().getPublicTransferBankCardPhoto())) {
            Picasso.with(mContext).load(expandsLog.getSettlementInfoResponseDTO().getPublicTransferBankCardPhoto()).resize(200, 200).into(publicImg_1);
        }
        if (!StringUtils.isEmpty(expandsLog.getSettlementInfoResponseDTO().getPublicTransferIdCardFront())) {
            Picasso.with(mContext).load(expandsLog.getSettlementInfoResponseDTO().getPublicTransferIdCardFront()).resize(200, 200).into(publicImg_2);
        }
        if (!StringUtils.isEmpty(expandsLog.getSettlementInfoResponseDTO().getPublicTransferIdCardBack())) {
            Picasso.with(mContext).load(expandsLog.getSettlementInfoResponseDTO().getPublicTransferIdCardBack()).resize(200, 200).into(publicImg_3);
        }
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            ExpandsLogRes hiddenExpandsLog = getArguments().getParcelable(Constants.DATA);
            if (!hiddenExpandsLog.getOtherInfoResponseDTO().getAuditId().
                    equals(expandsLog.getOtherInfoResponseDTO().getAuditId())) {
                openTimeLayoutView.removeAllViews();
                expandsLog = hiddenExpandsLog;
                chainInfoInit();
            }
        }

    }
}
