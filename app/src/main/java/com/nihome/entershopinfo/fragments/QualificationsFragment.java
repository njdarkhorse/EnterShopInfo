package com.nihome.entershopinfo.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nihome.entershopinfo.Constants;
import com.nihome.entershopinfo.R;
import com.nihome.entershopinfo.application.SysApplication;
import com.nihome.entershopinfo.dialog.ImagePickDialog;
import com.nihome.entershopinfo.dialog.LoadingDialog;
import com.nihome.entershopinfo.entity.User;
import com.nihome.entershopinfo.entity.request.QualificationInfoReq;
import com.nihome.entershopinfo.entity.request.UploadImgReq;
import com.nihome.entershopinfo.entity.response.ExpandsLogRes;
import com.nihome.entershopinfo.entity.response.ResponseBase;
import com.nihome.entershopinfo.entity.response.UploadImgRes;
import com.nihome.entershopinfo.ui.HomePageActivity;
import com.nihome.entershopinfo.ui.PermissionsActivity;
import com.nihome.entershopinfo.utils.Base64Util;
import com.nihome.entershopinfo.utils.BitmapUtils;
import com.nihome.entershopinfo.utils.ImagePicker;
import com.nihome.entershopinfo.utils.Log;
import com.nihome.entershopinfo.utils.NetUtil;
import com.nihome.entershopinfo.utils.PermissionsChecker;
import com.nihome.entershopinfo.utils.RequestAPI;
import com.nihome.entershopinfo.utils.StringUtils;
import com.squareup.picasso.Picasso;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.Response;


/**
 * Created by Carson on 2017/7/18.
 */
public class QualificationsFragment extends Fragment implements View.OnClickListener, ImagePickDialog.DialogSelectedListener{
    private static final String TAG = "QualificationsFragment";
    private static final int IMG_1 = 1;
    private static final int IMG_2 = 2;
    private static final int IMG_3 = 3;
    private static final int IMG_4 = 4;

    private PermissionsChecker mPermissionsChecker; // 权限检测器
    static final String[] PERMISSIONS = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};
    private static final int REQUEST_PERMISSION = 100;  //权限请求
    private ImagePickDialog imagePickDialog;
    private ImagePicker mImagePicker;
    private Uri cameraUri;
    private Uri cutImgUri;
    private int IMG_TYPE;

    private Context mContext;
    private Gson mGson;
    private NetUtil mNetUtil;
    private User mUser;
    private LoadingDialog dialog;
    private int selectType;
    private ExpandsLogRes expandsLog;
    private QualificationInfoReq qualificationInfoReq;
    private String auditId;
    private short operation;

    private TextView infoPrompt;
    private ImageView imgBtn_1;
    private ImageView imgBtn_2;
    private ImageView imgBtn_3;
    private ImageView imgBtn_4;
    private TextView backBtn;
    private TextView goNext;

    private String reqImg_1;
    private String reqImg_2;
    private String reqImg_3;
    private String reqImg_4;
    private String reqImgUrl_1;
    private String reqImgUrl_2;
    private String reqImgUrl_3;
    private String reqImgUrl_4;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mGson = new Gson();
        mNetUtil = new NetUtil(mContext);
        mUser = SysApplication.getInstance().getmUser();
        mPermissionsChecker = new PermissionsChecker(mContext);
        dialog = new LoadingDialog(mContext);
        selectType = getArguments().getInt(Constants.SELECT_TYPE);
        expandsLog = getArguments().getParcelable(Constants.DATA);
        auditId = SysApplication.getInstance().getAuditId();
        if (expandsLog.getQualificationInfoResponseDTO() == null) operation = Constants.NEW;
        else operation = Constants.UPDATE;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_qualifications_info, container, false);
        infoPrompt = (TextView) view.findViewById(R.id.info_prompt);
        imgBtn_1 = (ImageView) view.findViewById(R.id.img_btn_1);
        imgBtn_2 = (ImageView) view.findViewById(R.id.img_btn_2);
        imgBtn_3 = (ImageView) view.findViewById(R.id.img_btn_3);
        imgBtn_4 = (ImageView) view.findViewById(R.id.img_btn_4);
        goNext = (TextView) view.findViewById(R.id.go_next);
        backBtn = (TextView) view.findViewById(R.id.back_btn);

        infoPrompt.setText(Html.fromHtml(getString(R.string.basic_info_prompt)));
        imgBtn_1.setOnClickListener(this);
        imgBtn_2.setOnClickListener(this);
        imgBtn_3.setOnClickListener(this);
        imgBtn_4.setOnClickListener(this);
        backBtn.setOnClickListener(this);
        goNext.setOnClickListener(this);
        if (operation == Constants.UPDATE) initData();
        return view;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            selectType = getArguments().getInt(Constants.SELECT_TYPE);
            expandsLog = getArguments().getParcelable(Constants.DATA);
            auditId = SysApplication.getInstance().getAuditId();
            if (expandsLog.getQualificationInfoResponseDTO() == null) operation = Constants.NEW;
            else operation = Constants.UPDATE;
        }
        if (operation == Constants.NEW) viewInit();
        if (operation == Constants.UPDATE) initData();
    }


    void initData(){
        Picasso.with(mContext).load(expandsLog.getQualificationInfoResponseDTO().getBusinessLicenseUrl()).
                placeholder(R.drawable.loading).resize(200, 200).into(imgBtn_1);
        Picasso.with(mContext).load(expandsLog.getQualificationInfoResponseDTO().getOrganizationCodeUrl()).
                placeholder(R.drawable.loading).resize(200, 200).into(imgBtn_2);
        Picasso.with(mContext).load(expandsLog.getQualificationInfoResponseDTO().getTaxRegistrationUrl()).
                placeholder(R.drawable.loading).resize(200, 200).into(imgBtn_3);
        Picasso.with(mContext).load(expandsLog.getQualificationInfoResponseDTO().getFoodPermitUrl()).
                placeholder(R.drawable.loading).resize(200, 200).into(imgBtn_4);
        reqImg_1 = expandsLog.getQualificationInfoResponseDTO().getBusinessLicenseFileId();
        reqImg_2 = expandsLog.getQualificationInfoResponseDTO().getOrganizationCodeFileId();
        reqImg_3 = expandsLog.getQualificationInfoResponseDTO().getTaxRegistrationFileId();
        reqImg_4 = expandsLog.getQualificationInfoResponseDTO().getFoodPermitFileId();
        reqImgUrl_1 = expandsLog.getQualificationInfoResponseDTO().getBusinessLicenseUrl();
        reqImgUrl_2 = expandsLog.getQualificationInfoResponseDTO().getOrganizationCodeUrl();
        reqImgUrl_3 = expandsLog.getQualificationInfoResponseDTO().getTaxRegistrationUrl();
        reqImgUrl_4 = expandsLog.getQualificationInfoResponseDTO().getFoodPermitUrl();
    }

    void viewInit(){
        imgBtn_1.setImageResource(R.drawable.upload_img);
        imgBtn_2.setImageResource(R.drawable.upload_img);
        imgBtn_3.setImageResource(R.drawable.upload_img);
        imgBtn_4.setImageResource(R.drawable.upload_img);
        reqImg_1 = "";
        reqImg_2 = "";
        reqImg_3 = "";
        reqImg_4 = "";
        reqImgUrl_1 = "";
        reqImgUrl_2 = "";
        reqImgUrl_3 = "";
        reqImgUrl_4 = "";
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_btn_1:
                IMG_TYPE = IMG_1;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (mPermissionsChecker.lacksPermissions(PERMISSIONS)) {
                        startPermissionsActivity();
                    } else {
                        showDialog();
                    }
                } else {
                    showDialog();
                }
                break;
            case R.id.img_btn_2:
                IMG_TYPE = IMG_2;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (mPermissionsChecker.lacksPermissions(PERMISSIONS)) {
                        startPermissionsActivity();
                    } else {
                        showDialog();
                    }
                } else {
                    showDialog();
                }
                break;
            case R.id.img_btn_3:
                IMG_TYPE = IMG_3;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (mPermissionsChecker.lacksPermissions(PERMISSIONS)) {
                        startPermissionsActivity();
                    } else {
                        showDialog();
                    }
                } else {
                    showDialog();
                }
                break;
            case R.id.img_btn_4:
                IMG_TYPE = IMG_4;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (mPermissionsChecker.lacksPermissions(PERMISSIONS)) {
                        startPermissionsActivity();
                    } else {
                        showDialog();
                    }
                } else {
                    showDialog();
                }
                break;
            case R.id.back_btn:
                ((HomePageActivity)getActivity()).backClick();
                break;
            case R.id.go_next:
                //((HomePageActivity) getActivity()).goNextOfQualifications();
                if (!mNetUtil.isNetConnected()) {
                    Toast.makeText(mContext, getString(R.string.cannot_connect_network), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (check()) subQualificationInfo();
                else Toast.makeText(mContext, getString(R.string.submit_prompt), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private Boolean check(){
        if (StringUtils.isEmpty(reqImg_1)) return false;
        if (StringUtils.isEmpty(reqImg_2)) return false;
        if (StringUtils.isEmpty(reqImg_3)) return false;
        if (StringUtils.isEmpty(reqImg_4)) return false;
        return true;
    }


    private void subQualificationInfo(){
        dialog.show(getString(R.string.load_text));
        qualificationInfoReq = new QualificationInfoReq();
        qualificationInfoReq.setAuditId(auditId);
        qualificationInfoReq.setMarketerId(mUser.getUserId());
        qualificationInfoReq.setOperation(operation);
        qualificationInfoReq.setBusinessLicenseFileId(reqImg_1);
        qualificationInfoReq.setOrganizationCodeFileId(reqImg_2);
        qualificationInfoReq.setTaxRegistrationFileId(reqImg_3);
        qualificationInfoReq.setFoodPermitFileId(reqImg_4);
        qualificationInfoReq.setBusinessLicenseUrl(reqImgUrl_1);
        qualificationInfoReq.setOrganizationCodeUrl(reqImgUrl_2);
        qualificationInfoReq.setTaxRegistrationUrl(reqImgUrl_3);
        qualificationInfoReq.setFoodPermitUrl(reqImgUrl_4);
        Log.e(TAG, "subQualificationInfo json = " + mGson.toJson(qualificationInfoReq));
        OkHttpUtils.postString().url(RequestAPI.QUALIFICATIONS_URL)
                .content(new Gson().toJson(qualificationInfoReq))
                .mediaType(MediaType.parse("application/json; charset=utf-8"))
                .build().execute(new QualificationInfoCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.e(TAG, e + "");
                dialog.dismiss();
                if (e instanceof UnknownHostException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (e instanceof SocketException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (e instanceof SocketTimeoutException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (e instanceof IOException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }

                Toast.makeText(mContext, getString(R.string.service_error), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(ResponseBase<String> response, int id) {
                dialog.dismiss();
                switch (Integer.valueOf(response.getResultCode())) {
                    case Constants.REQ_SUCCESS:
                        expandsLog.setQualificationInfoResponseDTO(qualificationInfoReq);
                        ((HomePageActivity) getActivity()).goNextOfQualifications();
                        break;
                    default:
                        Toast.makeText(mContext, response.getMsg(), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }

    abstract class QualificationInfoCallback extends Callback<ResponseBase<String>> {
        @Override
        public ResponseBase<String> parseNetworkResponse(Response response, int id) throws Exception {
            String string = response.body().string();
            Log.e(TAG, string);
            Type jsonType = new TypeToken<ResponseBase<String>>(){}.getType();
            ResponseBase<String> res = mGson.fromJson(string, jsonType);
            return res;
        }
    }


    /** 上传图片 */
    private void uploadImgService(){
        dialog.show(getString(R.string.load_text));
        Log.e(TAG, "image path = " + cutImgUri.getPath());
        String img = "data:image/png;base64,";
        img +=  Base64Util.encode(BitmapUtils.FileTobyte(new File(cutImgUri.getPath())));
        UploadImgReq uploadImgReq = new UploadImgReq();
        uploadImgReq.setFileBase64Data(img);
        uploadImgReq.setUserId(mUser.getUserId());
        OkHttpUtils.postString().url(RequestAPI.UPLOAD_IMG_URL)
                .content(new Gson().toJson(uploadImgReq))
                .mediaType(MediaType.parse("application/json; charset=utf-8"))
                .build().execute(new UploadImgCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                dialog.dismiss();
                Toast.makeText(mContext, getString(R.string.service_error), Toast.LENGTH_SHORT).show();
                Log.e(TAG, e + "");
            }

            @Override
            public void onResponse(ResponseBase<UploadImgRes> response, int id) {
                dialog.dismiss();
                switch (Integer.valueOf(response.getResultCode())) {
                    case Constants.REQ_SUCCESS:
                        Toast.makeText(mContext, getString(R.string.upload_success), Toast.LENGTH_SHORT).show();
                        if (IMG_TYPE == IMG_1) {
                            reqImg_1 = response.getResult().getFileId();
                            reqImgUrl_1 = response.getResult().getFileUrl();
                            Picasso.with(mContext).load(response.getResult().getFileUrl()).
                                    placeholder(R.drawable.loading).resize(200, 200).into(imgBtn_1);
                        }
                        if (IMG_TYPE == IMG_2) {
                            reqImg_2 = response.getResult().getFileId();
                            reqImgUrl_2 = response.getResult().getFileUrl();
                            Picasso.with(mContext).load(response.getResult().getFileUrl()).
                                    placeholder(R.drawable.loading).resize(200, 200).into(imgBtn_2);
                        }
                        if (IMG_TYPE == IMG_3) {
                            reqImg_3 = response.getResult().getFileId();
                            reqImgUrl_3 = response.getResult().getFileUrl();
                            Picasso.with(mContext).load(response.getResult().getFileUrl()).
                                    placeholder(R.drawable.loading).resize(200, 200).into(imgBtn_3);
                        }
                        if (IMG_TYPE == IMG_4) {
                            reqImg_4 = response.getResult().getFileId();
                            reqImgUrl_4 = response.getResult().getFileUrl();
                            Picasso.with(mContext).load(response.getResult().getFileUrl()).
                                    placeholder(R.drawable.loading).resize(200, 200).into(imgBtn_4);
                        }
                        break;
                    default:
                        Toast.makeText(mContext, response.getMsg(), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }

    abstract class UploadImgCallback extends Callback<ResponseBase<UploadImgRes>> {
        @Override
        public ResponseBase<UploadImgRes> parseNetworkResponse(Response response, int id) throws Exception {
            String string = response.body().string();
            Log.e(TAG, string);
            Type jsonType = new TypeToken<ResponseBase<UploadImgRes>>(){}.getType();
            ResponseBase<UploadImgRes> res = mGson.fromJson(string, jsonType);
            return res;
        }
    }

    private void startPermissionsActivity() {
        PermissionsActivity.startActivityForResult(getActivity(), REQUEST_PERMISSION,
                PERMISSIONS);
    }

    private void showDialog(){
        if (imagePickDialog == null) imagePickDialog = new ImagePickDialog(mContext);
        imagePickDialog.setDialogSelectedListener(this);
        imagePickDialog.show();
    }


    @Override
    public void onPhone() {
        Log.e(TAG, "相册........");
        if (mImagePicker == null) mImagePicker = new ImagePicker(getActivity(), this);
        mImagePicker.pickOnphone();
    }


    @Override
    public void onCapture() {
        Log.e(TAG, "拍照........");
        if (mImagePicker == null) mImagePicker = new ImagePicker(getActivity(), this);
        cameraUri = mImagePicker.openCamera();
    }

    @Override
    public void onCancel() {

    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case ImagePicker.REQUEST_CAPTURE:
                    cutImgUri = mImagePicker.cropPhoto(cameraUri);
                    break;
                case ImagePicker.REQUEST_PICK_IMAGE:
                    cameraUri = data.getData(); //获取系统返回的照片的Uri
                    cutImgUri = mImagePicker.cropPhoto(cameraUri);
                    break;
                case ImagePicker.REQUEST_PICTURE_CUT:
                    if (!mNetUtil.isNetConnected()) {
                        Toast.makeText(mContext, getString(R.string.cannot_connect_network), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    uploadImgService();
                    break;
            }
        }
    }
}
