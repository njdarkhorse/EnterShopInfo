package com.nihome.entershopinfo.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nihome.entershopinfo.Constants;
import com.nihome.entershopinfo.R;
import com.nihome.entershopinfo.application.SysApplication;
import com.nihome.entershopinfo.dialog.ImagePickDialog;
import com.nihome.entershopinfo.dialog.LoadingDialog;
import com.nihome.entershopinfo.entity.User;
import com.nihome.entershopinfo.entity.request.SettlementInfoReq;
import com.nihome.entershopinfo.entity.request.UploadImgReq;
import com.nihome.entershopinfo.entity.response.ExpandsLogRes;
import com.nihome.entershopinfo.entity.response.ResponseBase;
import com.nihome.entershopinfo.entity.response.UploadImgRes;
import com.nihome.entershopinfo.ui.HomePageActivity;
import com.nihome.entershopinfo.ui.PermissionsActivity;
import com.nihome.entershopinfo.utils.Base64Util;
import com.nihome.entershopinfo.utils.BitmapUtils;
import com.nihome.entershopinfo.utils.ImagePicker;
import com.nihome.entershopinfo.utils.Log;
import com.nihome.entershopinfo.utils.NetUtil;
import com.nihome.entershopinfo.utils.PermissionsChecker;
import com.nihome.entershopinfo.utils.RequestAPI;
import com.nihome.entershopinfo.utils.StringUtils;
import com.squareup.picasso.Picasso;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.Response;

/**
 * Created by Carson on 2017/7/20.
 */
public class SettlementInfoFragment extends Fragment implements View.OnClickListener, ImagePickDialog.DialogSelectedListener {
    private static final String TAG = "SettlementInfoFragment";

    private PermissionsChecker mPermissionsChecker; // 权限检测器
    static final String[] PERMISSIONS = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};
    private static final int REQUEST_PERMISSION = 100;  //权限请求
    private ImagePickDialog imagePickDialog;
    private ImagePicker mImagePicker;
    private Uri cameraUri;
    private Uri cutImgUri;

    private int IMG_TYPE;
    private static final int PRIVATE_1 = 1;
    private static final int PRIVATE_2 = 2;
    private static final int PRIVATE_3 = 3;
    private static final int PUBLIC_1 = 10;
    private static final int PUBLIC_2 = 20;
    private static final int PUBLIC_3 = 30;
    private static final int OPEN_ING = 40;

    private Context mContext;
    private User mUser;
    private NetUtil mNetUtil;
    private Gson mGson;
    private LoadingDialog dialog;
    private int selectType;
    private ExpandsLogRes expandsLog;
    private SettlementInfoReq settlementInfoReq;
    private String auditId;
    private short operation;

    private LinearLayout serviceChargeLayout;
    private LinearLayout settlementCycleLayout;

    private TextView settlementInfoPrompt;
    private EditText serviceChargeEdit;
    private TextView serviceChargeFlag;
    private EditText settlementCycleEdit;

    private EditText privateUserNameEdit;
    private EditText privateIdNumberEdit;
    private EditText privateCardNumberEdit;
    private EditText privateBankNameEdit;
    private ImageView privatePhone_1;
    private ImageView privatePhone_2;
    private ImageView privatePhone_3;


    private EditText publicUserNameEdit;
    private EditText publicIdNumberEdit;
    private EditText publicCardNumberEdit;
    private EditText publicBankNameEdit;
    private ImageView publicPhone_1;
    private ImageView publicPhone_2;
    private ImageView publicPhone_3;

    private LinearLayout openingPermitLayout;
    private ImageView openingPermitImg;
    private EditText openingPermitEdit;

    private TextView backBtn;
    private TextView goNextBtn;


    private String imgId;
    private String privateImgId_1;
    private String privateImgId_2;
    private String privateImgId_3;
    private String publicImgId_1;
    private String publicImgId_2;
    private String publicImgId_3;
    private String openingPermitImgUrl;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mNetUtil = new NetUtil(mContext);
        mUser = SysApplication.getInstance().getmUser();
        mGson = new Gson();
        dialog = new LoadingDialog(mContext);
        mPermissionsChecker = new PermissionsChecker(mContext);
        selectType = getArguments().getInt(Constants.SELECT_TYPE);
        expandsLog = getArguments().getParcelable(Constants.DATA);
        auditId = SysApplication.getInstance().getAuditId();
        if (expandsLog.getSettlementInfoResponseDTO() == null) operation = Constants.NEW;
        else operation = Constants.UPDATE;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settlement_info, container, false);

        serviceChargeLayout = (LinearLayout) view.findViewById(R.id.service_charge_layout);
        settlementCycleLayout = (LinearLayout) view.findViewById(R.id.settlement_cycle_layout);

        settlementInfoPrompt = (TextView) view.findViewById(R.id.settlement_info_prompt);
        serviceChargeEdit = (EditText) view.findViewById(R.id.service_charge_edit);
        serviceChargeFlag = (TextView) view.findViewById(R.id.service_charge_flag);
        settlementCycleEdit = (EditText) view.findViewById(R.id.settlement_cycle_edit);

        privateUserNameEdit = (EditText) view.findViewById(R.id.private_user_name);
        privateIdNumberEdit = (EditText) view.findViewById(R.id.private_id_number);
        privateCardNumberEdit = (EditText) view.findViewById(R.id.private_card_number);
        privateBankNameEdit = (EditText) view.findViewById(R.id.private_bank_name);
        privatePhone_1 = (ImageView) view.findViewById(R.id.private_bank_card_phone);
        privatePhone_2 = (ImageView) view.findViewById(R.id.private_bank_id_phone_1);
        privatePhone_3 = (ImageView) view.findViewById(R.id.private_bank_id_phone_2);

        publicUserNameEdit = (EditText) view.findViewById(R.id.public_user_name);
        publicIdNumberEdit = (EditText) view.findViewById(R.id.public_id_number);
        publicCardNumberEdit = (EditText) view.findViewById(R.id.public_card_number);
        publicBankNameEdit = (EditText) view.findViewById(R.id.public_bank_name);
        publicPhone_1 = (ImageView) view.findViewById(R.id.public_bank_card_phone);
        publicPhone_2 = (ImageView) view.findViewById(R.id.public_bank_id_phone_1);
        publicPhone_3 = (ImageView) view.findViewById(R.id.public_bank_id_phone_2);
        openingPermitLayout = (LinearLayout) view.findViewById(R.id.opening_permit_layout);
        openingPermitImg = (ImageView) view.findViewById(R.id.opening_permit_img);
        openingPermitEdit = (EditText) view.findViewById(R.id.opening_permit_edit);


        backBtn = (TextView) view.findViewById(R.id.settlement_info_back);
        goNextBtn = (TextView) view.findViewById(R.id.settlement_info_go_next);

        openingPermitImg.setOnClickListener(this);
        backBtn.setOnClickListener(this);
        goNextBtn.setOnClickListener(this);
        privatePhone_1.setOnClickListener(this);
        privatePhone_2.setOnClickListener(this);
        privatePhone_3.setOnClickListener(this);
        publicPhone_1.setOnClickListener(this);
        publicPhone_2.setOnClickListener(this);
        publicPhone_3.setOnClickListener(this);

        settlementInfoPrompt.setText(Html.fromHtml(getString(R.string.info_prompt_3)));
        if (selectType == Constants.XCX_TYPE || selectType == Constants.CHAIN_CHILD_TYPE) {
            serviceChargeLayout.setVisibility(View.VISIBLE);
            settlementCycleLayout.setVisibility(View.VISIBLE);
            serviceChargeFlag.setText(getString(R.string.hundred_flag));
            settlementCycleEdit.setText("3");
            settlementCycleEdit.setEnabled(false);
        } else if (selectType == Constants.H5_TYPE) {
            serviceChargeLayout.setVisibility(View.VISIBLE);
            settlementCycleLayout.setVisibility(View.VISIBLE);
            serviceChargeFlag.setText(getString(R.string.thousand_flag));
            settlementCycleEdit.setText("");
            settlementCycleEdit.setEnabled(true);
        } else if (selectType == Constants.CHAIN_TYPE) {
            serviceChargeLayout.setVisibility(View.GONE);
            settlementCycleLayout.setVisibility(View.GONE);
        }
        if (operation == Constants.UPDATE) initData();
        return view;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            selectType = getArguments().getInt(Constants.SELECT_TYPE);
            expandsLog = getArguments().getParcelable(Constants.DATA);
            auditId = SysApplication.getInstance().getAuditId();
            if (expandsLog.getSettlementInfoResponseDTO() == null) operation = Constants.NEW;
            else operation = Constants.UPDATE;
        }
        if (selectType == Constants.XCX_TYPE || selectType == Constants.CHAIN_CHILD_TYPE) {
            serviceChargeLayout.setVisibility(View.VISIBLE);
            settlementCycleLayout.setVisibility(View.VISIBLE);
            serviceChargeFlag.setText(getString(R.string.hundred_flag));
            settlementCycleEdit.setText("3");
            settlementCycleEdit.setEnabled(false);
        } else if (selectType == Constants.H5_TYPE) {
            serviceChargeLayout.setVisibility(View.VISIBLE);
            settlementCycleLayout.setVisibility(View.VISIBLE);
            serviceChargeFlag.setText(getString(R.string.thousand_flag));
            settlementCycleEdit.setText("");
            settlementCycleEdit.setEnabled(true);
        } else if (selectType == Constants.CHAIN_TYPE) {
            serviceChargeLayout.setVisibility(View.GONE);
            settlementCycleLayout.setVisibility(View.GONE);
        }
        if (operation == Constants.NEW) viewInit();
        if (operation == Constants.UPDATE) initData();
    }

    private void initData() {
        clearImg();
        BigDecimal rate = new BigDecimal(0);
        if (selectType == Constants.XCX_TYPE) {
            rate = expandsLog.getSettlementInfoResponseDTO().getSettlementRate();
            rate = rate.multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP);
        }
        if (selectType == Constants.H5_TYPE) {
            rate = expandsLog.getSettlementInfoResponseDTO().getSettlementRate();
            rate = rate.multiply(new BigDecimal(1000)).setScale(0, BigDecimal.ROUND_HALF_UP);
        }
        serviceChargeEdit.setText(rate.toString());
        settlementCycleEdit.setText(expandsLog.getSettlementInfoResponseDTO().getSettlementCycle().toString());

        privateUserNameEdit.setText(expandsLog.getSettlementInfoResponseDTO().getPrivateTransferName());
        privateIdNumberEdit.setText(expandsLog.getSettlementInfoResponseDTO().getPrivateTransferIdentifyCard());
        privateCardNumberEdit.setText(expandsLog.getSettlementInfoResponseDTO().getPrivateTransferBankCard());
        privateBankNameEdit.setText(expandsLog.getSettlementInfoResponseDTO().getPrivateTransferBankAccount());
        if (!StringUtils.isEmpty(expandsLog.getSettlementInfoResponseDTO().getPrivateTransferBankCardPhoto())) {
            privateImgId_1 = expandsLog.getSettlementInfoResponseDTO().getPrivateTransferBankCardPhoto();
            Picasso.with(mContext).load(expandsLog.getSettlementInfoResponseDTO().getPrivateTransferBankCardPhoto()).
                    placeholder(R.drawable.loading).resize(200, 200).into(privatePhone_1);
        }
        if (!StringUtils.isEmpty(expandsLog.getSettlementInfoResponseDTO().getPrivateTransferIdCardFront())) {
            privateImgId_2 = expandsLog.getSettlementInfoResponseDTO().getPrivateTransferIdCardFront();
            Picasso.with(mContext).load(expandsLog.getSettlementInfoResponseDTO().getPrivateTransferIdCardFront()).
                    placeholder(R.drawable.loading).resize(200, 200).into(privatePhone_2);
        }
        if (!StringUtils.isEmpty(expandsLog.getSettlementInfoResponseDTO().getPrivateTransferIdCardBack())) {
            privateImgId_3 = expandsLog.getSettlementInfoResponseDTO().getPrivateTransferIdCardBack();
            Picasso.with(mContext).load(expandsLog.getSettlementInfoResponseDTO().getPrivateTransferIdCardBack()).
                    placeholder(R.drawable.loading).resize(200, 200).into(privatePhone_3);
        }

        publicUserNameEdit.setText(expandsLog.getSettlementInfoResponseDTO().getPublicTransferName());
        publicIdNumberEdit.setText(expandsLog.getSettlementInfoResponseDTO().getPublicTransferIdentifyCard());
        publicCardNumberEdit.setText(expandsLog.getSettlementInfoResponseDTO().getPublicTransferBankCard());
        publicBankNameEdit.setText(expandsLog.getSettlementInfoResponseDTO().getPublicTransferBankAccount());
        if (!StringUtils.isEmpty(expandsLog.getSettlementInfoResponseDTO().getPublicTransferBankCardPhoto())) {
            publicImgId_1 = expandsLog.getSettlementInfoResponseDTO().getPublicTransferBankCardPhoto();
            Picasso.with(mContext).load(expandsLog.getSettlementInfoResponseDTO().getPublicTransferBankCardPhoto()).
                    placeholder(R.drawable.loading).resize(200, 200).into(publicPhone_1);
        }
        if (!StringUtils.isEmpty(expandsLog.getSettlementInfoResponseDTO().getPublicTransferIdCardFront())) {
            publicImgId_2 = expandsLog.getSettlementInfoResponseDTO().getPublicTransferIdCardFront();
            Picasso.with(mContext).load(expandsLog.getSettlementInfoResponseDTO().getPublicTransferIdCardFront()).
                    placeholder(R.drawable.loading).resize(200, 200).into(publicPhone_2);
        }
        if (!StringUtils.isEmpty(expandsLog.getSettlementInfoResponseDTO().getPublicTransferIdCardFront())) {
            publicImgId_3 = expandsLog.getSettlementInfoResponseDTO().getPublicTransferIdCardBack();
            Picasso.with(mContext).load(expandsLog.getSettlementInfoResponseDTO().getPublicTransferIdCardBack()).
                    placeholder(R.drawable.loading).resize(200, 200).into(publicPhone_3);
        }


        if (!StringUtils.isEmpty(expandsLog.getSettlementInfoResponseDTO().getPublicOpenVoucherId())) {
            imgId = expandsLog.getSettlementInfoResponseDTO().getPublicOpenVoucherId();
            openingPermitImgUrl = expandsLog.getSettlementInfoResponseDTO().getPublicOpenVoucherUrl();
            Picasso.with(mContext).load(expandsLog.getSettlementInfoResponseDTO().getPublicOpenVoucherUrl()).
                    placeholder(R.drawable.loading).resize(200, 200).into(openingPermitImg);
        }
        if (!StringUtils.isEmpty(expandsLog.getSettlementInfoResponseDTO().getPublicOpenVoucherCode())) {
            openingPermitEdit.setText(expandsLog.getSettlementInfoResponseDTO().getPublicOpenVoucherCode());
        }
    }

    void viewInit() {
        serviceChargeEdit.setText("");
        if (selectType == Constants.XCX_TYPE || selectType == Constants.CHAIN_CHILD_TYPE) {
            serviceChargeLayout.setVisibility(View.VISIBLE);
            settlementCycleLayout.setVisibility(View.VISIBLE);
            serviceChargeFlag.setText(getString(R.string.hundred_flag));
            settlementCycleEdit.setText("1");
            settlementCycleEdit.setEnabled(false);
        } else if (selectType == Constants.H5_TYPE) {
            serviceChargeLayout.setVisibility(View.VISIBLE);
            settlementCycleLayout.setVisibility(View.VISIBLE);
            serviceChargeFlag.setText(getString(R.string.thousand_flag));
            settlementCycleEdit.setText("");
            settlementCycleEdit.setEnabled(true);
        } else if (selectType == Constants.CHAIN_TYPE) {
            serviceChargeLayout.setVisibility(View.GONE);
            settlementCycleLayout.setVisibility(View.GONE);
        }
        imgId = "";
        openingPermitImg.setImageResource(R.drawable.upload_img);
        openingPermitEdit.setText("");

        privateUserNameEdit.setText("");
        privateIdNumberEdit.setText("");
        privateCardNumberEdit.setText("");
        privateBankNameEdit.setText("");

        publicUserNameEdit.setText("");
        publicIdNumberEdit.setText("");
        publicCardNumberEdit.setText("");
        publicBankNameEdit.setText("");
        clearImg();
    }

    void clearImg() {
        publicPhone_1.setImageResource(R.drawable.upload_img);
        publicPhone_2.setImageResource(R.drawable.upload_img);
        publicPhone_3.setImageResource(R.drawable.upload_img);
        privatePhone_1.setImageResource(R.drawable.upload_img);
        privatePhone_2.setImageResource(R.drawable.upload_img);
        privatePhone_3.setImageResource(R.drawable.upload_img);
        privateImgId_1 = "";
        privateImgId_2 = "";
        privateImgId_3 = "";
        publicImgId_1 = "";
        publicImgId_2 = "";
        publicImgId_3 = "";
        openingPermitImgUrl = "";
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.opening_permit_img:
                IMG_TYPE = OPEN_ING;
                showPhone();
                break;
            case R.id.private_bank_card_phone:
                IMG_TYPE = PRIVATE_1;
                showPhone();
                break;
            case R.id.private_bank_id_phone_1:
                IMG_TYPE = PRIVATE_2;
                showPhone();
                break;
            case R.id.private_bank_id_phone_2:
                IMG_TYPE = PRIVATE_3;
                showPhone();
                break;
            case R.id.public_bank_card_phone:
                IMG_TYPE = PUBLIC_1;
                showPhone();
                break;
            case R.id.public_bank_id_phone_1:
                IMG_TYPE = PUBLIC_2;
                showPhone();
                break;
            case R.id.public_bank_id_phone_2:
                IMG_TYPE = PUBLIC_3;
                showPhone();
                break;
            case R.id.settlement_info_back:
                ((HomePageActivity) getActivity()).backClick();
                break;
            case R.id.settlement_info_go_next:
                if (!mNetUtil.isNetConnected()) {
                    Toast.makeText(mContext, getString(R.string.cannot_connect_network), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (check()) subSettlementInfo();
                else Toast.makeText(mContext, getString(R.string.submit_prompt), Toast.LENGTH_SHORT).show();
                //((HomePageActivity)getActivity()).goNextOfSettlementInfo();
                break;
        }
    }

    void showPhone() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mPermissionsChecker.lacksPermissions(PERMISSIONS)) {
                startPermissionsActivity();
            } else {
                showDialog();
            }
        } else {
            showDialog();
        }
    }


    private Boolean check() {
        if (selectType != Constants.CHAIN_TYPE) {
            if (StringUtils.isEmpty(serviceChargeEdit.getText().toString())) {
                Toast.makeText(mContext, "请填写手续费", Toast.LENGTH_LONG).show();
                return false;
            }
            if (StringUtils.isEmpty(settlementCycleEdit.getText().toString())) {
                Toast.makeText(mContext, "请填写结算周期", Toast.LENGTH_LONG).show();
                return false;
            }
        }


        if (StringUtils.isEmpty(privateUserNameEdit.getText().toString())) {
            Toast.makeText(mContext, "请填写对私姓名", Toast.LENGTH_LONG).show();
            return false;
        }
        if (StringUtils.isEmpty(privateIdNumberEdit.getText().toString())) {
            Toast.makeText(mContext, "请填写对私身份证号", Toast.LENGTH_LONG).show();
            return false;
        }
        if (StringUtils.isEmpty(privateCardNumberEdit.getText().toString())) {
            Toast.makeText(mContext, "请填写对私银行卡号", Toast.LENGTH_LONG).show();
            return false;
        }
        if (StringUtils.isEmpty(privateBankNameEdit.getText().toString())) {
            Toast.makeText(mContext, "请填写对私开户银行", Toast.LENGTH_LONG).show();
            return false;
        }

        if (StringUtils.isEmpty(privateImgId_1) || StringUtils.isEmpty(privateImgId_2) || StringUtils.isEmpty(privateImgId_3)) {
            if (StringUtils.isEmpty(privateImgId_1))
                Toast.makeText(mContext, "请填写对私银行卡正面", Toast.LENGTH_LONG).show();
            if (StringUtils.isEmpty(privateImgId_2))
                Toast.makeText(mContext, "请填写对私身份证正面", Toast.LENGTH_LONG).show();
            if (StringUtils.isEmpty(privateImgId_3))
                Toast.makeText(mContext, "请填写对私身份证反面", Toast.LENGTH_LONG).show();
            return false;
        }

        if (StringUtils.isEmpty(publicUserNameEdit.getText().toString())) {
            Toast.makeText(mContext, "请填写对公姓名", Toast.LENGTH_LONG).show();
            return false;
        }
        if (StringUtils.isEmpty(publicIdNumberEdit.getText().toString())) {
            Toast.makeText(mContext, "请填写对公身份证号", Toast.LENGTH_LONG).show();
            return false;
        }

        if (StringUtils.isEmpty(publicCardNumberEdit.getText().toString())) {
            Toast.makeText(mContext, "请填写对公银行卡号", Toast.LENGTH_LONG).show();
            return false;
        }
        if (StringUtils.isEmpty(publicBankNameEdit.getText().toString())) {
            Toast.makeText(mContext, "请填写对公开户银行", Toast.LENGTH_LONG).show();
            return false;
        }

        if (StringUtils.isEmpty(publicImgId_1) || StringUtils.isEmpty(publicImgId_2) || StringUtils.isEmpty(publicImgId_3)) {
            if (StringUtils.isEmpty(publicImgId_1))
                Toast.makeText(mContext, "请填写对公银行卡正面", Toast.LENGTH_LONG).show();
            if (StringUtils.isEmpty(publicImgId_2))
                Toast.makeText(mContext, "请填写对公身份证正面", Toast.LENGTH_LONG).show();
            if (StringUtils.isEmpty(publicImgId_3))
                Toast.makeText(mContext, "请填写对公身份证反面", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private void subSettlementInfo() {
        dialog.show(getString(R.string.load_text));
        settlementInfoReq = new SettlementInfoReq();
        settlementInfoReq.setAuditId(auditId);
        settlementInfoReq.setMarketerId(mUser.getUserId());
        settlementInfoReq.setOperation(operation);
        String serviceCharge = serviceChargeEdit.getText().toString();
        if (selectType == Constants.XCX_TYPE || selectType == Constants.CHAIN_CHILD_TYPE) {
            settlementInfoReq.setSettlementRate(new BigDecimal(serviceCharge).divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP));
            settlementInfoReq.setSettlementCycle(new Short(settlementCycleEdit.getText().toString()));
        } else if (selectType == Constants.H5_TYPE) {
            settlementInfoReq.setSettlementRate(new BigDecimal(serviceCharge).divide(new BigDecimal(1000)).setScale(3, BigDecimal.ROUND_HALF_UP));
            settlementInfoReq.setSettlementCycle(new Short(settlementCycleEdit.getText().toString()));
        } else {
            settlementInfoReq.setSettlementRate(new BigDecimal(0));
            settlementInfoReq.setSettlementCycle((short)0);
        }


        settlementInfoReq.setPrivateTransferName(privateUserNameEdit.getText().toString());
        settlementInfoReq.setPrivateTransferIdentifyCard(privateIdNumberEdit.getText().toString());
        settlementInfoReq.setPrivateTransferBankCard(privateCardNumberEdit.getText().toString());
        settlementInfoReq.setPrivateTransferBankAccount(privateBankNameEdit.getText().toString());

        settlementInfoReq.setPublicTransferName(publicUserNameEdit.getText().toString());
        settlementInfoReq.setPublicTransferIdentifyCard(publicIdNumberEdit.getText().toString());
        settlementInfoReq.setPublicTransferBankCard(publicCardNumberEdit.getText().toString());
        settlementInfoReq.setPublicTransferBankAccount(publicBankNameEdit.getText().toString());
        if (!StringUtils.isEmpty(imgId))
            settlementInfoReq.setPublicOpenVoucherId(imgId);
        if (!StringUtils.isEmpty(openingPermitEdit.getText().toString()))
            settlementInfoReq.setPublicOpenVoucherCode(openingPermitEdit.getText().toString());

        settlementInfoReq.setPublicOpenVoucherUrl(openingPermitImgUrl);
        settlementInfoReq.setPrivateTransferBankCardPhoto(privateImgId_1);
        settlementInfoReq.setPrivateTransferIdCardFront(privateImgId_2);
        settlementInfoReq.setPrivateTransferIdCardBack(privateImgId_3);
        settlementInfoReq.setPublicTransferBankCardPhoto(publicImgId_1);
        settlementInfoReq.setPublicTransferIdCardFront(publicImgId_2);
        settlementInfoReq.setPublicTransferIdCardBack(publicImgId_3);

        Log.e(TAG, "subSettlementInfo json = " + new Gson().toJson(settlementInfoReq));
        OkHttpUtils.postString().url(RequestAPI.SETTLEMENT_INFO_URL)
                .content(new Gson().toJson(settlementInfoReq))
                .mediaType(MediaType.parse("application/json; charset=utf-8"))
                .build().execute(new SettlementInfoCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.e(TAG, e + "");
                dialog.dismiss();
                if (e instanceof UnknownHostException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (e instanceof SocketException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (e instanceof SocketTimeoutException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (e instanceof IOException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }

                Toast.makeText(mContext, getString(R.string.service_error), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(ResponseBase<String> response, int id) {
                dialog.dismiss();
                switch (Integer.valueOf(response.getResultCode())) {
                    case Constants.REQ_SUCCESS:
                        expandsLog.setSettlementInfoResponseDTO(settlementInfoReq);
                        ((HomePageActivity) getActivity()).goNextOfSettlementInfo();
                        break;
                    default:
                        Toast.makeText(mContext, response.getMsg(), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }


    abstract class SettlementInfoCallback extends Callback<ResponseBase<String>> {
        @Override
        public ResponseBase<String> parseNetworkResponse(Response response, int id) throws Exception {
            String string = response.body().string();
            Log.e(TAG, string);
            Type jsonType = new TypeToken<ResponseBase<String>>() {
            }.getType();
            ResponseBase<String> res = mGson.fromJson(string, jsonType);
            return res;
        }
    }

    /** 上传图片 */
    private void uploadImgService() {
        dialog.show(getString(R.string.load_text));
        Log.e(TAG, "image path = " + cutImgUri.getPath());
        String img = "data:image/png;base64,";

//        img +=  Base64Util.encode(BitmapUtils.FileTobyte(new File(cutImgUri.getPath())));
        img += Base64Util.encode(BitmapUtils.FileTobyte(new File(cutImgUri.getPath())));
        UploadImgReq uploadImgReq = new UploadImgReq();
        uploadImgReq.setFileBase64Data(img);
        uploadImgReq.setUserId(mUser.getUserId());
        OkHttpUtils.postString().url(RequestAPI.UPLOAD_IMG_URL)
                .content(new Gson().toJson(uploadImgReq))
                .mediaType(MediaType.parse("application/json; charset=utf-8"))
                .build().execute(new UploadImgCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.e(TAG, e + "");
                dialog.dismiss();
                if (e instanceof UnknownHostException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (e instanceof SocketException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (e instanceof SocketTimeoutException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (e instanceof IOException) {
                    Toast.makeText(mContext, getString(R.string.net_error), Toast.LENGTH_SHORT).show();
                    return;
                }

                Toast.makeText(mContext, getString(R.string.service_error), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(ResponseBase<UploadImgRes> response, int id) {
                dialog.dismiss();
                switch (Integer.valueOf(response.getResultCode())) {
                    case Constants.REQ_SUCCESS:
                        Toast.makeText(mContext, getString(R.string.upload_success), Toast.LENGTH_SHORT).show();
                        if (IMG_TYPE == OPEN_ING) {
                            imgId = response.getResult().getFileId();
                            openingPermitImgUrl = response.getResult().getFileUrl();
                            Picasso.with(mContext).load(response.getResult().getFileUrl()).
                                    placeholder(R.drawable.loading).resize(200, 200).into(openingPermitImg);
                        }
                        if (IMG_TYPE == PRIVATE_1) {
                            privateImgId_1 = response.getResult().getFileUrl();
                            Picasso.with(mContext).load(response.getResult().getFileUrl()).
                                    placeholder(R.drawable.loading).resize(200, 200).into(privatePhone_1);
                        }
                        if (IMG_TYPE == PRIVATE_2) {
                            privateImgId_2 = response.getResult().getFileUrl();
                            Picasso.with(mContext).load(response.getResult().getFileUrl()).
                                    placeholder(R.drawable.loading).resize(200, 200).into(privatePhone_2);
                        }
                        if (IMG_TYPE == PRIVATE_3) {
                            privateImgId_3 = response.getResult().getFileUrl();
                            Picasso.with(mContext).load(response.getResult().getFileUrl()).
                                    placeholder(R.drawable.loading).resize(200, 200).into(privatePhone_3);
                        }
                        if (IMG_TYPE == PUBLIC_1) {
                            publicImgId_1 = response.getResult().getFileUrl();
                            Picasso.with(mContext).load(response.getResult().getFileUrl()).
                                    placeholder(R.drawable.loading).resize(200, 200).into(publicPhone_1);
                        }
                        if (IMG_TYPE == PUBLIC_2) {
                            publicImgId_2 = response.getResult().getFileUrl();
                            Picasso.with(mContext).load(response.getResult().getFileUrl()).
                                    placeholder(R.drawable.loading).resize(200, 200).into(publicPhone_2);
                        }
                        if (IMG_TYPE == PUBLIC_3) {
                            publicImgId_3 = response.getResult().getFileUrl();
                            Picasso.with(mContext).load(response.getResult().getFileUrl()).
                                    placeholder(R.drawable.loading).resize(200, 200).into(publicPhone_3);
                        }
                        break;
                    default:
                        Toast.makeText(mContext, response.getMsg(), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }

    abstract class UploadImgCallback extends Callback<ResponseBase<UploadImgRes>> {
        @Override
        public ResponseBase<UploadImgRes> parseNetworkResponse(Response response, int id) throws Exception {
            String string = response.body().string();
            Log.e(TAG, string);
            Type jsonType = new TypeToken<ResponseBase<UploadImgRes>>() {
            }.getType();
            ResponseBase<UploadImgRes> res = mGson.fromJson(string, jsonType);
            return res;
        }
    }


    private void startPermissionsActivity() {
        PermissionsActivity.startActivityForResult(getActivity(), REQUEST_PERMISSION,
                PERMISSIONS);
    }

    private void showDialog() {
        if (imagePickDialog == null) imagePickDialog = new ImagePickDialog(mContext);
        imagePickDialog.setDialogSelectedListener(this);
        imagePickDialog.show();
    }

    @Override
    public void onPhone() {
        Log.e(TAG, "相册........");
        if (mImagePicker == null) mImagePicker = new ImagePicker(getActivity(), this);
        mImagePicker.pickOnphone();
    }

    @Override
    public void onCapture() {
        Log.e(TAG, "拍照........");
        if (mImagePicker == null) mImagePicker = new ImagePicker(getActivity(), this);
        cameraUri = mImagePicker.openCamera();
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case ImagePicker.REQUEST_CAPTURE:
                    cutImgUri = mImagePicker.cropPhoto(cameraUri);
                    break;
                case ImagePicker.REQUEST_PICK_IMAGE:
                    cameraUri = data.getData(); //获取系统返回的照片的Uri
                    cutImgUri = mImagePicker.cropPhoto(cameraUri);
                    break;
                case ImagePicker.REQUEST_PICTURE_CUT:
                    if (!mNetUtil.isNetConnected()) {
                        Toast.makeText(mContext, getString(R.string.cannot_connect_network), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    uploadImgService();
                    break;
            }
        }
    }

}
