package com.nihome.entershopinfo.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nihome.entershopinfo.Constants;
import com.nihome.entershopinfo.R;
import com.nihome.entershopinfo.application.SysApplication;
import com.nihome.entershopinfo.dialog.AlertDialog;
import com.nihome.entershopinfo.entity.User;
import com.nihome.entershopinfo.entity.response.ExpandsLogRes;
import com.nihome.entershopinfo.fragments.BasicInfoFragment;
import com.nihome.entershopinfo.fragments.ChildShopLogFragment;
import com.nihome.entershopinfo.fragments.ExpandsLogFragment;
import com.nihome.entershopinfo.fragments.LogDetailFragment;
import com.nihome.entershopinfo.fragments.MoneyVoucherFragment;
import com.nihome.entershopinfo.fragments.QualificationsFragment;
import com.nihome.entershopinfo.fragments.SelectModelFragment;
import com.nihome.entershopinfo.fragments.SendInfoFragment;
import com.nihome.entershopinfo.fragments.SettlementInfoFragment;
import com.nihome.entershopinfo.fragments.ShopInfoFragment;
import com.nihome.entershopinfo.fragments.XcxFinishFragment;
import com.nihome.entershopinfo.fragments.XcxRelevantFragment;
import com.nihome.entershopinfo.utils.FileUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Carson on 2017/7/18.
 */
public class HomePageActivity extends BaseActivity implements View.OnClickListener{
    private static final int SELECT_FRAGMENT                    = 1;
    private static final int BASIC_FRAGMENT                     = 2;
    private static final int SHOP_INFO_FRAGMENT                 = 3;
    private static final int SEND_INFO_FRAGMENT                 = 4;
    private static final int SETTLEMENT_FRAGMENT                = 5;
    private static final int RELEVANT_FRAGMENT                  = 6;
    private static final int QUALIFICATION_FRAGMENT             = 7;
    private static final int FINISH_FRAGMENT                    = 8;
    private static final int EXPANDS_LOG_FRAGMENT               = 9;
    private static final int LOG_DETAIL_FRAGMENT                = 10;
    private static final int MONEY_VOUCHER_FRAGMENT             = 11;
    private static final int CHILD_SHOP_LOG_FRAGMENT            = 12;
    private static final int TAB_FRAGMENT                       = 1000;
    private static final int LOG_FRAGMENT                       = 1001;


    private int tabFragment;
    private int logFragment;
    private int frgmentType;

    public static final int TS           = 100;
    public static final int WM           = 101;
    public static final int TS_WM        = 102;
    private int businessWay = TS;


    private boolean isExit = false;
    private Context mContext;
    private User mUser;

    private TextView bdName;
    private TextView expandsShopBtn;
    private View expandsShopLine;
    private TextView expandsLogBtn;
    private View expandsLogLine;
    private TextView exitBtn;
    private View exitLine;

    private SelectModelFragment selectModelFragment;
    private BasicInfoFragment basicInfoFragment;
    private ShopInfoFragment shopInfoFragment;
    private SendInfoFragment sendInfoFragment;
    private SettlementInfoFragment settlementInfoFragment;
    private XcxRelevantFragment xcxRelevantFragment;
    private QualificationsFragment qualificationsFragment;
    private XcxFinishFragment xcxFinishFragment;
    private ExpandsLogFragment expandsLogFragment;
    private LogDetailFragment logDetailFragment;
    private MoneyVoucherFragment moneyVoucherFragment;
    private ChildShopLogFragment childShopLogFragment;


    private Fragment backFragment;

    private LinearLayout tabLayout;
    private RelativeLayout tabRelative_1;
    private RelativeLayout tabRelative_2;
    private RelativeLayout tabRelative_3;
    private RelativeLayout tabRelative_4;
    private RelativeLayout tabRelative_5;
    private RelativeLayout tabRelative_6;
    private RelativeLayout tabRelative_7;
    private RelativeLayout tabRelative_8;

    private RelativeLayout tabRelativeOther;

    private TextView title1View;
    private TextView title2View;
    private TextView title6View;

    private List<RelativeLayout> tabRelativeList = new ArrayList<>();
    private int imgWidth;

    private int selectType;
    private ExpandsLogRes expandsLog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        SysApplication.getInstance().addActivity(this);
        mUser = SysApplication.getInstance().getmUser();
        mContext = this;
        imgWidth = (int) getResources().getDimension(R.dimen.size_12dp);
        frgmentType = TAB_FRAGMENT;
        initViews();
        initFragment();
    }

    private void initViews(){
        bdName = (TextView) findViewById(R.id.bd_name);
        expandsShopBtn = (TextView) findViewById(R.id.expands_shop_btn);
        expandsShopLine = findViewById(R.id.expands_shop_line);
        expandsLogBtn = (TextView) findViewById(R.id.expands_log_btn);
        expandsLogLine = findViewById(R.id.expands_log_line);
        exitBtn = (TextView) findViewById(R.id.exit_btn);
        exitLine = findViewById(R.id.exit_line);

        tabLayout = (LinearLayout) findViewById(R.id.tab_layout);
        tabRelative_1 = (RelativeLayout) findViewById(R.id.tab_relative_1);
        title1View = (TextView) findViewById(R.id.title_1_view);
        tabRelative_2 = (RelativeLayout) findViewById(R.id.tab_relative_2);
        title2View = (TextView) findViewById(R.id.title_2_view);
        tabRelative_3 = (RelativeLayout) findViewById(R.id.tab_relative_3);
        tabRelative_4 = (RelativeLayout) findViewById(R.id.tab_relative_4);
        tabRelative_5 = (RelativeLayout) findViewById(R.id.tab_relative_5);
        tabRelative_6 = (RelativeLayout) findViewById(R.id.tab_relative_6);
        title6View = (TextView) findViewById(R.id.title_6_view);
        tabRelative_7 = (RelativeLayout) findViewById(R.id.tab_relative_7);
        tabRelative_8 = (RelativeLayout) findViewById(R.id.tab_relative_8);

        tabRelativeOther = (RelativeLayout) findViewById(R.id.tab_relative_other);

        expandsShopBtn.setOnClickListener(this);
        expandsLogBtn.setOnClickListener(this);
        exitBtn.setOnClickListener(this);
        bdName.setText(mUser.getUserName());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.expands_shop_btn:
                frgmentType = TAB_FRAGMENT;
                expandsShopBtn.setTextColor(ContextCompat.getColor(mContext, R.color.red));
                expandsShopLine.setBackgroundResource(R.color.red);
                expandsLogBtn.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
                expandsLogLine.setBackgroundResource(R.color.line_bg);
                exitBtn.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
                exitLine.setBackgroundResource(R.color.line_bg);
                clickExpandsShop();
                break;
            case R.id.expands_log_btn:
                frgmentType = LOG_FRAGMENT;
                expandsLogBtn.setTextColor(ContextCompat.getColor(mContext, R.color.red));
                expandsLogLine.setBackgroundResource(R.color.red);
                expandsShopBtn.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
                expandsShopLine.setBackgroundResource(R.color.line_bg);
                exitBtn.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
                exitLine.setBackgroundResource(R.color.line_bg);
                expandsLogFragment();
                break;
            case R.id.exit_btn:
                exitBtn.setTextColor(ContextCompat.getColor(mContext, R.color.red));
                exitLine.setBackgroundResource(R.color.red);
                expandsShopBtn.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
                expandsShopLine.setBackgroundResource(R.color.line_bg);
                expandsLogBtn.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
                expandsLogLine.setBackgroundResource(R.color.line_bg);
                exitDialog();
                break;
        }
    }

    /**
     * 初始化Fragment
     */
    private void initFragment() {
        tabFragment = SELECT_FRAGMENT;
        tabLayout.setVisibility(View.GONE);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        if (selectModelFragment == null) {
            selectModelFragment = new SelectModelFragment();
            transaction.add(R.id.content_frame, selectModelFragment);
        } else {
            transaction.show(selectModelFragment);
        }
        transaction.commit();
    }


    /**
     * 点击小程序加载基本信息Fragment
     */
    public void xcxClick(Fragment backFragment){
        this.backFragment = backFragment;


        this.expandsLog = new ExpandsLogRes();
        tabFragment = BASIC_FRAGMENT;
        selectType = Constants.XCX_TYPE;
        addFragment(selectType);
        initTab(0, R.id.tab_relative_2);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.hide(selectModelFragment);
        if (basicInfoFragment == null) {
            basicInfoFragment = new BasicInfoFragment();

            Bundle bundle = new Bundle();
            bundle.putInt(Constants.SELECT_TYPE, selectType);
            bundle.putParcelable(Constants.DATA, expandsLog);
            basicInfoFragment.setArguments(bundle);

            transaction.add(R.id.content_frame, basicInfoFragment);
            transaction.addToBackStack(null);
        } else {
            basicInfoFragment.getArguments().putInt(Constants.SELECT_TYPE, selectType);
            basicInfoFragment.getArguments().putParcelable(Constants.DATA, expandsLog);
            transaction.show(basicInfoFragment);
        }
        transaction.commit();
    }


    /**
     * 点击加载连锁店总帐户基本信息Fragment
     */
    public void xcxClick_chain(ExpandsLogRes log, Fragment backFragment){
        this.backFragment = backFragment;


        this.expandsLog = log;
        tabFragment = BASIC_FRAGMENT;
        selectType = Constants.CHAIN_TYPE;
        addFragment(selectType);
        initTab(0, R.id.tab_relative_2);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        if (log.getBasicInfoResponseDTO() == null)
            transaction.hide(selectModelFragment);
        if (log.getBasicInfoResponseDTO() != null) {
            SysApplication.getInstance().setAuditId(log.getBasicInfoResponseDTO().getAuditId());
            frgmentType = TAB_FRAGMENT;
            expandsShopBtn.setTextColor(ContextCompat.getColor(mContext, R.color.red));
            expandsShopLine.setBackgroundResource(R.color.red);
            expandsLogBtn.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
            expandsLogLine.setBackgroundResource(R.color.line_bg);
            exitBtn.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
            exitLine.setBackgroundResource(R.color.line_bg);

            transaction.hide(expandsLogFragment);
        }
        if (basicInfoFragment == null) {
            basicInfoFragment = new BasicInfoFragment();

            Bundle bundle = new Bundle();
            bundle.putInt(Constants.SELECT_TYPE, selectType);
            bundle.putParcelable(Constants.DATA, expandsLog);
            basicInfoFragment.setArguments(bundle);

            transaction.add(R.id.content_frame, basicInfoFragment);
            transaction.addToBackStack(null);
        } else {
            basicInfoFragment.getArguments().putInt(Constants.SELECT_TYPE, selectType);
            basicInfoFragment.getArguments().putParcelable(Constants.DATA, expandsLog);

            transaction.show(basicInfoFragment);
        }
        transaction.commit();
    }


    /**
     * 点击加载连锁店总帐户基本信息Fragment
     */
    public void xcxClick_chainChild(String parentAuditId, ExpandsLogRes log, Fragment backFragment){
        this.backFragment = backFragment;


        expandsShopBtn.setTextColor(ContextCompat.getColor(mContext, R.color.red));
        expandsShopLine.setBackgroundResource(R.color.red);
        expandsLogBtn.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
        expandsLogLine.setBackgroundResource(R.color.line_bg);
        exitBtn.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
        exitLine.setBackgroundResource(R.color.line_bg);


        this.expandsLog = log;
        if (expandsLog.getOtherInfoResponseDTO() != null) {
            SysApplication.getInstance().setAuditId(expandsLog.getOtherInfoResponseDTO().getAuditId());
        }
        frgmentType = TAB_FRAGMENT;
        tabFragment = BASIC_FRAGMENT;
        selectType = Constants.CHAIN_CHILD_TYPE;
        addFragment(selectType);
        initTab(0, R.id.tab_relative_2);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        if (logFragment == EXPANDS_LOG_FRAGMENT){
            transaction.hide(expandsLogFragment);
        }
        if (logFragment == CHILD_SHOP_LOG_FRAGMENT) {
            transaction.hide(childShopLogFragment);
        }
        if (basicInfoFragment == null) {
            basicInfoFragment = new BasicInfoFragment();

            Bundle bundle = new Bundle();
            bundle.putInt(Constants.SELECT_TYPE, selectType);
            bundle.putParcelable(Constants.DATA, expandsLog);
            bundle.putString("parentAuditId", parentAuditId);
            basicInfoFragment.setArguments(bundle);

            transaction.add(R.id.content_frame, basicInfoFragment);
            transaction.addToBackStack(null);
        } else {
            basicInfoFragment.getArguments().putInt(Constants.SELECT_TYPE, selectType);
            basicInfoFragment.getArguments().putParcelable(Constants.DATA, expandsLog);
            basicInfoFragment.getArguments().putString("parentAuditId", parentAuditId);

            transaction.show(basicInfoFragment);
        }
        transaction.commit();
    }



    /**
     * 点击H5加载基本信息Fragment
     */
    public void h5Click(){
        this.expandsLog = new ExpandsLogRes();
        tabFragment = BASIC_FRAGMENT;
        selectType = Constants.H5_TYPE;
        addFragment(selectType);
        initTab(0, R.id.tab_relative_2);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.hide(selectModelFragment);
        if (basicInfoFragment == null) {
            basicInfoFragment = new BasicInfoFragment();

            Bundle bundle = new Bundle();
            bundle.putInt(Constants.SELECT_TYPE, selectType);
            bundle.putParcelable(Constants.DATA, expandsLog);
            basicInfoFragment.setArguments(bundle);

            transaction.add(R.id.content_frame, basicInfoFragment);
            transaction.addToBackStack(null);
        } else {
            basicInfoFragment.getArguments().putInt(Constants.SELECT_TYPE, selectType);
            basicInfoFragment.getArguments().putParcelable(Constants.DATA, expandsLog);

            transaction.show(basicInfoFragment);
        }
        transaction.commit();
    }

    /**
     * 基本信息Fragment点击下一步
     */
    public void goNextOfBasicInfo(){
        tabLayout.setVisibility(View.VISIBLE);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.hide(basicInfoFragment);
        if (selectType == Constants.XCX_TYPE || selectType == Constants.H5_TYPE || selectType == Constants.CHAIN_CHILD_TYPE) {
            tabFragment = SHOP_INFO_FRAGMENT;
            initTab(1, R.id.tab_relative_3);
            if (shopInfoFragment == null) {
                shopInfoFragment = new ShopInfoFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.SELECT_TYPE, selectType);
                bundle.putParcelable(Constants.DATA, expandsLog);
                shopInfoFragment.setArguments(bundle);
                transaction.add(R.id.content_frame, shopInfoFragment);
                transaction.addToBackStack(null);
            } else {
                shopInfoFragment.getArguments().putInt(Constants.SELECT_TYPE, selectType);
                shopInfoFragment.getArguments().putParcelable(Constants.DATA, expandsLog);
                transaction.show(shopInfoFragment);
            }
        }
        if (selectType == Constants.CHAIN_TYPE) {
            tabFragment = SETTLEMENT_FRAGMENT;
            tabLayout.setVisibility(View.VISIBLE);
            initTab(1, R.id.tab_relative_5);
            if (settlementInfoFragment == null) {
                settlementInfoFragment = new SettlementInfoFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.SELECT_TYPE, selectType);
                bundle.putParcelable(Constants.DATA, expandsLog);
                settlementInfoFragment.setArguments(bundle);
                transaction.add(R.id.content_frame, settlementInfoFragment);
                transaction.addToBackStack(null);
            } else {
                settlementInfoFragment.getArguments().putInt(Constants.SELECT_TYPE, selectType);
                settlementInfoFragment.getArguments().putParcelable(Constants.DATA, expandsLog);
                transaction.show(settlementInfoFragment);
            }
        }


        transaction.commit();
    }

    /**
     * 店铺信息Fragment点击下一步
     */
    public void goNextOfShopInfo(int businessWay){
        this.businessWay = businessWay;
        tabLayout.setVisibility(View.VISIBLE);
        if (businessWay == TS) initTab(3, R.id.tab_relative_5);
        else initTab(2, R.id.tab_relative_4);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.hide(shopInfoFragment);
        if (businessWay == TS) {
            tabFragment = SETTLEMENT_FRAGMENT;
            if (settlementInfoFragment == null) {
                settlementInfoFragment = new SettlementInfoFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.SELECT_TYPE, selectType);
                bundle.putParcelable(Constants.DATA, expandsLog);
                settlementInfoFragment.setArguments(bundle);
                transaction.add(R.id.content_frame, settlementInfoFragment);
                transaction.addToBackStack(null);
            } else {
                settlementInfoFragment.getArguments().putInt(Constants.SELECT_TYPE, selectType);
                settlementInfoFragment.getArguments().putParcelable(Constants.DATA, expandsLog);
                transaction.show(settlementInfoFragment);
            }
        }else {
            tabFragment = SEND_INFO_FRAGMENT;
            if (sendInfoFragment == null) {
                sendInfoFragment = new SendInfoFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.SELECT_TYPE, selectType);
                bundle.putParcelable(Constants.DATA, expandsLog);
                sendInfoFragment.setArguments(bundle);
                transaction.add(R.id.content_frame, sendInfoFragment);
                transaction.addToBackStack(null);
            } else {
                sendInfoFragment.getArguments().putInt(Constants.SELECT_TYPE, selectType);
                sendInfoFragment.getArguments().putParcelable(Constants.DATA, expandsLog);
                transaction.show(sendInfoFragment);
            }
        }
        transaction.commit();
    }

    /**
     * 外卖信息Fragment点击下一步
     */
    public void goNextOfSendInfo(){
        tabFragment = SETTLEMENT_FRAGMENT;
        tabLayout.setVisibility(View.VISIBLE);
        initTab(3, R.id.tab_relative_5);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.hide(sendInfoFragment);
        if (settlementInfoFragment == null) {
            settlementInfoFragment = new SettlementInfoFragment();
            Bundle bundle = new Bundle();
            bundle.putInt(Constants.SELECT_TYPE, selectType);
            bundle.putParcelable(Constants.DATA, expandsLog);
            settlementInfoFragment.setArguments(bundle);
            transaction.add(R.id.content_frame, settlementInfoFragment);
            transaction.addToBackStack(null);
        } else {
            settlementInfoFragment.getArguments().putInt(Constants.SELECT_TYPE, selectType);
            settlementInfoFragment.getArguments().putParcelable(Constants.DATA, expandsLog);
            transaction.show(settlementInfoFragment);
        }
        transaction.commit();
    }

    /**
     * 结算信息Fragment点击下一步
     */
    public void goNextOfSettlementInfo(){
        tabFragment = RELEVANT_FRAGMENT;
        tabLayout.setVisibility(View.VISIBLE);
        if (selectType == Constants.XCX_TYPE || selectType == Constants.H5_TYPE || selectType == Constants.CHAIN_CHILD_TYPE) {
            initTab(4, R.id.tab_relative_6);
        }
        if (selectType == Constants.CHAIN_TYPE) {
            initTab(2, R.id.tab_relative_6);
        }
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.hide(settlementInfoFragment);
        tabFragment = RELEVANT_FRAGMENT;
        if (xcxRelevantFragment == null) {
            xcxRelevantFragment = new XcxRelevantFragment();
            Bundle bundle = new Bundle();
            bundle.putInt(Constants.SELECT_TYPE, selectType);
            bundle.putParcelable(Constants.DATA, expandsLog);
            xcxRelevantFragment.setArguments(bundle);
            transaction.add(R.id.content_frame, xcxRelevantFragment);
            transaction.addToBackStack(null);
        } else {
            xcxRelevantFragment.getArguments().putInt(Constants.SELECT_TYPE, selectType);
            xcxRelevantFragment.getArguments().putParcelable(Constants.DATA, expandsLog);
            transaction.show(xcxRelevantFragment);
        }


        transaction.commit();
    }

    /**
     * 小程序Fragment点击下一步
     */
    public void goNextOfRelevant(){
        tabLayout.setVisibility(View.VISIBLE);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.hide(xcxRelevantFragment);
        if (selectType == Constants.XCX_TYPE || selectType == Constants.CHAIN_CHILD_TYPE) {
            initTab(5, R.id.tab_relative_7);
            tabFragment = QUALIFICATION_FRAGMENT;
            if (qualificationsFragment == null) {
                qualificationsFragment = new QualificationsFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.SELECT_TYPE, selectType);
                bundle.putParcelable(Constants.DATA, expandsLog);
                qualificationsFragment.setArguments(bundle);
                transaction.add(R.id.content_frame, qualificationsFragment);
                transaction.addToBackStack(null);
            } else {
                qualificationsFragment.getArguments().putInt(Constants.SELECT_TYPE, selectType);
                qualificationsFragment.getArguments().putParcelable(Constants.DATA, expandsLog);
                transaction.show(qualificationsFragment);
            }
        } else {
            initTab(5, R.id.tab_relative_8);
            tabFragment = FINISH_FRAGMENT;
            if (xcxFinishFragment == null) {
                xcxFinishFragment = new XcxFinishFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.SELECT_TYPE, selectType);
                bundle.putParcelable(Constants.DATA, expandsLog);
                xcxFinishFragment.setArguments(bundle);
                transaction.add(R.id.content_frame, xcxFinishFragment);
                transaction.addToBackStack(null);
            } else {
                xcxFinishFragment.getArguments().putInt(Constants.SELECT_TYPE, selectType);
                xcxFinishFragment.getArguments().putParcelable(Constants.DATA, expandsLog);
                transaction.show(xcxFinishFragment);
            }
        }


        transaction.commit();
    }

    /**
     * 资质信息Fragment点击下一步
     */
    public void goNextOfQualifications(){
        tabFragment = FINISH_FRAGMENT;
        tabLayout.setVisibility(View.VISIBLE);
        initTab(6, R.id.tab_relative_8);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.hide(qualificationsFragment);
        if (xcxFinishFragment == null) {
            xcxFinishFragment = new XcxFinishFragment();
            Bundle bundle = new Bundle();
            bundle.putInt(Constants.SELECT_TYPE, selectType);
            bundle.putParcelable(Constants.DATA, expandsLog);
            xcxFinishFragment.setArguments(bundle);
            transaction.add(R.id.content_frame, xcxFinishFragment);
            transaction.addToBackStack(null);
        } else {
            xcxFinishFragment.getArguments().putInt(Constants.SELECT_TYPE, selectType);
            xcxFinishFragment.getArguments().putParcelable(Constants.DATA, expandsLog);
            transaction.show(xcxFinishFragment);
        }
        transaction.commit();
    }

    /**
     * 完成Fragment点击返回
     */
    public void homeFinish(){
        tabLayout.setVisibility(View.GONE);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        if (frgmentType == TAB_FRAGMENT) {
            tabFragment = SELECT_FRAGMENT;
            transaction.hide(xcxFinishFragment);
            if (selectModelFragment == null) {
                selectModelFragment = new SelectModelFragment();
                transaction.add(R.id.content_frame, selectModelFragment);
            } else {
                transaction.show(selectModelFragment);
            }
        }
        if (frgmentType == LOG_FRAGMENT) {
            logFragment = EXPANDS_LOG_FRAGMENT;
            transaction.hide(moneyVoucherFragment);
            if (expandsLogFragment == null) {
                expandsLogFragment = new ExpandsLogFragment();
                transaction.add(R.id.content_frame, expandsLogFragment);
            } else {
                transaction.show(expandsLogFragment);
            }
        }

        transaction.commit();
    }



    /**
     * 跳转拓展记录Fragment
     */
    public void expandsLogFragment(){
        tabLayout.setVisibility(View.GONE);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        if (tabFragment == FINISH_FRAGMENT) transaction.hide(xcxFinishFragment);
        if (tabFragment == QUALIFICATION_FRAGMENT) transaction.hide(qualificationsFragment);
        if (tabFragment == RELEVANT_FRAGMENT) transaction.hide(xcxRelevantFragment);
        if (tabFragment == SETTLEMENT_FRAGMENT) transaction.hide(settlementInfoFragment);
        if (tabFragment == SEND_INFO_FRAGMENT) transaction.hide(sendInfoFragment);
        if (tabFragment == SHOP_INFO_FRAGMENT) transaction.hide(shopInfoFragment);
        if (tabFragment == BASIC_FRAGMENT) transaction.hide(basicInfoFragment);
        if (tabFragment == SELECT_FRAGMENT) transaction.hide(selectModelFragment);
        if (expandsLogFragment == null) {
            logFragment = EXPANDS_LOG_FRAGMENT;
            expandsLogFragment = new ExpandsLogFragment();
            transaction.add(R.id.content_frame, expandsLogFragment);
            transaction.addToBackStack(null);
        } else if (logFragment == EXPANDS_LOG_FRAGMENT){
            transaction.show(expandsLogFragment);
        } else if (logFragment == LOG_DETAIL_FRAGMENT) {
            transaction.show(logDetailFragment);
        } else if (logFragment == MONEY_VOUCHER_FRAGMENT){
            transaction.show(moneyVoucherFragment);
        } else if (logFragment == CHILD_SHOP_LOG_FRAGMENT) {
            transaction.show(childShopLogFragment);
        }
        transaction.commit();
    }

    /**
     * 点击扩展门店
     */
    private void clickExpandsShop(){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        if (logFragment == EXPANDS_LOG_FRAGMENT) {
            transaction.hide(expandsLogFragment);
        }
        if (logFragment == LOG_DETAIL_FRAGMENT) {
            transaction.hide(logDetailFragment);
        }
        if (logFragment == MONEY_VOUCHER_FRAGMENT){
            transaction.hide(moneyVoucherFragment);
        }
        if (logFragment == CHILD_SHOP_LOG_FRAGMENT) {
            transaction.hide(childShopLogFragment);
        }
        switch (tabFragment) {
            case FINISH_FRAGMENT:
                tabLayout.setVisibility(View.VISIBLE);
                transaction.show(xcxFinishFragment);
                break;
            case QUALIFICATION_FRAGMENT:
                tabLayout.setVisibility(View.VISIBLE);
                transaction.show(qualificationsFragment);
                break;
            case RELEVANT_FRAGMENT:
                tabLayout.setVisibility(View.VISIBLE);
                transaction.show(xcxRelevantFragment);
                break;
            case SETTLEMENT_FRAGMENT:
                tabLayout.setVisibility(View.VISIBLE);
                transaction.show(settlementInfoFragment);
                break;
            case SEND_INFO_FRAGMENT:
                tabLayout.setVisibility(View.VISIBLE);
                transaction.show(sendInfoFragment);
                break;
            case SHOP_INFO_FRAGMENT:
                tabLayout.setVisibility(View.VISIBLE);
                transaction.show(shopInfoFragment);
                break;
            case BASIC_FRAGMENT:
                tabLayout.setVisibility(View.VISIBLE);
                transaction.show(basicInfoFragment);
                break;
            case SELECT_FRAGMENT:
                tabLayout.setVisibility(View.GONE);
                transaction.show(selectModelFragment);
                break;
        }
        transaction.commit();
    }


    /**
     * 跳转记录详细Fragment
     */
    public void lookDetailLog(ExpandsLogRes log){
        short status = log.getOtherInfoResponseDTO().getAuditStatus();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        if (logFragment == EXPANDS_LOG_FRAGMENT) transaction.hide(expandsLogFragment);
        if (logFragment == CHILD_SHOP_LOG_FRAGMENT) transaction.hide(childShopLogFragment);
        if (status == 101 || status == 202) { /**  状态为101补充信息 */
            this.expandsLog  = log;
            frgmentType = TAB_FRAGMENT;
            tabFragment = BASIC_FRAGMENT;

            expandsShopBtn.setTextColor(ContextCompat.getColor(mContext, R.color.red));
            expandsShopLine.setBackgroundResource(R.color.red);
            expandsLogBtn.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
            expandsLogLine.setBackgroundResource(R.color.line_bg);
            exitBtn.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
            exitLine.setBackgroundResource(R.color.line_bg);

            SysApplication.getInstance().setAuditId(expandsLog.getOtherInfoResponseDTO().getAuditId());
            if (expandsLog.getBasicInfoResponseDTO().getPartnerType() == 1) {
                selectType = Constants.CHAIN_TYPE;
            } else {
                selectType = expandsLog.getOtherInfoResponseDTO().getStoreSource() == 1 ? Constants.XCX_TYPE : Constants.H5_TYPE;
            }

            addFragment(selectType);
            initTab(0, R.id.tab_relative_2);
            tabLayout.setVisibility(View.VISIBLE);
            if (basicInfoFragment == null) {
                basicInfoFragment = new BasicInfoFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.SELECT_TYPE, selectType);
                bundle.putParcelable(Constants.DATA, expandsLog);
                basicInfoFragment.setArguments(bundle);
                transaction.add(R.id.content_frame, basicInfoFragment);
                transaction.addToBackStack(null);
            } else {
                basicInfoFragment.getArguments().putInt(Constants.SELECT_TYPE, selectType);
                basicInfoFragment.getArguments().putParcelable(Constants.DATA, expandsLog);
                transaction.show(basicInfoFragment);
            }
        }else {         /**  跳转详细信息 */
            logFragment = LOG_DETAIL_FRAGMENT;
            if (logDetailFragment == null) {
                logDetailFragment = new LogDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.DATA, log);
                logDetailFragment.setArguments(bundle);
                transaction.add(R.id.content_frame, logDetailFragment);
                transaction.addToBackStack(null);
            } else {
                logDetailFragment.getArguments().putParcelable(Constants.DATA, log);
                transaction.show(logDetailFragment);
            }
        }
        transaction.commit();
    }


    public void childShopLogFragment(ExpandsLogRes parentLog){
        logFragment = CHILD_SHOP_LOG_FRAGMENT;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.hide(expandsLogFragment);
        if (childShopLogFragment == null) {
            childShopLogFragment = new ChildShopLogFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable("parentLog", parentLog);
            childShopLogFragment.setArguments(bundle);
            transaction.add(R.id.content_frame, childShopLogFragment);
            transaction.addToBackStack(null);
        } else {
            childShopLogFragment.getArguments().putParcelable("parentLog", parentLog);
            transaction.show(childShopLogFragment);
        }
        transaction.commit();
    }


    public void moneyVoucherBtn(ExpandsLogRes log){
        logFragment = MONEY_VOUCHER_FRAGMENT;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.hide(expandsLogFragment);
        if (moneyVoucherFragment == null) {
            moneyVoucherFragment = new MoneyVoucherFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.DATA, log);
            moneyVoucherFragment.setArguments(bundle);
            transaction.add(R.id.content_frame, moneyVoucherFragment);
            transaction.addToBackStack(null);
        } else {
            moneyVoucherFragment.getArguments().putParcelable(Constants.DATA, log);
            transaction.show(moneyVoucherFragment);
        }
        transaction.commit();
    }


    public void moneyVoucherBack(){
        logFragment = EXPANDS_LOG_FRAGMENT;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.hide(moneyVoucherFragment);
        transaction.show(expandsLogFragment);
        transaction.commit();
    }



    public void logDetailBack(){
        logFragment = EXPANDS_LOG_FRAGMENT;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.hide(logDetailFragment);
        transaction.show(expandsLogFragment);
        transaction.commit();
    }

    public void childShopBack(){
        logFragment = EXPANDS_LOG_FRAGMENT;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.hide(childShopLogFragment);
        transaction.show(expandsLogFragment);
        transaction.commit();
    }


    public void backClick(){
        if (frgmentType == TAB_FRAGMENT) {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction transaction = fm.beginTransaction();
            switch (tabFragment) {
                case FINISH_FRAGMENT:
                    if (selectType == Constants.XCX_TYPE || selectType == Constants.CHAIN_CHILD_TYPE) {
                        tabFragment = QUALIFICATION_FRAGMENT;
                        tabLayout.setVisibility(View.VISIBLE);
                        initTab(5, R.id.tab_relative_7);
                        transaction.hide(xcxFinishFragment);
                        transaction.show(qualificationsFragment);
                    }

                    if (selectType == Constants.H5_TYPE) {
                        tabFragment = RELEVANT_FRAGMENT;
                        tabLayout.setVisibility(View.VISIBLE);
                        initTab(4, R.id.tab_relative_6);
                        transaction.hide(xcxFinishFragment);
                        transaction.show(xcxRelevantFragment);
                    }

                    if (selectType == Constants.CHAIN_TYPE) {
                        tabFragment = RELEVANT_FRAGMENT;
                        tabLayout.setVisibility(View.VISIBLE);
                        initTab(4, R.id.tab_relative_6);
                        transaction.hide(xcxFinishFragment);
                        transaction.show(xcxRelevantFragment);
                    }
                    break;
                case QUALIFICATION_FRAGMENT:
                    tabFragment = RELEVANT_FRAGMENT;
                    tabLayout.setVisibility(View.VISIBLE);
                    initTab(4, R.id.tab_relative_6);
                    transaction.hide(qualificationsFragment);
                    transaction.show(xcxRelevantFragment);
                    break;
                case RELEVANT_FRAGMENT:
                    tabFragment = SETTLEMENT_FRAGMENT;
                    tabLayout.setVisibility(View.VISIBLE);
                    initTab(3, R.id.tab_relative_5);
                    transaction.hide(xcxRelevantFragment);
                    transaction.show(settlementInfoFragment);
                    break;
                case SETTLEMENT_FRAGMENT:
                    if (selectType == Constants.XCX_TYPE || selectType == Constants.H5_TYPE || selectType == Constants.CHAIN_CHILD_TYPE) {
                        if (businessWay == TS) {
                            tabFragment = SHOP_INFO_FRAGMENT;
                            tabLayout.setVisibility(View.VISIBLE);
                            initTab(1, R.id.tab_relative_3);
                            transaction.hide(settlementInfoFragment);
                            transaction.show(shopInfoFragment);
                        }else {
                            tabFragment = SEND_INFO_FRAGMENT;
                            tabLayout.setVisibility(View.VISIBLE);
                            initTab(2, R.id.tab_relative_4);
                            transaction.hide(settlementInfoFragment);
                            transaction.show(sendInfoFragment);
                        }
                    }
                    if (selectType == Constants.CHAIN_TYPE) {
                        tabFragment = BASIC_FRAGMENT;
                        tabLayout.setVisibility(View.VISIBLE);
                        initTab(0, R.id.tab_relative_2);
                        transaction.hide(settlementInfoFragment);
                        transaction.show(basicInfoFragment);
                    }
                    break;
                case SEND_INFO_FRAGMENT:
                    tabFragment = SHOP_INFO_FRAGMENT;
                    tabLayout.setVisibility(View.VISIBLE);
                    initTab(1, R.id.tab_relative_3);
                    transaction.hide(sendInfoFragment);
                    transaction.show(shopInfoFragment);
                    break;
                case SHOP_INFO_FRAGMENT:
                    tabFragment = BASIC_FRAGMENT;
                    tabLayout.setVisibility(View.VISIBLE);
                    initTab(0, R.id.tab_relative_2);
                    transaction.hide(shopInfoFragment);
                    transaction.show(basicInfoFragment);
                    break;
                case BASIC_FRAGMENT:
                    tabFragment = SELECT_FRAGMENT;
                    if (backFragment instanceof ExpandsLogFragment) {
                        tabLayout.setVisibility(View.GONE);
                        frgmentType = LOG_FRAGMENT;
                        expandsLogBtn.setTextColor(ContextCompat.getColor(mContext, R.color.red));
                        expandsLogLine.setBackgroundResource(R.color.red);
                        expandsShopBtn.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
                        expandsShopLine.setBackgroundResource(R.color.line_bg);
                        exitBtn.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
                        exitLine.setBackgroundResource(R.color.line_bg);
                        logFragment = EXPANDS_LOG_FRAGMENT;
                        transaction.hide(basicInfoFragment);
                        transaction.show(expandsLogFragment);
                    }else if (backFragment instanceof ChildShopLogFragment) {
                        tabLayout.setVisibility(View.GONE);
                        frgmentType = LOG_FRAGMENT;
                        expandsLogBtn.setTextColor(ContextCompat.getColor(mContext, R.color.red));
                        expandsLogLine.setBackgroundResource(R.color.red);
                        expandsShopBtn.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
                        expandsShopLine.setBackgroundResource(R.color.line_bg);
                        exitBtn.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
                        exitLine.setBackgroundResource(R.color.line_bg);
                        logFragment = CHILD_SHOP_LOG_FRAGMENT;
                        transaction.hide(basicInfoFragment);
                        transaction.show(childShopLogFragment);
                    }else if (backFragment instanceof SelectModelFragment) {
                        tabLayout.setVisibility(View.GONE);
                        transaction.hide(basicInfoFragment);
                        transaction.show(selectModelFragment);
                    } else {
                        tabLayout.setVisibility(View.GONE);
                        transaction.hide(basicInfoFragment);
                        transaction.show(selectModelFragment);
                    }
                    break;
                case SELECT_FRAGMENT:
                    exit();
                    break;
            }
            transaction.commit();
        } else if (frgmentType == LOG_FRAGMENT) {
            switch (logFragment) {
                case EXPANDS_LOG_FRAGMENT:
                    exit();
                    break;
                case LOG_DETAIL_FRAGMENT:
                    logDetailBack();
                    break;
                case MONEY_VOUCHER_FRAGMENT:
                    moneyVoucherBack();
                    break;
                case CHILD_SHOP_LOG_FRAGMENT:
                    childShopBack();
                    break;
            }
        }
    }

    /**
     * 加载TAB
     * @param index 坐标
     * @param id  当前控件ID
     */
    private void initTab(int index, int id){
        for (RelativeLayout layout : tabRelativeList) {
            if (layout.getChildAt(1) != null) layout.removeViewAt(1);
        }
        for (int i = 0; i < tabRelativeList.size(); i ++) {
            if (i == index) continue;
            if (tabRelativeList.get(i).getId() == id) tabRelativeList.get(i).addView(loadImg(i, false));
            else tabRelativeList.get(i).addView(loadImg(i, true));
        }
    }

    /**
     * 渲染ImageView
     * @param flag  标识
     * @return
     */
    private ImageView loadImg(int index, Boolean flag){
        ImageView imageView = new ImageView(mContext);
        RelativeLayout.LayoutParams rl;
        if (flag) {
            imageView.setImageResource(R.drawable.into_img);
            rl = new RelativeLayout.LayoutParams(imgWidth, ViewGroup.LayoutParams.MATCH_PARENT);
            rl.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        } else {
            if (index == tabRelativeList.size() - 1) imageView.setImageResource(R.drawable.finish);
            else imageView.setImageResource(R.drawable.into_red);
            rl = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setLayoutParams(rl);
        return imageView;
    }


    private void addFragment(int type){
        if (type == Constants.H5_TYPE) {
            tabRelativeOther.setVisibility(View.GONE);

            tabRelative_3.setVisibility(View.VISIBLE);
            tabRelative_4.setVisibility(View.VISIBLE);
            tabRelative_6.setVisibility(View.VISIBLE);
            tabRelative_7.setVisibility(View.GONE);
            title1View.setText("申请H5");
            title2View.setText(getString(R.string.title_2));
            title6View.setText(getString(R.string.tile_h5));
            tabLayout.setVisibility(View.VISIBLE);

            tabRelativeList.clear();
            tabRelativeList.add(tabRelative_1);
            tabRelativeList.add(tabRelative_2);
            tabRelativeList.add(tabRelative_3);
            tabRelativeList.add(tabRelative_4);
            tabRelativeList.add(tabRelative_5);
            tabRelativeList.add(tabRelative_6);
            tabRelativeList.add(tabRelative_8);
        }else if (type == Constants.XCX_TYPE || type == Constants.CHAIN_CHILD_TYPE){
            tabRelativeOther.setVisibility(View.GONE);

            tabRelative_3.setVisibility(View.VISIBLE);
            tabRelative_4.setVisibility(View.VISIBLE);
            tabRelative_6.setVisibility(View.VISIBLE);
            tabRelative_7.setVisibility(View.VISIBLE);
            title1View.setText("申请小程序");
            title2View.setText(getString(R.string.title_2));
            title6View.setText(getString(R.string.title_6));
            tabLayout.setVisibility(View.VISIBLE);

            tabRelativeList.clear();
            tabRelativeList.add(tabRelative_1);
            tabRelativeList.add(tabRelative_2);
            tabRelativeList.add(tabRelative_3);
            tabRelativeList.add(tabRelative_4);
            tabRelativeList.add(tabRelative_5);
            tabRelativeList.add(tabRelative_6);
            tabRelativeList.add(tabRelative_7);
            tabRelativeList.add(tabRelative_8);
        }  else {
            tabRelativeOther.setVisibility(View.VISIBLE);

            tabRelative_6.setVisibility(View.VISIBLE);
            tabRelative_3.setVisibility(View.GONE);
            tabRelative_4.setVisibility(View.GONE);
            tabRelative_7.setVisibility(View.GONE);
            title1View.setText("申请小程序");
            title2View.setText("总账号");
            title6View.setText(getString(R.string.title_6));
            tabLayout.setVisibility(View.VISIBLE);

            tabRelativeList.clear();
            tabRelativeList.add(tabRelative_1);
            tabRelativeList.add(tabRelative_2);
            tabRelativeList.add(tabRelative_5);
            tabRelativeList.add(tabRelative_6);
            tabRelativeList.add(tabRelative_8);
        }
    }


    private void exitDialog(){
        AlertDialog dialog = new AlertDialog(mContext, getString(R.string.do_you_exit_account),
                getString(R.string.dial_confirm_ok), getString(R.string.away));
        dialog.setBtnOkLinstener(new ExitAccountClickListener());
        dialog.show();
    }

    class ExitAccountClickListener implements AlertDialog.OnClickListener{
        @Override
        public void onClick(DialogInterface dialog, int which) {
            if (which == 0) {
                if (frgmentType == TAB_FRAGMENT) {
                    expandsShopBtn.setTextColor(ContextCompat.getColor(mContext, R.color.red));
                    expandsShopLine.setBackgroundResource(R.color.red);
                    expandsLogBtn.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
                    expandsLogLine.setBackgroundResource(R.color.line_bg);
                    exitBtn.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
                    exitLine.setBackgroundResource(R.color.line_bg);
                }else if (frgmentType == LOG_FRAGMENT){
                    expandsLogBtn.setTextColor(ContextCompat.getColor(mContext, R.color.red));
                    expandsLogLine.setBackgroundResource(R.color.red);
                    expandsShopBtn.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
                    expandsShopLine.setBackgroundResource(R.color.line_bg);
                    exitBtn.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
                    exitLine.setBackgroundResource(R.color.line_bg);
                }
                dialog.dismiss();
            } else if (which == 1) {
                String filePath = SysApplication.getInstance().getLoginUserCacheFilePath();
                if (FileUtils.deleteFile(filePath)) {
                    Intent intent = new Intent(mContext, LoginActivity.class);
                    startActivity(intent);
                    SysApplication.getInstance().setmUser(null);
                    finish();
                }
            }
        }
    }


    Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            isExit = false;
        }
    };
    private void exit() {
        if (!isExit) {
            isExit = true;
            Toast.makeText(getApplicationContext(), "再按一次退出程序", Toast.LENGTH_SHORT).show();
            // 利用handler延迟发送更改状态信息
            mHandler.sendMessageDelayed(mHandler.obtainMessage(), 2000);
        } else {
            SysApplication.getInstance().exit();
            System.exit(0);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            backClick();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
}
